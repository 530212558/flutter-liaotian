// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

// import 'dart:html';

// import 'package:flutter/material.dart';
// import 'package:flutter_test/flutter_test.dart';

// import 'package:huasheng_front_flutter/main.dart';

import 'package:huasheng_front_flutter/utils/Global.dart';

class AccountManager {
  static final _singleton = AccountManager._internal();
  String lala = 'aaa';
  AccountManager._internal() {
    // loadLoginInfo();
    // loadUser();
    print(1);
  }
  factory AccountManager() {
    return _singleton;
  }
}

class Logger {
  final String name;
  bool mute = false;

  // _cache 是一个私有库,幸好名字前有个 _ 。
  static final Map<String, Logger> _cache = <String, Logger>{};

  factory Logger(String name) {
    if (_cache.containsKey(name)) {
      return _cache[name];
    } else {
      final logger = new Logger._internal(name);
      _cache[name] = logger;
      return logger;
    }
  }

  Logger._internal(this.name);
}

void main() {
  // testWidgets('Counter increments smoke test', (WidgetTester tester) async {
  //   // Build our app and trigger a frame.
  //   await tester.pumpWidget(MyApp());

  //   // Verify that our counter starts at 0.
  //   expect(find.text('0'), findsOneWidget);
  //   expect(find.text('1'), findsNothing);

  //   // Tap the '+' icon and trigger a frame.
  //   await tester.tap(find.byIcon(Icons.add));
  //   await tester.pump();

  //   // Verify that our counter has incremented.
  //   expect(find.text('0'), findsNothing);
  //   expect(find.text('1'), findsOneWidget);
  // });
  // print(AccountManager().lala);
  // AccountManager().lala = 'ccc';
  // print(AccountManager().lala);
  // var url = 'www.baidu.com';
  // var parameters = {'a': 1, "b": 2};
  // parameters.forEach((key, value) {
  //   print('key:$key, value:$value');
  //   if (url.indexOf(key) != -1) {
  //     url = url.replaceAll(':$key', value.toString());
  //   }
  // });
  // print(url);

  print(convertingMoney(132.6).number);
}
