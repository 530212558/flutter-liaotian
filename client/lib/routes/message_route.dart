import 'package:huasheng_front_flutter/pages/messages/message_detail_page.dart';

final messageRoutes = {
  '/message_detail_page': (context, {arguments}) =>
      MessagesDetailPage(arguments: arguments),
};
