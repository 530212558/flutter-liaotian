import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/pages/main/root_page.dart';
import 'package:huasheng_front_flutter/pages/login_register/apply_invitation_code_page.dart';
import 'package:huasheng_front_flutter/pages/login_register/forget_password_page.dart';
import 'package:huasheng_front_flutter/pages/login_register/complete_info_page.dart';
import 'package:huasheng_front_flutter/pages/login_register/login_page.dart';
import 'package:huasheng_front_flutter/pages/login_register/enter_invitation_code_page.dart';
import 'package:huasheng_front_flutter/pages/login_register/register_page.dart';
import 'package:huasheng_front_flutter/pages/login_register/reset_password_page.dart';
import 'package:huasheng_front_flutter/pages/login_register/select_gender_page.dart';
import 'package:huasheng_front_flutter/pages/square/dynami_dcetails_page.dart';
import 'package:huasheng_front_flutter/pages/square/meeting_detail_page.dart';
import 'package:huasheng_front_flutter/pages/square/post_meeting_page.dart';
import 'package:huasheng_front_flutter/pages/square/release_news_page.dart';
//import 'package:huasheng_front_flutter/pages/tabs/tabs_page.dart';
import 'package:huasheng_front_flutter/pages/user/user_page.dart';
import 'package:huasheng_front_flutter/pages/widgets/webview_page.dart';
import 'package:huasheng_front_flutter/routes/message_route.dart';

import '../utils/size_fit.dart';

//配置路由
final routes = {
  '/': (context, {arguments}) => TabsPage(arguments: arguments),
  '/LoginPage': (context) => LoginPage(),
  '/RegisterPage': (context) => RegisterPage(),
  '/SelectGenderPage': (context) => SelectGenderPage(),
  '/EnterInvitationCodePage': (context, {arguments}) =>
      EnterInvitationCodePage(arguments: arguments),
  '/ApplyInvitationCodePage': (context) => ApplicationInvitationCode(),
  '/CompleteInfoPage': (context, {arguments}) =>
      CompleteInfoPage(arguments: arguments),
  '/ForgetPasswordPage': (context) => ForgetPasswordPage(),
  '/ResetPasswordPage': (context, {arguments}) =>
      ResetPasswordPage(arguments: arguments),
  '/UserPage': (context, {arguments}) => UserPage(arguments: arguments),
  '/dynamicDcetailsPage': (context, {arguments}) =>
      DynamicDcetailsPage(arguments: arguments),
  '/releaseNewsPage': (context) => ReleaseNewsPage(),
  '/meetingDetailPage': (context) => MeetingDetailPage(),
  '/postMeetingPage': (context, {arguments}) =>
      PostMeetingPage(arguments: arguments),
  '/WebViewPage': (context, {arguments}) => WebViewPage(arguments: arguments),
  ...messageRoutes
};

Route<dynamic> onGenerateRoute(RouteSettings settings) {
  final Function pageContentBuilder = routes[settings.name];
  if (pageContentBuilder != null) {
    if (settings.arguments != null) {
      final Route route = MaterialPageRoute(builder: (context) {
        HYSizeFit.initialize(context);
        return pageContentBuilder(context, arguments: settings.arguments);
      });
      return route;
    } else {
      final Route route = MaterialPageRoute(builder: (context) {
        HYSizeFit.initialize(context);
        return pageContentBuilder(context);
      });
      return route;
    }
  }
  return null;
}
