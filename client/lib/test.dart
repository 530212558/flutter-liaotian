import 'dart:io';

import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/http/dio.dart';
import 'package:image_picker/image_picker.dart';

class HeadImageUploadPage extends StatefulWidget {
  @override
  _HeadImageUploadPageState createState() => _HeadImageUploadPageState();
}

class _HeadImageUploadPageState extends State<HeadImageUploadPage> {
  String _image = "";

  //拍照
  _takePhoto() async {
    final picker = ImagePicker();
    var image =
        await picker.getImage(source: ImageSource.camera); //maxWidth: 400剪切操作
    // setState(() {
    //   _image = File(image.path);
    // });
    //调用图片上传
    Dios.uploadImage(File(image.path), success: (String url) {
      this.setState(() {
        this._image = url;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Image Picker Example'),
      ),
      body: Center(
        child:
            _image == "" ? Text('No image selected.') : Image.network(_image),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _takePhoto,
        tooltip: 'Pick Image',
        child: Icon(Icons.add_a_photo),
      ),
    );
  }
}
