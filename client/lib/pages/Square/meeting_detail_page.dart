import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';
import 'package:huasheng_front_flutter/pages/square/widgets/meetingListItem.dart';

class MeetingDetailPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Color(0xFF333333),
        ),
        title: Text(
          '约会详情',
          style: TextStyle(
            color: Color(0xFF333333), //设置字体颜色
          ),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: MeetingListItem(index: 0, isDetail: true),
    );
  }
}
