import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/models/dynamics_model.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';

class DynamicItem extends StatefulWidget {
  DynamicItem(this.item, {this.onComment, this.onLike});
  Function(Dynamics item) onComment;
  Function(Dynamics item) onLike;
  Dynamics item;
  @override
  _DynamicItemState createState() => _DynamicItemState(item);
}
// class _DynamicItemState extends StatelessWidget{

// }
class _DynamicItemState extends State<DynamicItem> {
  _DynamicItemState(this.item);
  Dynamics item;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  var showOption = true;

  List<Widget> buildGrid(List<String> fileUrls) {
    List<Widget> tiles = []; //先建一个数组用于存放循环生成的widget
    for (var item in fileUrls) {
      tiles.add(
        GestureDetector(
          onTap: () => print('object'),
          child: Container(
            // color: Colors.red,
            decoration: BoxDecoration(
              // color: Colors.red,
              borderRadius: BorderRadius.circular(20.rpx),
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.rpx),
              child: Image.network(
                item,
                fit: BoxFit.fill,
                // fit: BoxFit.scaleDown,
              ),
            ),
          ),
        ),
      );
    }
    return tiles;
  }

  @override
  Widget build(BuildContext context) {
    var sex = item.sex == 1
        ? 'assets/images/square/GenderMale.png'
        : 'assets/images/square/GenderFemale.png';
    var realAuth = item.realAuth == 1
        ? [Color(0xff1AD8D0), Color(0xff1AD89E)]
        : [Color(0xffFE7C96), Color(0xffFE997C)];
    var realAuthStr = '';
    // 0 未认证 1 真人 2 女神 3男神
    if (item.realAuth == 1)
      realAuthStr = '真人';
    else if (item.realAuth == 2)
      realAuthStr = '女神';
    else if (item.realAuth == 3) realAuthStr = '3男神';
    var selfPublish = item.selfPublish == 1 ? false : true;
    var likeStatus = item.likeStatus == 1
        ? 'assets/images/square/PraiseAsh.png'
        : 'assets/images/square/PraiseAshActive.png';
    var commentStatus = item.commentStatus == 1
        ? 'assets/images/square/CommentGrey.png'
        : 'assets/images/square/CommentGreyActive.png';
    var crossAxisCount = 1;
    if (item.fileUrls.length != 1)
      crossAxisCount = item.fileUrls.length > 3 ? 4 : 3;
    return Container(
      padding: EdgeInsets.only(top: 40.rpx, bottom: 26.rpx),
      child: Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, '/dynamicDcetailsPage',
                      arguments: item);
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                        width: 100.rpx,
                        height: 100.rpx,
                        margin: EdgeInsets.only(right: 28.rpx),
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: NetworkImage(item.avatar),
                                fit: BoxFit.cover))),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Container(
                                constraints: BoxConstraints(
                                  maxWidth: 250.rpx,
                                ),
                                child: Text(
                                  item.nickName,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                      color: Color(0xff333333),
                                      fontSize: 32.rpx,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                              Expanded(
                                child: Row(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(
                                          left: 12.rpx, right: 12.rpx),
                                      child: Image.asset(
                                        sex,
                                        width: 36.rpx,
                                        height: 36.rpx,
                                      ),
                                    ),
                                    Offstage(
                                      offstage:
                                          realAuthStr == '' ? true : false,
                                      child: Container(
                                        width: 80.rpx,
                                        height: 36.rpx,
                                        decoration: BoxDecoration(
                                            gradient: LinearGradient(
                                                colors: realAuth), // 渐变色
                                            borderRadius:
                                                BorderRadius.circular(24.rpx)),
                                        child: FlatButton(
                                          padding: EdgeInsets.all(0),
                                          onPressed: () {},
                                          child: Text(
                                            realAuthStr,
                                            style: TextStyle(
                                                color: Color(0xffffffff),
                                                fontSize: 24.rpx),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Offstage(
                                        offstage: selfPublish,
                                        child: Container(
                                          margin: EdgeInsets.only(left: 12.rpx),
                                          width: 120.rpx,
                                          height: 36.rpx,
                                          decoration: BoxDecoration(
                                              color: Color.fromRGBO(
                                                  26, 216, 208, 0.2),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      24.rpx)),
                                          child: FlatButton(
                                            padding: EdgeInsets.all(0),
                                            onPressed: () {},
                                            child: Text(
                                              '我发布的',
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      26, 216, 208, 1),
                                                  fontSize: 24.rpx),
                                            ),
                                          ),
                                        )),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 8.rpx),
                          ),
                          Text(
                            item.createdOn,
                            style: TextStyle(
                                color: Color(0xff999999),
                                fontWeight: FontWeight.w400,
                                fontSize: 28.rpx),
                          )
                        ],
                      ),
                    ),
                    Container(
                      width: 40.rpx,
                      child: FlatButton(
                        padding: EdgeInsets.all(0),
                        onPressed: () {
                          this.setState(() {
                            this.showOption = !this.showOption;
                          });
                        },
                        child: Image.asset(
                          'assets/images/square/MoreBlack.png',
                          width: 40.rpx,
                          height: 8.rpx,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 40.rpx, bottom: 28.rpx),
                child: Text(
                  item.content,
                  style: TextStyle(
                      color: Color(0xff333333),
                      fontSize: 32.rpx,
                      fontWeight: FontWeight.w500),
                ),
              ),
              Offstage(
                offstage: item.fileUrls.length == 0,
                child: Wrap(
                    spacing: 18.rpx, // 主轴(水平)方向间距
                    runSpacing: 4.0, // 纵轴（垂直）方向间距
                    children: buildGrid(item.fileUrls)),
              ),
              Padding(
                padding: EdgeInsets.only(top: 40.rpx),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(
                    width: 48.rpx,
                    height: 48.rpx,
                    child: FlatButton(
                        onPressed: () {
                          if (widget.onLike != null) widget.onLike(item);
                        },
                        padding: EdgeInsets.all(0),
                        child: Image.asset(
                          likeStatus,
                          width: 48.rpx,
                          height: 48.rpx,
                        )),
                  ),
                  Text(
                    ' ${item.likedTotal}',
                    style: TextStyle(
                        color: Color(0xff1AD8D0),
                        fontSize: 28.rpx,
                        fontWeight: FontWeight.w500),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 40.rpx),
                    width: 48.rpx,
                    height: 48.rpx,
                    child: FlatButton(
                        onPressed: () {
                          if (widget.onComment != null) widget.onComment(item);
                        },
                        padding: EdgeInsets.all(0),
                        child: Image.asset(
                          commentStatus,
                          width: 48.rpx,
                          height: 48.rpx,
                        )),
                  ),
                  Text(
                    ' ${item.commentTotal}',
                    style: TextStyle(
                        color: Color(0xff999999),
                        fontSize: 28.rpx,
                        fontWeight: FontWeight.w400),
                  ),
                ],
              )
            ],
          ),
          Positioned(
              top: 80.rpx,
              right: 0,
              child: Offstage(
                offstage: showOption,
                child: Container(
                  width: 168.rpx,
                  height: 160.rpx,
                  decoration: BoxDecoration(
                      color: Color(0xffF5F5F5),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(32.rpx),
                          bottomRight: Radius.circular(32.rpx),
                          bottomLeft: Radius.circular(32.rpx))
                      // shape: BoxShape.circle,
                      ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        padding: EdgeInsets.only(bottom: 20.rpx),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    width: 1, color: Color(0xffffffff)))),
                        child: Text(
                          '开放评论',
                          style: TextStyle(
                              color: Color(0xff333333),
                              fontSize: 28.rpx,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 18.rpx),
                        child: Text(
                          '删除动态',
                          style: TextStyle(
                              color: Color(0xff333333),
                              fontSize: 28.rpx,
                              fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                ),
              )),
        ],
      ),
    );
  }
}
