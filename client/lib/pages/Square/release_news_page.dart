import 'dart:ffi';
import 'dart:io';
import 'dart:typed_data';

// import 'package:amap_location/amap_location.dart';
// import 'package:amap_search_fluttify/amap_search_fluttify.dart';
import 'package:dio/dio.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:huasheng_front_flutter/http/api_defines.dart';
import 'package:huasheng_front_flutter/http/api_manager.dart';
import 'package:huasheng_front_flutter/models/response_model.dart';
import 'package:huasheng_front_flutter/pages/widgets/ShowDialog.dart';
import 'package:huasheng_front_flutter/pages/widgets/public_ui.dart';
import 'package:huasheng_front_flutter/utils/Global.dart';
import 'package:huasheng_front_flutter/utils/authorityCheck.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';
// import 'package:image_picker/image_picker.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:path_provider/path_provider.dart';

class ReleaseNewsPage extends StatefulWidget {
  @override
  _ReleaseNewsPageState createState() => _ReleaseNewsPageState();
}

class _ReleaseNewsPageState extends State<ReleaseNewsPage> {
  TextEditingController content = new TextEditingController();

  var noComment = false;
  var implicitHomonymy = false;

  Widget _image;
  List<Asset> resultList;
  // final ImagePicker picker = ImagePicker();

  @override
  void initState() {
    super.initState();
    // 先启动一下定位
    // AMapLocationClient.startup(AMapLocationOption(
    //     desiredAccuracy: CLLocationAccuracy.kCLLocationAccuracyHundredMeters));
    // amap_search_fluttify: ^0.12.0 #高德地图搜索组件 初始化 (Ios key);
    // AmapCore.init('bf2775ecf0c61d253c12253ba65049bd');
    this.getAddress();
  }

  getImages() async {
    // final pickedFile = await picker.getImage(source: ImageSource.camera);
    bool result = await Permissions.getCamera();
    if (result) {
      try {
        List<Asset> resultList = await MultiImagePicker.pickImages(
          // 选择图片的最大数量
          maxImages: 8,
          // 是否支持拍照
          enableCamera: true,
          materialOptions: MaterialOptions(
              // 显示所有照片，值为 false 时显示相册
              startInAllView: true,
              allViewTitle: '所有照片',
              actionBarColor: '#2196F3',
              textOnNothingSelected: '没有选择照片'),
        );
        this.setState(() {
          this.resultList = resultList;
          this._imagesList();
          this._image = GridView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 4,
                mainAxisSpacing: 18.rpx,
                crossAxisSpacing: 18.rpx,
              ),
              itemCount: resultList.length,
              itemBuilder: (context, index) {
                Asset asset = resultList[index];
                return AssetThumb(
                  asset: asset,
                  width: 50,
                  height: 50,
                );
              });
        });
      } on Exception catch (e) {
        e.toString();
      }
    } else {
      alertDialog(
        context: context,
        onCancel: () {
          print('onCancel');
          Navigator.of(context).pop();
          // widget.callBack();
        },
        onConfirm: () {
          print('onConfirm');
          Permissions.openAppSetting();
          Navigator.of(context).pop();
          // widget.callBack();
        },
        title: Text('相册权限被拒绝'),
        content: Text(('是否需要跳转到打开相册权限？')),
      );
    }
  }

  getAddress() async {
    bool result = await Permissions.getLocation();
    if (result) {
      // AMapLocation location = await AMapLocationClient.getLocation(true);

      // //  纬度 latitude 经度 longitude
      // print('latitude: ${location.latitude},longitude: ${location.longitude}');
      // print(location);
      // if (location.latitude == null || location.longitude == null) {
      //   print('获取经纬度错误❌');
      //   return false;
      // }

      // /// 逆地理编码（坐标转地址）
      // ReGeocode reGeocodeList = await AmapSearch.instance.searchReGeocode(
      //   LatLng(
      //     double.parse(location.latitude.toString()),
      //     double.parse(location.longitude.toString()),
      //   ),
      //   radius: 200.0,
      // );
      // // print('-------------------');
      // // print(reGeocodeList.provinceName);
      // // print('-------------------');
      // // print(await reGeocodeList.toFutureString());
      // // print('-------------------');
      // // var address = await reGeocodeList.formatAddress;
      // // print(address);
      // saveDynamicsParams['lat'] = location.latitude;
      // saveDynamicsParams['lon'] = location.longitude;
      // saveDynamicsParams['province'] = await reGeocodeList.provinceName;
      // saveDynamicsParams['city'] = await reGeocodeList.cityName;
      // saveDynamicsParams['district'] = await reGeocodeList.districtName;
      // // _locationController.text = '$provinceName/$cityName';
    } else {
      print('openAppSettings');
      alertDialog(
        context: context,
        onCancel: () {
          print('onCancel');
          Navigator.of(context).pop();
        },
        onConfirm: () {
          print('onConfirm');
          Permissions.openAppSetting();
          Navigator.of(context).pop();
        },
        title: Text('定位权限被拒绝'),
        content: Text(('必须先提供定位权限才能申请邀请码')),
      );
    }
  }

  _imagesList() async {
    List<String> files = [];
    for (int i = 0; i < resultList.length; i++) {
      if (i >= 8) {
        toast('上传照片最多不超过8张');
        continue;
      }
      //获得一个uuud码用于给图片命名
      // final String uuid = Uuid().v1();
      //请求原始图片数据
      ByteData item = await resultList[i].getByteData();
      //获取图片数据，并转换成uint8List类型
      Uint8List imageData = item.buffer.asUint8List();

      //获得应用临时目录路径
      final Directory _directory = await getTemporaryDirectory();
      final Directory _imageDirectory =
          await new Directory('${_directory.path}/image/')
              .create(recursive: true);
      var path = _imageDirectory.path;
      File imageFile = new File('$path${i}originalImage_uuid.jpg')
        ..writeAsBytesSync(imageData);
      //  添加路径
      files.add(imageFile.path);

      // this.setState(() {
      //   _image = Image.file(
      //     File(imageFile.path),
      //     fit: BoxFit.cover,
      //     width: 160.rpx,
      //     height: 160.rpx,
      //   );
      // });
    }
    saveDynamicsParams['files'] = files;
    // saveDynamicsParams['files'] = await toMultipartFile(files);
  }

  dynamic saveDynamicsParams = {
    'content': '',
    'enable_comment': 1,
    'enable_same_sex_hide': 1,
    'province': '省',
    'city': '市',
    'district': '区/县城',
    'lat': 1,
    'lon': 1,
    'files': [],
    'file_type': 1
  };
  saveDynamics() async {
    if (content.text.length <= 0) {
      return toast('请填写动态内容');
    }
    // print(this.saveDynamicsParams);
    saveDynamicsParams['content'] = content.text;
    saveDynamicsParams['enable_comment'] = noComment ? 2 : 1;
    saveDynamicsParams['enable_same_sex_hide'] = implicitHomonymy ? 2 : 1;

    // ResponseModel response = await APIManager().postFormData(
    //     kAPIURLUploadFile, {'files': saveDynamicsParams['files']});

    // ResponseModel response = await APIManager()
    //     .uploadFile(kAPIURLUploadFile, saveDynamicsParams['files']);

    ResponseModel response = await APIManager().uploadFile(
        kAPIURLUserDynamics, saveDynamicsParams['files'],
        params: saveDynamicsParams);

    // ResponseModel response;
    // response = await APIManager()
    //     .postFormData(kAPIURLUserDynamics, saveDynamicsParams);
    if (response.isSucceeded()) {
      // var imageURLs = response.response['data'];
      // if (imageURLs.length > 0) {
      //   // _params['avater'] = imageURLs.first;
      //   // _submitUserInfo();
      // }
      toast('提交成功');
      Navigator.pop(context);
    } else {
      toast('发布动态失败');
    }

    // print('-------------------');
    // var data = response.response;
    // print(data);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Color(0xFF333333),
          ),
          title: Text(
            '发布动态',
            style: TextStyle(
              color: Color(0xFF333333), //设置字体颜色
            ),
          ),
          backgroundColor: Colors.white,
          elevation: 0,
          leading: GestureDetector(
            child: Icon(Icons.arrow_back_ios),
            onTap: () => Navigator.pop(context),
          ),
        ),
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
            child: Container(
          padding: EdgeInsets.only(left: 28.rpx, right: 28.rpx, top: 60.rpx),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextField(
                controller: content,
                minLines: 4,
                maxLines: 12,
                // maxLength: 2000,
                textAlign: TextAlign.left,
                decoration: InputDecoration(
                  border: UnderlineInputBorder(borderSide: BorderSide.none),
                  hintText: '请输入个人简介，让别人快速认识你',
                  hintStyle:
                      TextStyle(color: Color(0xFF999999), fontSize: 29.rpx),
                  // contentPadding: EdgeInsets.only(left: 20, top: 66.rpx),
                ),
              ),
              Container(
                child: _image != null ? _image : null,
              ),
              GestureDetector(
                onTap: () {
                  this.getImages();
                },
                child: Container(
                  margin: EdgeInsets.only(bottom: 28.rpx),
                  child: Image.asset(
                    'assets/images/square/AddPhotos.png',
                    width: 220.rpx,
                    height: 220.rpx,
                  ),
                ),
              ),
              Row(
                children: [
                  Image.asset(
                    'assets/images/square/exclamatoryMark.png',
                    width: 28.rpx,
                    height: 28.rpx,
                  ),
                  Padding(
                    padding: EdgeInsets.only(right: 12.rpx),
                  ),
                  Text(
                    '请忽上传裸露低俗的照片，严重者将做封号处理',
                    style: TextStyle(
                        color: Color(0xFF999999),
                        fontSize: 28.rpx,
                        fontWeight: FontWeight.w400),
                  )
                ],
              ),
              Padding(
                padding: EdgeInsets.only(top: 78.rpx),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '禁止评论',
                    style: TextStyle(
                        color: Color(0xFF333333),
                        fontSize: 32.rpx,
                        fontWeight: FontWeight.w500),
                  ),
                  GestureDetector(
                    onTap: () {
                      this.setState(() {
                        noComment = !noComment;
                      });
                    },
                    child: Image.asset(
                      noComment == false
                          ? 'assets/images/square/close.png'
                          : 'assets/images/square/open.png',
                      width: 104.rpx,
                      height: 68.rpx,
                    ),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.only(top: 32.rpx),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '对同性用户隐藏',
                    style: TextStyle(
                        color: Color(0xFF333333),
                        fontSize: 32.rpx,
                        fontWeight: FontWeight.w500),
                  ),
                  GestureDetector(
                    onTap: () {
                      this.setState(() {
                        implicitHomonymy = !implicitHomonymy;
                      });
                    },
                    child: Image.asset(
                      implicitHomonymy
                          ? 'assets/images/square/open.png'
                          : 'assets/images/square/close.png',
                      width: 104.rpx,
                      height: 68.rpx,
                    ),
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 112.rpx, bottom: 56.rpx),
                alignment: Alignment.center,
                child: MaterialButton(
                  onPressed: () {
                    this.saveDynamics();
                  },
                  child: Text(
                    '登录',
                    style: TextStyle(
                      color: Color(0xFF333333),
                      fontSize: 32.rpx,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  minWidth: 590.rpx,
                  height: 100.rpx,
                  color: Color(0xffFFD132),
                  shape: StadiumBorder(),
                ),
              ),
              Container(
                alignment: Alignment.center,
                child: Text(
                  '会员免费，非会员需支付12元',
                  style: TextStyle(
                    color: Color(0xFFFF3D3D),
                    fontSize: 28.rpx,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ],
          ),
        )));
  }
}
