import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/models/dynamics_model.dart';
import 'package:huasheng_front_flutter/pages/widgets/comments.dart';
import 'package:huasheng_front_flutter/pages/square/dynamicWidgets/list.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';

class DynamicDcetailsPage extends StatefulWidget {
  final Dynamics arguments;

  DynamicDcetailsPage({this.arguments});
  @override
  _DynamicDcetailsPageState createState() =>
      _DynamicDcetailsPageState(arguments);
}

class _DynamicDcetailsPageState extends State<DynamicDcetailsPage> {
  Dynamics item;
  _DynamicDcetailsPageState(this.item);
  bool isLoading = false;
  ScrollController scrollController = ScrollController();
  // List<NewsViewModel> list = List.from(newsList);
  var hideComments = false;

  @override
  void initState() {
    super.initState();
    // 给列表滚动添加监听
    this.scrollController.addListener(() {
      // 滑动到底部的关键判断
      if (!this.isLoading &&
          this.scrollController.position.pixels >=
              this.scrollController.position.maxScrollExtent) {
        print('滑动到底部的关键判断');
        // // 开始加载数据
        setState(() {
          this.isLoading = true;
          // this.loadMoreData();
        });
      }
    });
  }

  @override
  void dispose() {
    // 组件销毁时，释放资源（一定不能忘，否则可能会引起内存泄露）
    super.dispose();
    this.scrollController.dispose();
  }

  Future onRefresh() {
    return Future.delayed(Duration(seconds: 1), () {
      print('当前已是最新数据');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('动态详情',
              style: TextStyle(
                color: Color(0xFF333333), //设置字体颜色
              )),
          iconTheme: IconThemeData(
            color: Color(0xFF333333),
          ),
          backgroundColor: Colors.white,
          elevation: 0,
          leading: GestureDetector(
            child: Icon(Icons.arrow_back_ios),
            onTap: () => Navigator.pop(context),
          ),
        ),
        backgroundColor: Colors.white,
        body: GestureDetector(
          onTap: () {
            this.setState(() {
              hideComments = false;
            });
          },
          child: ListView(
            controller: this.scrollController,
            children: [
              Container(
                padding: EdgeInsets.only(left: 28.rpx, right: 28.rpx),
                child: DynamicItem(
                  item,
                  onComment: (item) {
                    print(item.id);
                    this.setState(() {
                      // if (widget.callback != null) {
                      //   widget.callback(this.hideComments);
                      // }
                      this.hideComments = !this.hideComments;
                    });
                  },
                ),
              ),
              Container(
                height: 20.rpx,
                color: Color(0xffF5F5F5),
                child: null,
              ),
              Visibility(
                visible: hideComments,
                // maintainState: true,
                child: comments(),
              ),
              Column(
                children: [
                  for (var i in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
                    // for (var i in [1, 2, 3])
                    comment(),
                ],
              ),

              // ListView.separated(
              //     controller: this.scrollController,
              //     separatorBuilder: (BuildContext context, int index) {
              //       return Divider(color: Color(0xffF5F5F5), thickness: 2
              //           // color: Colors.red,
              //           );
              //     },
              //     itemCount: 10,
              //     itemBuilder: (BuildContext context, int index) {
              //       return ;
              //     })
            ],
          ),
        ));
  }
}

comment() {
  return GestureDetector(
      onTap: () {
        print('GestureDetector');
        // Navigator.pushNamed(context, '/dynamicDcetailsPage');
      },
      child: Container(
        margin: EdgeInsets.only(top: 12.rpx, left: 28.rpx),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
                width: 100.rpx,
                height: 100.rpx,
                margin: EdgeInsets.only(right: 28.rpx),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: NetworkImage(
                            'https://pic2.zhimg.com/v2-639b49f2f6578eabddc458b84eb3c6a1.jpg'),
                        fit: BoxFit.cover))),
            Expanded(
                child: Container(
              padding: EdgeInsets.only(bottom: 24.rpx, top: 24.rpx),
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(width: 2, color: Color(0xffF5F5F5)))),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        constraints: BoxConstraints(
                          maxWidth: 360.rpx,
                        ),
                        child: Text(
                          'alan moranalan moranalan moranalan moranalan moranalan moranalan moran',
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              height: 1.5,
                              color: Color(0xff999999),
                              fontWeight: FontWeight.w400,
                              fontSize: 28.rpx),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(right: 28.rpx),
                        child: Text(
                          '2020/02/22 11:02:12',
                          // '11:02',
                          style: TextStyle(
                              color: Color(0xff999999),
                              fontWeight: FontWeight.w400,
                              fontSize: 24.rpx),
                        ),
                      )
                    ],
                  ),
                  Text(
                    '好好看哦，不错，下次我也去👍',
                    maxLines: 4,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color: Color(0xff333333),
                        fontSize: 32.rpx,
                        fontWeight: FontWeight.w500),
                  )
                ],
              ),
            )),
          ],
        ),
      ));
}
