import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/pages/square/dynamic_page.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';
import 'package:huasheng_front_flutter/pages/square/meeting_page.dart';
import 'package:huasheng_front_flutter/pages/square/widgets/meetingType.dart';

class SquarePage extends StatefulWidget {
  SquarePage({Key key}) : super(key: key);

  _SquarePageState createState() => _SquarePageState();
}

class _SquarePageState extends State<SquarePage>
    with SingleTickerProviderStateMixin {
  final _bottomSheetScaffoldKey = GlobalKey<ScaffoldState>();
  TabController controller;

  TabController _tabController;
  int _tabControllerIndex = 0;
  int tabIndex = 0;

  bool _showFloatingActionButton = true;

  _openModalBottomSheet() {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Stack(children: [
            Container(
              height: 25,
              width: double.infinity,
              color: Colors.black54,
            ),
            Container(
              height: 442.rpx,
              width: double.infinity,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30.rpx),
                  topRight: Radius.circular(30.rpx),
                ),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Container(),
                      ),
                      Expanded(
                        flex: 4,
                        child: FlatButton(
                          onPressed: () {
                            Navigator.pop(context);
                            Navigator.pushNamed(context, '/releaseNewsPage');
                          },
                          child: Column(
                            children: [
                              Image(
                                image: AssetImage(
                                    'assets/images/square/postDynamic.png'),
                                width: 105.rpx,
                                height: 105.rpx,
                                fit: BoxFit.cover,
                              ),
                              SizedBox(
                                height: 10.rpx,
                              ),
                              Text(
                                '动态',
                                style: TextStyle(
                                  fontSize: 30.rpx,
                                  fontWeight: FontWeight.normal,
                                  color: Color(0xFF999999),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: FlatButton(
                          onPressed: () {
                            Navigator.pop(context);
                            openModalMeetingTypeSheet(
                              context,
                              (meetingType) {
                                if (meetingType != null) {
                                  Navigator.pushNamed(
                                    this.context,
                                    '/postMeetingPage',
                                    arguments: {'meetingType': meetingType},
                                  );
                                }
                              },
                            );
                          },
                          child: Column(
                            children: [
                              Image(
                                image: AssetImage(
                                    'assets/images/square/postMeeting.png'),
                                width: 105.rpx,
                                height: 105.rpx,
                                fit: BoxFit.cover,
                              ),
                              SizedBox(
                                height: 10.rpx,
                              ),
                              Text(
                                '约会',
                                style: TextStyle(
                                  fontSize: 30.rpx,
                                  fontWeight: FontWeight.normal,
                                  color: Color(0xFF999999),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 40.rpx,
                  ),
                  Divider(),
                  FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      alignment: Alignment.center,
                      height: 80.rpx,
                      child: Text('取消',
                          style: TextStyle(
                            fontSize: 36.rpx,
                            fontWeight: FontWeight.normal,
                            color: Color(0xFF999999),
                          )),
                    ),
                  ),
                ],
              ),
            ),
          ]);
        });
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  List tabs = <Widget>[];
  var tabsList = ['动态', '约会'];

  @override
  void initState() {
    super.initState();

    tabs = tabsList
        .map((String item) => Tab(
              child: Container(
                child: Text(
                  item,
                ),
              ),
            ))
        .toList();

    _tabController = TabController(
      vsync: this,
      initialIndex: tabIndex, //默认加载第几个
      length: 2,
    );

    _tabController.addListener(() {
      //监听器。
      print(_tabController.index); //获取下标
      setState(() {
        _tabControllerIndex = _tabController.index;
        //进行改变
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget _appBar() {
      return AppBar(
          backgroundColor: Colors.white,
          elevation: 0.0,
          centerTitle: false,
          title: TabBar(
            controller: _tabController, //可以和TabBarView使用同一个TabController
            tabs: tabs,
            labelColor: Color(0xff333333), //选中标签颜色
            labelStyle: TextStyle(
              fontSize: 36.0.rpx,
              fontWeight: FontWeight.w500,
            ),
            unselectedLabelColor: Color(0xff979797),
            unselectedLabelStyle: TextStyle(
              fontSize: 32.0.rpx,
              fontWeight: FontWeight.w400,
            ),
            indicatorColor: Color(0xffFFD132),
            indicatorWeight: 6.rpx,
            indicatorPadding: EdgeInsets.only(left: (48.rpx), right: (48.rpx)),
            // labelPadding: EdgeInsets.symmetric(horizontal: (30)),
            // unselectedLabelColor: Colors.blue,
            isScrollable: true,
          ),
          actions: <Widget>[
            SizedBox(
              width: 88.rpx,
              height: 48.rpx,
              child: FlatButton(
                onPressed: () {
                  _openModalBottomSheet();
                },
                padding: EdgeInsets.all(0),
                child: Icon(
                  Icons.add_circle_outline,
                  size: 48.rpx,
                  color: Colors.grey[800],
                ),
              ),
            ),
          ]);
    }

    return DefaultTabController(
      initialIndex: tabIndex, //默认显示第几个
      length: 2, //有几个tab页面
      child: Scaffold(
          key: _bottomSheetScaffoldKey,
          appBar: _appBar(),
          floatingActionButton:
              _tabControllerIndex == 0 && _showFloatingActionButton
                  ? FloatingActionButton(
                      child: Image.asset(
                        'assets/images/square/Topping.png',
                        width: 132.rpx,
                        height: 132.rpx,
                      ),
                      onPressed: () {
                        dynamicPageChildKey.currentState.childFunction();
                      },
                      elevation: 0,
                      // highlightElevation: 0,
                      backgroundColor: Color.fromRGBO(255, 255, 255, 0))
                  : null,
          body: TabBarView(
            controller: _tabController,
            //此处的每一个Widget代表的是每个tab所对应的页面
            children: <Widget>[
              DynamicPage(
                key: dynamicPageChildKey,
                callback: (data) {
                  this.setState(() {
                    _showFloatingActionButton = data;
                  });
                },
              ),
              MeetingPage(),
            ],
          )),
    );
  }
}
