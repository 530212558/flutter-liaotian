import 'dart:convert';
import 'dart:ffi';

import 'package:date_format/date_format.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/http/api_defines.dart';
import 'package:huasheng_front_flutter/http/api_manager.dart';
import 'package:huasheng_front_flutter/models/dynamics_model.dart';
import 'package:huasheng_front_flutter/models/response_model.dart';
import 'package:huasheng_front_flutter/pages/widgets/comments.dart';
import 'package:huasheng_front_flutter/pages/square/dynamicWidgets/list.dart';
import 'package:huasheng_front_flutter/pages/user/widgets/user_page_album_grid_item.dart';
import 'package:huasheng_front_flutter/pages/widgets/ShowDialog.dart';
import 'package:huasheng_front_flutter/pages/widgets/jhPickerTool.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';
import 'package:huasheng_front_flutter/pages/square/widgets/meetingList.dart';

GlobalKey<_DynamicPageState> dynamicPageChildKey = GlobalKey();

class DynamicPage extends StatefulWidget {
  final Function(bool data) callback;

  DynamicPage({Key key, this.callback}) : super(key: key);
  @override
  _DynamicPageState createState() => _DynamicPageState();
}

class _DynamicPageState extends State<DynamicPage> {
  bool isLoading = false;
  var gender = '不限性别';
  var region = '不限地区';
  var sex_option = 1;
  ScrollController scrollController = ScrollController();
  // List<NewsViewModel> list = List.from(newsList);
  List<Dynamics> list = [];
  var hideComments = false;

  childFunction() {
    scrollController.animateTo(0,
        duration: Duration(milliseconds: 500), curve: Curves.decelerate);
  }

  List<Map<String, Object>> _industryList = [];
  _getIndustryList() async {
    // final response = await APIManager().get(kAPIURLIndustry, null);
    final response = await APIManager().get(kAPIURLProvinceCityList, null);

    if (response.isSucceeded()) {
      if (mounted) {
        setState(() {
          List<Map<String, Object>> list = [];
          // response.response['categories'].forEach((element) {
          //   list.add(element);
          // });
          response.response['PCList'].forEach((element) {
            list.add(element);
          });

          _industryList = list;
        });
      }
    } else {
      APIManager().toastAPIError(response);
    }
  }

  getDynamics() async {
    ResponseModel response;
    response = await APIManager().get(kAPIURLUserDynamics, {
      'current_page': 0,
      'page': 0,
      'page_size': 10,
      'sex_option': sex_option
    });
    if (response.isSucceeded()) {
      var data = response.response['data'];
      List<Dynamics> list = dynamicsFromJson(json.encode(data));
      this.setState(() {
        print(list);
        this.list = list;
      });
    } else {
      APIManager().toastAPIError(response);
    }

    // Map<String, dynamic> json = {
    //   'openid': '11112222',
    //   'current_page': 0,
    //   'page': 0,
    //   'page_size': 10,
    //   // 'sex_option': sex_option
    // };
    // var a = await dios.get('/user/dynamics', queryParameters: json);
    // print(a);
  }

  @override
  void initState() {
    super.initState();
    // 给列表滚动添加监听
    this.scrollController.addListener(() {
      // 滑动到底部的关键判断
      if (!this.isLoading &&
          this.scrollController.position.pixels >=
              this.scrollController.position.maxScrollExtent) {
        print('滑动到底部的关键判断');
        // // 开始加载数据
        setState(() {
          this.isLoading = true;
          // this.loadMoreData();
        });
      }
    });
    this.setState(() {
      var item = Dynamics.fromJson({
        "ID": 1,
        "Content": 'content',
        "EnableComment": 1,
        "SameSexHide": 1,
        "VerifyState": 1,
        "VerifyStateName": 'verifyStateName',
        "CreatedOn": 'createdOn',
        "Province": 'province',
        "City": 'city',
        "District": 'district',
        "FileUrls": [
          'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=2471193303,3938768064&fm=26&gp=0.jpg'
        ],
        "OpenId": 'openId',
        "NickName": 'nickName',
        "RealAuth": 1,
        "SelfPublish": 1,
        "Sex": 1,
        "Avatar":
            'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=2471193303,3938768064&fm=26&gp=0.jpg',
        "Lat": 30,
        "Lon": 60,
        "ShowOption": false,
        "LikeStatus": 1,
        "LikedTotal": 1,
        "CommentStatus": 1,
        "CommentTotal": 1,
      });
      this.list = [item];
    });
    // this.getDynamics();
    // this._getIndustryList();
  }

  @override
  void dispose() {
    // 组件销毁时，释放资源（一定不能忘，否则可能会引起内存泄露）
    super.dispose();
    this.scrollController.dispose();
  }

  Future onRefresh() {
    this.getDynamics();
    return Future.delayed(Duration(seconds: 1), () {
      print('当前已是最新数据');
    });
  }

  _adsBlock() {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.rpx, 20.rpx, 0.rpx, 20.rpx),
      child: Material(
        child: Ink(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(18.rpx)),
          ),
          child: InkWell(
            onTap: () {},
            child: Container(
              height: 200.rpx,
              alignment: Alignment(0, 0),
              child: Ink.image(
                fit: BoxFit.cover,
                image: NetworkImage(
                  "http://qh1ryrs63.hn-bkt.clouddn.com/20209220235.png",
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 28.rpx, right: 28.rpx),
      color: Colors.white,
      child: Column(
        children: [
          // _adsBlock(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                width: 332.rpx,
                height: 80.rpx,
                child: FlatButton(
                    shape: RoundedRectangleBorder(
                        // side: BorderSide(
                        //   color: Colors.white,
                        // ),
                        borderRadius:
                            BorderRadius.all(Radius.circular(16.rpx))),
                    color: Color(0xffF5F5F5),
                    onPressed: () {
                      // this.setState(() {
                      //   // _onlinePriority = !_onlinePriority;
                      // });
                      chooseGender(context, selectSex: (int index, String sex) {
                        print(index);
                        // print(sex);
                        this.setState(() {
                          this.gender = sex;
                        });
                      });
                    },
                    padding: EdgeInsets.all(0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          '$gender ',
                          style: TextStyle(
                              color: Color(0xff999999),
                              fontSize: 28.rpx,
                              fontWeight: FontWeight.w400),
                        ),
                        Image.asset(
                          'assets/images/square/GrayDown.png',
                          width: 28.rpx,
                          height: 28.rpx,
                        ),
                      ],
                    )),
              ),
              SizedBox(
                width: 332.rpx,
                height: 80.rpx,
                child: FlatButton(
                    shape: RoundedRectangleBorder(
                        // side: BorderSide(
                        //   color: Colors.white,
                        // ),
                        side: BorderSide.none,
                        borderRadius:
                            BorderRadius.all(Radius.circular(16.rpx))),
                    color: Color(0xffF5F5F5),
                    onPressed: () {
                      PickHelper.openCityPicker(context,
                          title: '选择所在行业',
                          name: 'province',
                          nextLevel: 'cites', onConfirm:
                              (List<String> stringData, List<int> selecteds) {
                        // this.industry.text = stringData.join('-');
                        // this.parem['trade'] = stringData.join('-');
                        var region = stringData[1];
                        this.setState(() {
                          this.region = region;
                        });
                      });
                    },
                    padding: EdgeInsets.all(0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          '$region ',
                          style: TextStyle(
                              color: Color(0xff999999),
                              fontSize: 28.rpx,
                              fontWeight: FontWeight.w400),
                        ),
                        Image.asset(
                          'assets/images/square/GrayDown.png',
                          width: 28.rpx,
                          height: 28.rpx,
                        ),
                      ],
                    )),
              ),
            ],
          ),
          Expanded(
            child: GestureDetector(
              onTap: () {
                this.setState(() {
                  hideComments = false;
                  if (widget.callback != null) {
                    widget.callback(true);
                  }
                });
              },
              child: RefreshIndicator(
                onRefresh: this.onRefresh,
                child: ListView.separated(
                    controller: this.scrollController,
                    separatorBuilder: (BuildContext context, int index) {
                      return Divider(color: Color(0xffF5F5F5), thickness: 2
                          // color: Colors.red,
                          );
                    },
                    itemCount: list.length,
                    itemBuilder: (BuildContext context, int index) {
                      Dynamics item = list[index];
                      return DynamicItem(
                        item,
                        onComment: (item) {
                          print(item.id);
                          this.setState(() {
                            if (widget.callback != null) {
                              widget.callback(this.hideComments);
                            }
                            this.hideComments = !this.hideComments;
                          });
                        },
                        onLike: (item) {
                          print(item.toJson());
                        },
                      );
                    }),
              ),
            ),
          ),
          Visibility(
            visible: hideComments,
            // maintainState: true,
            child: comments(),
          )
        ],
      ),
    );
  }
}
