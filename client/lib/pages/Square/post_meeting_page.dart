import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';
import 'package:huasheng_front_flutter/pages/widgets/upload/imgUpload.dart';
import 'package:huasheng_front_flutter/pages/square/widgets/meetingType.dart';
import 'package:city_pickers/city_pickers.dart';
import 'package:huasheng_front_flutter/pages/widgets/jhPickerTool.dart';

class PostMeetingPage extends StatefulWidget {
  final Map<String, dynamic> arguments;

  PostMeetingPage({
    this.arguments,
  });

  @override
  _PostMeetingPageState createState() => _PostMeetingPageState(arguments);
}

class _PostMeetingPageState extends State<PostMeetingPage> {
  final Map<String, dynamic> _arguments;
  _PostMeetingPageState(this._arguments) : super();
  bool _ablecomment = false;
  String _meetingType = '';
  String _address = '';
  String _time = '';
  DateTime selectedDate = DateTime.now();
  TimeOfDay selectedTime = TimeOfDay(hour: 9, minute: 30);

  _showDatePicker() {
    JhPickerTool.showDatePicker(
      context,
      dateType: DateType.YMD_HM,
      value: selectedDate,
      minValue: selectedDate,
      clickCallback: (var str, var time, DateTime dateTime) {
        setState(() {
          selectedDate = dateTime;
          _time = time.toString().replaceAll(RegExp(r'\..+'), '');
        });

        // this.setState(() {
        //   this._birthdayController.text = str;
        //   params['birth_year'] = dateTime.year.toInt();
        //   params['birth_month'] = dateTime.month.toInt();
        //   params['birth_day'] = dateTime.day.toInt();
        // });
      },
    );
  }

  final _inputTitleStyle = TextStyle(
    fontSize: 34.rpx,
    color: Color(0xFF333333),
    fontWeight: FontWeight.bold,
  );

  @override
  void initState() {
    super.initState();
    if (this._arguments != null) {
      _meetingType = this._arguments['meetingType'];
    }
  }

  Widget _inputTitle(title) {
    return Text(
      title,
      style: _inputTitleStyle,
    );
  }

  Widget _inputText(title, selected) {
    if (selected) {
      return Text(
        title,
        style: TextStyle(
          fontSize: 34.rpx,
          color: Color(0xFF333333),
        ),
      );
    }
    return Text(
      title,
      style: TextStyle(
        fontSize: 34.rpx,
        color: Color(0xFF999999),
      ),
    );
  }

  Widget _row(title, {onTap, textBuildWidget}) {
    return Container(
      height: 100.rpx,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          _inputTitle(title),
          InkWell(
            onTap: onTap,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                textBuildWidget(),
                Icon(
                  Icons.chevron_right,
                  size: 50.rpx,
                  color: Color(0xFF666666),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Color(0xFF333333),
        ),
        title: Text(
          '发布约会',
          style: TextStyle(
            color: Color(0xFF333333), //设置字体颜色
          ),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(left: 28.rpx, right: 28.rpx, top: 20.rpx),
        child: Column(
          children: [
            _row(
              '节目',
              onTap: () {
                openModalMeetingTypeSheet(
                  context,
                  (meetingType) {
                    if (meetingType != null) {
                      setState(() {
                        this._meetingType = meetingType;
                      });
                    }
                  },
                );
              },
              textBuildWidget: () {
                String actTitle = '请选择活动';
                bool selected = false;
                if (_meetingType != null && _meetingType.isEmpty == false) {
                  actTitle = MeetingTypeData[_meetingType];
                  selected = true;
                }

                return Wrap(
                  children: [
                    selected
                        ? Image(
                            image: AssetImage('assets/images/meeting/' +
                                _meetingType +
                                '.png'),
                            width: 50.rpx,
                            height: 50.rpx,
                            fit: BoxFit.cover,
                          )
                        : Container(),
                    SizedBox(
                      width: 10.rpx,
                    ),
                    _inputText(actTitle, selected),
                  ],
                );
              },
            ),
            _row(
              '地点',
              onTap: () async {
                Result city = await CityPickers.showFullPageCityPicker(
                    context: context, showType: ShowType.pc);

                setState(() {
                  this._address = city.cityName;
                });
              },
              textBuildWidget: () {
                String addressText = '请选择城市';
                bool selected = false;
                if (_address != null && _address.isEmpty == false) {
                  addressText = _address;
                  selected = true;
                }

                return _inputText(addressText, selected);
              },
            ),
            _row(
              '时间',
              onTap: () {
                _showDatePicker();

                //print('fdfdf');
              },
              textBuildWidget: () {
                String _timeText = '请选择时间';
                bool selected = false;
                if (_time != null && _time.isEmpty == false) {
                  _timeText = _time;
                  selected = true;
                }

                return _inputText(_timeText, selected);
              },
            ),
            Container(
                margin: EdgeInsets.only(top: 20.rpx),
                alignment: Alignment.centerLeft,
                child: Wrap(
                  alignment: WrapAlignment.start,
                  crossAxisAlignment: WrapCrossAlignment.center,
                  children: [
                    _inputTitle('说明'),
                    Text(
                      '(选填)',
                      style: TextStyle(
                        fontSize: 35.rpx,
                        color: Color(0xFF666666),
                      ),
                    )
                  ],
                )),
            TextField(
              style: TextStyle(
                fontSize: 32.rpx,
              ),
              minLines: 4,
              maxLines: 12,
              textAlign: TextAlign.left,
              decoration: InputDecoration(
                border: UnderlineInputBorder(borderSide: BorderSide.none),
                hintText: '输入说明',
                hintStyle:
                    TextStyle(color: Color(0xFF999999), fontSize: 34.rpx),
                // contentPadding: EdgeInsets.only(left: 20, top: 66.rpx),
              ),
            ),
            SizedBox(
              height: 10.rpx,
            ),
            ImgUpload(),
            SizedBox(
              height: 30.rpx,
            ),
            Row(
              children: [
                Image.asset(
                  'assets/images/square/exclamatoryMark.png',
                  width: 28.rpx,
                  height: 28.rpx,
                ),
                Padding(
                  padding: EdgeInsets.only(right: 10.rpx),
                ),
                Text(
                  '请忽上传裸露低俗的照片，严重者将做封号处理',
                  style: TextStyle(
                      color: Color(0xFF999999),
                      fontSize: 28.rpx,
                      fontWeight: FontWeight.w400),
                )
              ],
            ),
            SizedBox(
              height: 50.rpx,
            ),
            MergeSemantics(
              child: ListTile(
                contentPadding: EdgeInsets.all(0),
                title: Text('禁止评论', style: _inputTitleStyle),
                trailing: CupertinoSwitch(
                  value: _ablecomment,
                  activeColor: Color(0xFFf3d045),
                  onChanged: (bool value) {
                    setState(() {
                      _ablecomment = value;
                    });
                  },
                ),
                onTap: () {
                  setState(() {
                    _ablecomment = !_ablecomment;
                  });
                },
              ),
            ),
            MergeSemantics(
              child: ListTile(
                contentPadding: EdgeInsets.all(1),
                title: Text('对同性用户隐藏', style: _inputTitleStyle),
                trailing: CupertinoSwitch(
                  value: _ablecomment,
                  activeColor: Color(0xFFf3d045),
                  onChanged: (bool value) {
                    setState(() {
                      _ablecomment = value;
                    });
                  },
                ),
                onTap: () {
                  setState(() {
                    _ablecomment = !_ablecomment;
                  });
                },
              ),
            ),
            SizedBox(
              height: 60.rpx,
            ),
            Container(
              width: double.infinity,
              height: 110.rpx,
              padding: EdgeInsets.fromLTRB(60.rpx, 0, 60.rpx, 0),
              child: RaisedButton(
                color: Color(0xFFf3d045),
                elevation: 0,
                onPressed: () {},
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(50.rpx)),
                ),
                child: Text(
                  '提交',
                  style: TextStyle(
                    fontSize: 34.rpx,
                  ),
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                top: 30.rpx,
                bottom: 80.rpx,
              ),
              child: Text(
                '会员免费，非会员需支付12元',
                style: TextStyle(fontSize: 32.rpx, color: Color(0xFFFF3D3D)),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
