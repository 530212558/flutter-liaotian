import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';

class Photos extends StatefulWidget {
  bool isDetail;
  Photos({Key key, this.isDetail = false}) : super(key: key);
  @override
  _PhotosState createState() => _PhotosState(isDetail);
}

class _PhotosState extends State<Photos> {
  bool isDetail;
  _PhotosState(this.isDetail) : super();
  List<Widget> _buildTiles(int length) {
    return List.generate(length, (int index) {
      return Container(
        alignment: Alignment(0.0, 0.0),
        decoration: BoxDecoration(
          color: Colors.grey[300],
          borderRadius: BorderRadius.circular(20.rpx),
          image: DecorationImage(
              image: NetworkImage(
                'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=1756183169,2200257038&fm=26&gp=0.jpg',
              ),
              fit: BoxFit.cover),
        ),
        child: null,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    int _crossAxisCount = 4;
    if (isDetail) {
      _crossAxisCount = 3;
    }
    return Container(
      child: GridView.count(
        padding: EdgeInsets.only(bottom: 0),
        shrinkWrap: true,
        physics: new NeverScrollableScrollPhysics(),
        crossAxisCount: _crossAxisCount,
        crossAxisSpacing: 20.rpx,
        mainAxisSpacing: 20.rpx,
        scrollDirection: Axis.vertical,
        children: _buildTiles(6),
      ),
    );
  }
}
