import 'dart:math';
import 'package:flutter/cupertino.dart' as IOS;
import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/pages/widgets/customPull/refreshSliver.dart';
import 'package:huasheng_front_flutter/pages/square/widgets/meetingListItem.dart';

class MeetingList extends StatefulWidget {
  final sliverToBoxAdapter;
  MeetingList({Key key, this.sliverToBoxAdapter}) : super(key: key);
  @override
  _MeetingListState createState() => _MeetingListState(sliverToBoxAdapter);
}

class _MeetingListState extends State<MeetingList> {
  final GlobalKey<CupertinoSliverRefreshControlState> sliverRefreshKey =
      GlobalKey<CupertinoSliverRefreshControlState>();
  // 列表项
  final _sliverToBoxAdapter;

  final int pageSize = 30;

  bool disposed = false;

  List<String> dataList = new List();

  final ScrollController _scrollController = new ScrollController();
  _MeetingListState(this._sliverToBoxAdapter) : super();

  Future<void> onRefresh() async {
    await Future.delayed(Duration(milliseconds: 500));
    dataList.clear();
    for (int i = 0; i < pageSize; i++) {
      dataList.add("refresh");
    }
    if (disposed) {
      return;
    }
    setState(() {});
  }

  Future<void> loadMore() async {
    await Future.delayed(Duration(milliseconds: 100));
    for (int i = 0; i < pageSize; i++) {
      dataList.add("loadmore");
    }
    if (disposed) {
      return;
    }
    setState(() {});
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    loadMore();
    // ///直接触发下拉
    // new Future.delayed(const Duration(milliseconds: 500), () {
    //   _scrollController.animateTo(-141,
    //       duration: Duration(milliseconds: 600), curve: Curves.linear);
    //   return true;
    // });
  }

  @override
  void dispose() {
    disposed = true;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return NotificationListener<ScrollNotification>(
      onNotification: (ScrollNotification notification) {
        ///通知 CupertinoSliverRefreshControl 当前的拖拽状态
        sliverRefreshKey.currentState.notifyScrollNotification(notification);

        ///判断当前滑动位置是不是到达底部，触发加载更多回调
        if (notification is ScrollEndNotification) {
          if (_scrollController.position.pixels > 0 &&
              _scrollController.position.pixels ==
                  _scrollController.position.maxScrollExtent) {
            loadMore();
          }
        }
        return false;
      },
      child: CustomScrollView(
        controller: _scrollController,
        physics: const BouncingScrollPhysics(
            parent: AlwaysScrollableScrollPhysics()),
        slivers: <Widget>[
          ///控制显示刷新的 CupertinoSliverRefreshControl
          CupertinoSliverRefreshControl(
            key: sliverRefreshKey,
            refreshIndicatorExtent: 100,
            refreshTriggerPullDistance: 140,
            onRefresh: onRefresh,
            builder: buildSimpleRefreshIndicator,
          ),
          SliverToBoxAdapter(
            child: _sliverToBoxAdapter,
          ),

          ///列表区域
          SliverSafeArea(
            sliver: SliverList(
              ///代理显示
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
                  if (index == dataList.length) {
                    return new Container(
                      margin: EdgeInsets.all(10),
                      child: Align(
                        child: CircularProgressIndicator(),
                      ),
                    );
                  }
                  return MeetingListItem(index: index);
                },
                childCount: (dataList.length >= pageSize)
                    ? dataList.length + 1
                    : dataList.length,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Widget buildSimpleRefreshIndicator(
  BuildContext context,
  RefreshIndicatorMode refreshState,
  double pulledExtent,
  double refreshTriggerPullDistance,
  double refreshIndicatorExtent,
) {
  const Curve opacityCurve = Interval(0.4, 0.8, curve: Curves.easeInOut);
  return Align(
    alignment: Alignment.center,
    child: Padding(
      padding: const EdgeInsets.only(top: 0.0, bottom: 0.0),
      child: refreshState != RefreshIndicatorMode.refresh
          ? Opacity(
              opacity: opacityCurve.transform(
                  min(pulledExtent / refreshTriggerPullDistance, 1.0)),
              child: const Icon(
                IOS.CupertinoIcons.down_arrow,
                color: IOS.CupertinoColors.inactiveGray,
                size: 36.0,
              ),
            )
          : Opacity(
              opacity: opacityCurve
                  .transform(min(pulledExtent / refreshIndicatorExtent, 1.0)),
              child: const IOS.CupertinoActivityIndicator(radius: 14.0),
            ),
    ),
  );
}
