import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';
import 'package:huasheng_front_flutter/pages/square/widgets/meetingListItemPhoto.dart';

class MeetingListItem extends StatefulWidget {
  final index;
  bool isDetail;
  MeetingListItem({Key key, this.index, this.isDetail = false})
      : super(key: key);

  @override
  _MeetingListItemState createState() => _MeetingListItemState(index, isDetail);
}

class _MeetingListItemState extends State<MeetingListItem> {
  final index;
  bool isDetail;
  _MeetingListItemState(this.index, this.isDetail) : super();
  Widget _headerButtonMine(index) {
    if (index == 0) {
      return Container(
        height: 60.rpx,
        child: PopupMenuButton(
          color: Color(0xFFF5F5F5),
          elevation: 0,
          offset: Offset(0.rpx, 80.rpx),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(50.rpx),
              topRight: Radius.circular(0.rpx),
              bottomLeft: Radius.circular(50.rpx),
              bottomRight: Radius.circular(50.rpx),
            ),
          ),
          icon: Icon(
            IconData(0xe67f, fontFamily: 'iconfont'),
            color: Color(0xFF363636),
            size: 40.rpx,
          ),
          itemBuilder: (BuildContext context) => [
            PopupMenuItem(
              child: Container(
                alignment: Alignment.center,
                child: Text('禁止评论'),
              ),
            ),
            PopupMenuItem(
              height: 2.rpx,
              enabled: false,
              child: Container(
                height: 2.rpx,
                color: Colors.white,
              ),
            ),
            PopupMenuItem(
              child: Container(
                alignment: Alignment.center,
                child: Text('删除约会'),
              ),
            ),
          ],
        ),
      );
    }
    return SizedBox();
  }

  Widget _headerFace() {
    return SizedBox(
      width: 100.rpx,
      height: 100.rpx,
      child: CircleAvatar(
        backgroundImage: NetworkImage(
            'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=2471193303,3938768064&fm=26&gp=0.jpg'),
      ),
    );
  }

  Widget _header(index) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              _headerFace(),
              SizedBox(
                width: 28.rpx,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Text(
                        '好看的Riley',
                        style: TextStyle(
                          fontSize: 32.rpx,
                          fontWeight: FontWeight.bold,
                          color: Color(0xFF333333),
                        ),
                      ),
                      SizedBox(
                        width: 10.rpx,
                      ),
                      SizedBox(
                        child: Icon(
                          IconData(0xe643, fontFamily: 'iconfont'),
                          color: Colors.pink[200],
                          size: 38.rpx,
                        ),
                      ),
                      index == 0
                          ? Container(
                              margin: EdgeInsets.only(left: 10.rpx),
                              padding: EdgeInsets.fromLTRB(
                                  12.rpx, 2.rpx, 12.rpx, 2.rpx),
                              decoration: BoxDecoration(
                                color: Color(0xFFDCF7F6),
                                borderRadius: BorderRadius.circular(20.rpx),
                              ),
                              child: Text(
                                '我发布的',
                                style: TextStyle(
                                  color: Color(0xFF1AD8D0),
                                ),
                              ),
                            )
                          : SizedBox(),
                    ],
                  ),
                  SizedBox(
                    height: 5.rpx,
                  ),
                  Text(
                    '2020/02/22 11:02:12',
                    style: TextStyle(
                      fontSize: 30.rpx,
                      color: Color(0xFF999999),
                    ),
                  )
                ],
              ),
            ],
          ),
          _headerButtonMine(index),
        ]);
  }

  Widget _contentRow(String title, String desc) {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            alignment: Alignment.topLeft,
            height: 60.rpx,
            child: Text(
              title + ':',
              style: TextStyle(
                fontSize: 32.rpx,
                color: Color(0xFF999999),
              ),
            ),
            width: 88.rpx,
          ),
          Container(
            width: 450.rpx,
            alignment: Alignment.topLeft,
            child: Text(
              desc,
              style: TextStyle(
                fontSize: 32.rpx,
                color: Color(0xFF333333),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _content() {
    _navigationTo() {
      return () {
        if (isDetail) {
          return;
        }
        Navigator.pushNamed(context, '/meetingDetailPage');
      };
    }

    return InkWell(
      onTap: _navigationTo(),
      child: Container(
        padding: EdgeInsets.only(top: 38.rpx),
        child: Column(
          children: [
            _contentRow('节目', '地下rap'),
            _contentRow('地点', '地点：深圳市90音乐大厦'),
            _contentRow('时间', '2020/09/09 晚上'),
            _contentRow('说明', '带上你的嘻哈装扮，让我们一起嗨起来！'),
          ],
        ),
      ),
    );
  }

  _bottomBarTextStyle(abled) {
    if (abled) {
      return TextStyle(
        color: Color(0xFF333333),
        letterSpacing: 0,
        fontSize: 32.rpx,
      );
    }
    return TextStyle(
      color: Color(0xFF999999),
      letterSpacing: 0,
      fontSize: 32.rpx,
    );
  }

  Widget _zan(abled) {
    if (abled) {
      return FlatButton.icon(
        onPressed: null,
        icon: Icon(
          IconData(0xe600, fontFamily: 'iconfont'),
          size: 50.rpx,
          color: Color(0xFF333333),
        ),
        label: Text('123', style: _bottomBarTextStyle(true)),
      );
    }
    return FlatButton.icon(
      onPressed: null,
      icon: Icon(
        IconData(0xe600, fontFamily: 'iconfont'),
        size: 45.rpx,
        color: Color(0xFF999999),
      ),
      label: Text('123', style: _bottomBarTextStyle(false)),
    );
  }

  Widget _comment(abled) {
    if (abled) {
      return FlatButton.icon(
        onPressed: () {},
        icon: Icon(
          IconData(0xe602, fontFamily: 'iconfont'),
          size: 45.rpx,
          color: Color(0xFF333333),
        ),
        label: Text('123', style: _bottomBarTextStyle(true)),
      );
    }
    return FlatButton.icon(
      onPressed: null,
      icon: Icon(
        IconData(0xe602, fontFamily: 'iconfont'),
        size: 45.rpx,
        color: Color(0xFF999999),
      ),
      label: Text('123', style: _bottomBarTextStyle(false)),
    );
  }

  Widget join(abled) {
    if (abled) {
      return RaisedButton(
        elevation: 0,
        shape: StadiumBorder(),
        onPressed: () {},
        padding: EdgeInsets.only(left: 20.rpx, right: 20.rpx),
        color: Color(0xFFFFD132),
        child: Text(
          '报名 (12)',
          textAlign: TextAlign.center,
          style: _bottomBarTextStyle(true),
        ),
      );
    }
    return FlatButton.icon(
      onPressed: null,
      icon: Icon(
        Icons.add_circle,
        size: 40.rpx,
        color: Color(0xFF333333),
      ),
      label: Text(
        '123',
        style: _bottomBarTextStyle(true),
      ),
    );
  }

  Widget _bottomBar(index) {
    if (index == 0) {
      return Container(
        margin: EdgeInsets.only(top: 20.rpx),
        child: ButtonBar(
          children: [
            _zan(false),
            _comment(false),
            join(false),
          ],
        ),
      );
    }

    return Container(
      margin: EdgeInsets.only(top: 20.rpx),
      child: ButtonBar(
        children: [
          _zan(true),
          _comment(true),
          join(true),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topLeft,
      margin: EdgeInsets.only(bottom: 6.rpx),
      padding: EdgeInsets.fromLTRB(28.rpx, 28.rpx, 28.rpx, 0),
      color: Colors.white,
      child: Column(children: [
        _header(index),
        _content(),
        SizedBox(height: 40.rpx),
        Photos(isDetail: isDetail),
        _bottomBar(index)
      ]),
    );
  }
}
