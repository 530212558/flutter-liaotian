import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';

Map<String, String> MeetingTypeData = {
  "changge": "唱歌",
  "kandianying": "看电影",
  "wanyouxi": "玩游戏",
  "yundong": "运动",
  "chihe": "吃喝",
  "yepu": "夜蒲",
  "lvyou": "旅游",
  "qita": "其他",
};

Future openModalMeetingTypeSheet(context, onChange) async {
  buildExpanded(context, title, value) {
    return Expanded(
      flex: 4,
      child: FlatButton(
        onPressed: () {
          Navigator.pop(context, value);
        },
        child: Column(
          children: [
            Image(
              image: AssetImage('assets/images/meeting/' + value + '.png'),
              width: 105.rpx,
              height: 105.rpx,
              fit: BoxFit.cover,
            ),
            SizedBox(
              height: 10.rpx,
            ),
            Text(
              title,
              style: TextStyle(
                fontSize: 30.rpx,
                fontWeight: FontWeight.normal,
                color: Color(0xFF999999),
              ),
            ),
          ],
        ),
      ),
    );
  }

  final option = await showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return Stack(children: [
          Container(
            height: 25,
            width: double.infinity,
            color: Colors.black54,
          ),
          Container(
            height: 660.rpx,
            width: double.infinity,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30.rpx),
                topRight: Radius.circular(30.rpx),
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    buildExpanded(context, '唱歌', 'changge'),
                    buildExpanded(context, '看电影', 'kandianying'),
                    buildExpanded(context, '玩游戏', 'wanyouxi'),
                    buildExpanded(context, '运动', 'yundong'),
                  ],
                ),
                SizedBox(
                  height: 30.rpx,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    buildExpanded(context, '吃喝', 'chihe'),
                    buildExpanded(context, '夜蒲', 'yepu'),
                    buildExpanded(context, '旅游', 'lvyou'),
                    buildExpanded(context, '其他', 'qita'),
                  ],
                ),
                SizedBox(
                  height: 40.rpx,
                ),
                Divider(),
                FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 80.rpx,
                    child: Text('取消',
                        style: TextStyle(
                          fontSize: 36.rpx,
                          fontWeight: FontWeight.normal,
                          color: Color(0xFF999999),
                        )),
                  ),
                ),
              ],
            ),
          ),
        ]);
      });

  onChange(option);
}
