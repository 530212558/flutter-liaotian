import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';
import 'package:huasheng_front_flutter/pages/square/widgets/meetingList.dart';

class MeetingPage extends StatefulWidget {
  @override
  _MeetingPageState createState() => _MeetingPageState();
}

class _MeetingPageState extends State<MeetingPage> {
  _filterBtns() {
    return Padding(
      padding: EdgeInsets.fromLTRB(28.rpx, 0.rpx, 28.rpx, 20.rpx),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            color: Color(0xFFF5F5F5),
            alignment: Alignment.center,
            width: 220.rpx,
            height: 80.rpx,
            child: DropdownButton(
              hint: Text('不限性别'),
              underline: Container(height: 0),
              items: [
                DropdownMenuItem(child: Text('不限性别')),
                DropdownMenuItem(child: Text('男')),
                DropdownMenuItem(child: Text('女')),
              ],
              onChanged: (value) {},
            ),
          ),
          Container(
            color: Color(0xFFF5F5F5),
            padding: EdgeInsets.only(left: 20.rpx),
            width: 220.rpx,
            height: 80.rpx,
            child: DropdownButton(
              hint: Text('不限地区'),
              underline: Container(height: 0),
              items: [
                DropdownMenuItem(child: Text('不限地区')),
                DropdownMenuItem(child: Text('北京')),
                DropdownMenuItem(child: Text('天津')),
                DropdownMenuItem(child: Text('河北')),
              ],
              onChanged: (value) {},
            ),
          ),
          Container(
            color: Color(0xFFF5F5F5),
            padding: EdgeInsets.only(left: 20.rpx),
            width: 220.rpx,
            height: 80.rpx,
            child: DropdownButton(
              hint: Text('发布时间'),
              underline: Container(height: 0),
              items: [
                DropdownMenuItem(child: Text('今天')),
                DropdownMenuItem(child: Text('一周内')),
                DropdownMenuItem(child: Text('一个月内')),
              ],
              onChanged: (value) {},
            ),
          ),
        ],
      ),
    );
  }

  _adsBlock() {
    return Padding(
      padding: EdgeInsets.fromLTRB(28.rpx, 20.rpx, 28.rpx, 20.rpx),
      child: Material(
        child: Ink(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(18.rpx)),
          ),
          child: InkWell(
            onTap: () {},
            child: Container(
              height: 200.rpx,
              alignment: Alignment(0, 0),
              child: Ink.image(
                fit: BoxFit.cover,
                image: NetworkImage(
                  "http://qh1ryrs63.hn-bkt.clouddn.com/20209220235.png",
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MeetingList(
      sliverToBoxAdapter: Container(
        color: Colors.white,
        child: Column(
          children: [
            // _adsBlock(),
            _filterBtns(),
          ],
        ),
      ),
    );
  }
}
