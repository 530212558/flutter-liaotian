import 'package:amap_location/amap_location.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:huasheng_front_flutter/http/news.dart';
import 'package:huasheng_front_flutter/http/public.dart';
import 'package:huasheng_front_flutter/http/socket/news.dart';
import 'package:huasheng_front_flutter/models/FriendsNewsList.dart';
import 'package:huasheng_front_flutter/models/userInfo.dart';
import 'package:huasheng_front_flutter/providers/FriendsNewsList.dart';
import 'package:huasheng_front_flutter/providers/GlobalData.dart';
import 'package:huasheng_front_flutter/providers/UserInfo.dart';
import 'package:huasheng_front_flutter/models/model_utils.dart';
import 'package:huasheng_front_flutter/pages/home/home_page.dart';
import 'package:huasheng_front_flutter/pages/messages/messages_page.dart';
import 'package:huasheng_front_flutter/pages/mine/mine_page.dart';
import 'package:huasheng_front_flutter/pages/square/square_page.dart';

import 'package:huasheng_front_flutter/pages/widgets/Loading.dart';
import 'package:huasheng_front_flutter/utils/authorityCheck.dart';
import 'package:provider/provider.dart';
import 'package:huasheng_front_flutter/pages/widgets/loadingIndicator/default.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TabsPage extends StatefulWidget {
  final Map<String, dynamic> arguments;

  TabsPage({
    this.arguments,
  });

  _TabsPageState createState() {
    int index = 0;
    if (arguments != null) {
      index = parseToInt(arguments['index']);
    }

    return _TabsPageState(index);
  }
}

class _TabsPageState extends State<TabsPage> {
  int _currentIndex;
  final List<Widget> _pagesList = [
    HomePage(),
    SquarePage(),
    MessagesPage(),
    MinePage(),
  ];

  _TabsPageState(index) {
    this._currentIndex = index;
  }

  void _checkUserStatus() async {
    SharedPreferences getInstance = await SharedPreferences.getInstance();
    var str = getInstance.getString('userInfo');
    if (str != null) {
      UserInfo userInfo = userInfoFromJson(str);

      if (userInfo.info == null) {
        // Provider.of<UserInfoProvider>(context, listen: false)
        //     .setUserInfo(userInfo);
        Navigator.pushReplacementNamed(context, '/SelectGenderPage');
      } else {
        setLocation(userInfo);
      }
      // Navigator.pushReplacementNamed(context, '/');
    } else {
      Navigator.pushNamed(context, '/LoginPage');
    }
  }

  getIndustry() {
    requestIndustry(onSuccess: (t) {
      GlobalData.setIndustry(t);
    });
  }

  getChinaArea() {
    requestChinaArea(onSuccess: (t) {
      GlobalData.setChinaArea(t);
    });
  }

  setLocation(UserInfo userInfo) async {
    bool result = await Permissions.getLocation();
    if (result) {
      AMapLocation location = await AMapLocationClient.getLocation(true);
      if (location.longitude != null && location.latitude != null) {
        if (userInfo.info != null) {
          userInfo.info.longitude = location.longitude;
          userInfo.info.latitude = location.latitude;
        }
      }
    }
    Provider.of<UserInfoProvider>(context, listen: false).setUserInfo(userInfo);
  }

  @override
  void initState() {
    super.initState();
    getIndustry();
    getChinaArea();

    Future.delayed(Duration.zero, () {
      _checkUserStatus();
    });
  }

  @override
  void dispose() {
    super.dispose();
    NewsIo.onClose();
  }

  bool init = true;

  @override
  Widget build(BuildContext context) {
    Loading.ctx = context; // 注入context
    UserInfo userInfo =
        Provider.of<UserInfoProvider>(context, listen: true).userInfo;
    if (userInfo.token == null || userInfo.info == null) {
      return Scaffold(
        body: LoadingIndicator(),
      );
    }
    if (init) {
      NewsIo.init(query: userInfo.toJson());
      requesGetFriendsNewsList(userInfo.id, onSuccess: (data) {
        // print(friendsNewsListToJson(data));
        Provider.of<FriendsNewsListProvider>(context, listen: false)
            .setFriendsNewsList(data);
      });
    }
    init = false;

    return Scaffold(
      // body: _pagesList[_currentIndex],
      body: IndexedStack(
        index: _currentIndex,
        children: _pagesList,
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: (index) {
          if (mounted) {
            setState(() {
              _currentIndex = index;
            });
          }
        },
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Image.asset('assets/images/tab_items/home_n.png'),
            activeIcon: Image.asset('assets/images/tab_items/home_s.png'),
            label: ('首页'),
          ),
          BottomNavigationBarItem(
            icon: Image.asset('assets/images/tab_items/square_n.png'),
            activeIcon: Image.asset('assets/images/tab_items/square_s.png'),
            label: ('广场'),
          ),
          BottomNavigationBarItem(
            icon: Image.asset('assets/images/tab_items/messages_n.png'),
            activeIcon: Image.asset('assets/images/tab_items/messages_s.png'),
            label: ('消息'),
          ),
          BottomNavigationBarItem(
            icon: Image.asset('assets/images/tab_items/mine_n.png'),
            activeIcon: Image.asset('assets/images/tab_items/mine_s.png'),
            label: ('我的'),
          )
        ],
        selectedItemColor: Color(0xFF333333),
        unselectedItemColor: Color(0xFF999999),
        selectedFontSize: 12,
        unselectedFontSize: 12,
      ),
    );
  }
}
