import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/models/model_utils.dart';
import 'package:huasheng_front_flutter/pages/home/home_page.dart';
import 'package:huasheng_front_flutter/pages/messages/messages_page.dart';
import 'package:huasheng_front_flutter/pages/mine/mine_page.dart';
import 'package:huasheng_front_flutter/pages/square/square_page.dart';

class TabsPage extends StatefulWidget {
  final Map<String, dynamic> arguments;

  TabsPage({
    this.arguments,
  });

  _TabsPageState createState() {
    int index = 0;
    if (arguments != null) {
      index = parseToInt(arguments['index']);
    }

    return _TabsPageState(index);
  }
}

class _TabsPageState extends State<TabsPage> {
  int _currentIndex;
  List _pagesList = [
    HomePage(),
    SquarePage(),
    MessagesPage(),
    MinePage(),
  ];

  _TabsPageState(index) {
    this._currentIndex = index;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _pagesList[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: (index) {
          if (mounted) {
            setState(() {
              _currentIndex = index;
            });
          }
        },
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            icon: Image.asset('assets/images/tab_items/home_n.png'),
            activeIcon: Image.asset('assets/images/tab_items/home_s.png'),
            label: '首页',
          ),
          BottomNavigationBarItem(
            icon: Image.asset('assets/images/tab_items/square_n.png'),
            activeIcon: Image.asset('assets/images/tab_items/square_s.png'),
            label: '广场',
          ),
          BottomNavigationBarItem(
            icon: Image.asset('assets/images/tab_items/messages_n.png'),
            activeIcon: Image.asset('assets/images/tab_items/messages_s.png'),
            label: '消息',
          ),
          BottomNavigationBarItem(
            icon: Image.asset('assets/images/tab_items/mine_n.png'),
            activeIcon: Image.asset('assets/images/tab_items/mine_s.png'),
            label: '我的',
          )
        ],
        selectedItemColor: Color(0xFF333333),
        unselectedItemColor: Color(0xFF999999),
        selectedFontSize: 12,
        unselectedFontSize: 12,
      ),
    );
  }
}
