import 'dart:io';

import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/http/dio.dart';
import 'package:huasheng_front_flutter/http/user.dart';
import 'package:huasheng_front_flutter/models/userInfo.dart';
import 'package:huasheng_front_flutter/pages/widgets/PhotoView.dart';
import 'package:huasheng_front_flutter/pages/widgets/public_ui.dart';
import 'package:huasheng_front_flutter/providers/UserInfo.dart';
import 'package:huasheng_front_flutter/style/index.dart';
import 'package:huasheng_front_flutter/utils/Global.dart';
import 'package:provider/provider.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';

class MinePage extends StatefulWidget {
  MinePage({Key key}) : super(key: key);

  _MinePageState createState() => _MinePageState();
}

class _MinePageState extends State<MinePage> {
  List<Widget> _images = [];
  List<String> _files = [];

  _imagesUpdate() {
    List<Widget> images = [];
    int index = 0;
    images = _files.map((String item) {
      int count = index;
      Widget widget = GestureDetector(
          onTap: () {
            Navigator.of(context).push(PageRouteBuilder(
                pageBuilder: (c, a, s) => PhotoView(
                      _files,
                      initialIndex: count,
                    )));
          },
          child: Image.network(
            item,
            width: 160.rpx,
            height: 160.rpx,
            fit: BoxFit.cover,
            // fit: BoxFit.scaleDown,
          ));
      index += 1;
      return widget;
    }).toList();
    if (images.length >= 1) {
      this.setState(() {
        _images = images;
      });
    }
  }

  _addImages() async {
    List<File> paths = await getImages(context);
    int num = 0;
    List<dynamic> album = [];
    UserInfo userInfo =
        Provider.of<UserInfoProvider>(context, listen: false).userInfo;
    paths.forEach((item) {
      Dios.uploadImage(File(item.path), images: userInfo.phone + '/album/',
          success: (String url) {
        num += 1;
        _files.add(url);
        album.add({'url': url, 'user_id': userInfo.id, 'purpose': 'my_album'});
        if (num >= paths.length) {
          _imagesUpdate();
          requesSaveAlbum(album, onSuccess: () {
            toast('上传完成。。。');
          });
        }
      });
    });
  }

  _getAlbum() {
    UserInfo userInfo =
        Provider.of<UserInfoProvider>(context, listen: false).userInfo;
    requesGetAlbum(userInfo.id, 'my_album', onSuccess: (data) {
      int num = 0;
      data.forEach((item) {
        _files.add(item.url);
        num += 1;
        if (num >= data.length) {
          _imagesUpdate();
        }
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _getAlbum();
  }

  @override
  Widget build(BuildContext context) {
    var userInfo =
        Provider.of<UserInfoProvider>(context, listen: true).userInfo;
    DateTime dateTime = DateTime.now();
    int yearNow = dateTime.year; //当前年份
    return Scaffold(
        body: ConstrainedBox(
      constraints: BoxConstraints.expand(),
      child: Stack(
        // alignment: Alignment.center, //指定未定位或部分定位widget的对齐方式
        children: <Widget>[
          Container(
            height: 208.rpx,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/mine/xiaolian_bg.png"),
                fit: BoxFit.cover,
              ),
            ),
          ),

          Positioned(
            width: 750.rpx,
            // height: 200.rpx,
            top: 100.rpx,
            bottom: 0,

            child: Container(
              padding: EdgeInsets.only(top: 76.rpx),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(32.rpx),
                      topRight: Radius.circular(32.rpx))),
              child: ListView(
                shrinkWrap: true,
                children: [
                  Container(
                    padding: EdgeInsets.only(bottom: 40.rpx),
                    decoration:
                        BoxDecoration(border: Border(bottom: default_border())),
                    child: userInfo.token == null
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  Navigator.pushNamed(context, '/LoginPage');
                                },
                                child: Text(
                                  '登录/',
                                  style: TextStyle(
                                      fontSize: 40.rpx,
                                      color: Color(0xff333333),
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.pushNamed(context, '/RegisterPage');
                                },
                                child: Text(
                                  '注册',
                                  style: TextStyle(
                                      fontSize: 40.rpx,
                                      color: Color(0xff333333),
                                      fontWeight: FontWeight.w500),
                                ),
                              )
                            ],
                          )
                        : Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    userInfo.info != null
                                        ? userInfo.info.nickname
                                        : 'userName',
                                    style: title_1(),
                                  ),
                                  Image.asset(
                                    'assets/images/home/vip.png',
                                    width: 50.rpx,
                                    height: 28.rpx,
                                  )
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  userInfo.token != null
                                      ? Text(
                                          "${yearNow - userInfo.info.birthYear}岁 • ${userInfo.info.career}",
                                          style: title_1_auxiliary(),
                                        )
                                      : '',
                                ],
                              ),
                              Container(
                                width: 80.rpx,
                                height:
                                    36.rpx, //  auth 是否已认证，1:未认证，2:已真人认证，3:已女神认证
                                decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                        colors: 2 == 2
                                            ? [
                                                Color(0xff1AD8D0),
                                                Color(0xff1AD89E)
                                              ]
                                            : [
                                                Color(0xffFE7C96),
                                                Color(0xffFE997C)
                                              ]), // 渐变色
                                    borderRadius:
                                        BorderRadius.circular(24.rpx)),
                                child: FlatButton(
                                  padding: EdgeInsets.all(0),
                                  onPressed: () {},
                                  child: Text(
                                    2 == 2 ? '真人' : '女神',
                                    style: TextStyle(
                                        color: Color(0xffffffff),
                                        fontSize: 24.rpx),
                                  ),
                                ),
                              ),
                            ],
                          ),
                  ),

                  Container(
                    padding: EdgeInsets.all(28.rpx),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      // border: Border(
                      //     bottom: BorderSide(
                      //         width: 20.rpx, color: Color(0xfff5f5f5)))
                    ),
                    child: Column(children: [
                      Container(
                        padding: EdgeInsets.only(bottom: 28.rpx),
                        decoration: BoxDecoration(
                          color: Colors.white,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              "我的相册",
                              style: title_1(),
                            ),
                            Text(
                              "上传照片",
                              style: title_1_auxiliary(color: 0xff1AD8D0),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        // height: 204.rpx,
                        // alignment: Alignment.center,
                        child: Provider.of<UserInfoProvider>(context,
                                        listen: true)
                                    .userInfo
                                    .token !=
                                null
                            ? Wrap(
                                spacing: 18.rpx, // 主轴(水平)方向间距
                                runSpacing: 18.rpx, // 纵轴（垂直）方向间距
                                // crossAxisAlignment: WrapCrossAlignment.start,
                                // alignment: WrapAlignment.end, //沿主轴方向居中
                                children: <Widget>[
                                  ..._images,
                                  GestureDetector(
                                    onTap: () {
                                      _addImages();
                                    },
                                    child: Container(
                                      margin: EdgeInsets.only(bottom: 28.rpx),
                                      child: Image.asset(
                                        'assets/images/square/AddPhotos.png',
                                        width: 160.rpx,
                                        height: 160.rpx,
                                      ),
                                    ),
                                  ),
                                ],
                              )
                            : Container(
                                height: 204.rpx,
                                alignment: Alignment.center,
                                child: Text(
                                  '登录账号查看照片',
                                  style: title_1_auxiliary(fontSize: 32),
                                ),
                              ),
                      )
                    ]),
                  ),
                  Container(
                    padding: EdgeInsets.all(28.rpx),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border(
                            bottom: default_border(width: 20),
                            top: default_border(width: 20))),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              "相册隐私",
                              style: title_1(),
                            ),
                            Row(
                              children: [
                                Container(
                                  margin: EdgeInsets.only(right: 16.rpx),
                                  child: Text(
                                    "公开",
                                    style: title_1_auxiliary(color: 0xff1AD8D0),
                                  ),
                                ),
                                Image.asset(
                                  'assets/images/public/icon_right.png',
                                  width: 28.rpx,
                                  height: 28.rpx,
                                )
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                  // FlatButton(
                  //   onPressed: () {
                  //     Navigator.pushNamed(context, '/UserPage');
                  //   },
                  //   child: Text('跳转用户详情页'),
                  // ),
                  Container(
                    padding: EdgeInsets.all(28.rpx),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border(
                          bottom: default_border(width: 20),
                        )),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "我的相册",
                          style: title_1(),
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              margin: EdgeInsets.only(right: 16.rpx),
                              child: Text(
                                "公开",
                                style: title_1_auxiliary(),
                              ),
                            ),
                            Image.asset(
                              'assets/images/public/icon_right.png',
                              width: 28.rpx,
                              height: 28.rpx,
                            )
                          ],
                        )
                      ],
                    ),
                  ),

                  FlatButton(
                    onPressed: () {
                      // AccountManager().clearLoginInfo();
                      Provider.of<UserInfoProvider>(context, listen: false)
                          .logout();
                      Navigator.pushNamed(context, '/LoginPage');
                    },
                    child: Text('退出登录'),
                  ),
                  Container(
                    decoration: BoxDecoration(
                        border: Border(bottom: default_border(width: 20))),
                  )
                ],
              ),
            ),
          ),

          Positioned(
            width: 750.rpx,
            // height: 200.rpx,
            top: 60.rpx,
            child: Container(
                child: Provider.of<UserInfoProvider>(context, listen: true)
                            .userInfo
                            .info !=
                        null
                    ? Image.network(
                        Provider.of<UserInfoProvider>(context, listen: true)
                            .userInfo
                            .info
                            .avatar,
                        width: 160.rpx,
                        height: 160.rpx,
                      )
                    : Image.asset(
                        'assets/images/public/default_avatar.png',
                        width: 160.rpx,
                        height: 160.rpx,
                      )),
          ),

          // FlatButton(
          //   onPressed: () {
          //     // AccountManager().clearLoginInfo();
          //     Provider.of<UserInfoProvider>(context, listen: false).logout();
          //     Navigator.pushNamed(context, '/LoginPage');
          //   },
          //   child: Text('退出登录'),
          // ),
          // Positioned(
          //   top: 18.0,
          //   child: Text("Your friend"),
          // )
        ],
      ),
    ));
  }
}
