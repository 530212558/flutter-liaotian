import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/http/api_defines.dart';
import 'package:huasheng_front_flutter/http/api_manager.dart';
import 'package:huasheng_front_flutter/pages/widgets/progress_hud.dart';
import 'package:huasheng_front_flutter/pages/widgets/public_ui.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';

class ResetPasswordPage extends StatefulWidget {
  final Map<String, dynamic> arguments;

  ResetPasswordPage({
    this.arguments,
  });

  @override
  _ResetPasswordPageState createState() => _ResetPasswordPageState();
}

class _ResetPasswordPageState extends State<ResetPasswordPage> {
  String _progressHUDText;
  bool _loading = false;
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _repasswordController = TextEditingController();
  bool _passwordVisible = false;

  _showProgressHUD(String text) {
    if (mounted) {
      setState(() {
        _progressHUDText = text;
        _loading = true;
      });
    }
  }

  _dismissProgressHUD() {
    if (mounted) {
      setState(() {
        _loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return CustomProgressHUD(
      progressHUDText: _progressHUDText,
      loading: _loading,
      child: Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          backgroundColor: Colors.transparent,
          elevation: 0,
          toolbarHeight: 44,
          leading: IconButton(
            icon: Image.asset('assets/images/login/back.png'),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Text(
            '重置密码',
            style: TextStyle(
              color: Color(0xFF333333),
              fontSize: 17,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Center(
            child: Container(
              width: 590.0.rpx,
              child: Column(
                children: <Widget>[
                  Padding(padding: EdgeInsets.only(bottom: 80.rpx)),
                  ConstrainedBox(
                    constraints: BoxConstraints(
                      maxHeight: 100.rpx,
                    ),
                    child: new TextField(
                      controller: _passwordController,
                      obscureText: !_passwordVisible,
                      decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              borderSide: BorderSide(
                                  color: Colors.white,
                                  width: 0,
                                  style: BorderStyle.solid)),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              borderSide: BorderSide(
                                  color: Colors.white,
                                  width: 0,
                                  style: BorderStyle.solid)),
                          fillColor: Color(0xFFF5F5F5),
                          filled: true,
                          hintText: '设置登录密码',
                          hintStyle: TextStyle(
                              color: Color(0xFF999999), fontSize: 32.rpx),
                          contentPadding:
                              EdgeInsets.only(left: 20, top: 66.rpx),
                          suffixIcon: GestureDetector(
                            onTap: () {
                              print(_passwordController.text);
                              _passwordController.clear();
                            },
                            child: Image.asset('assets/images/login/close.png'),
                          ),
                          suffixIconConstraints: BoxConstraints(minWidth: 60)),
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 40.rpx)),
                  ConstrainedBox(
                    constraints: BoxConstraints(
                      maxHeight: 100.rpx,
                    ),
                    child: new TextField(
                      controller: _repasswordController,
                      obscureText: !_passwordVisible,
                      style: TextStyle(textBaseline: TextBaseline.alphabetic),
                      decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              borderSide: BorderSide(
                                  color: Colors.white,
                                  width: 0,
                                  style: BorderStyle.solid)),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              borderSide: BorderSide(
                                  color: Colors.white,
                                  width: 0,
                                  style: BorderStyle.solid)),
                          fillColor: Color(0xFFF5F5F5),
                          filled: true,
                          hintText: '请再次输入密码',
                          hintStyle: TextStyle(
                              color: Color(0xFF999999), fontSize: 32.rpx),
                          contentPadding:
                              EdgeInsets.only(left: 20, top: 66.rpx),
                          suffixIcon: GestureDetector(
                            onTap: () {
                              setState(() {
                                _passwordVisible = !_passwordVisible;
                              });
                            },
                            child: Image.asset(
                              _passwordVisible
                                  ? 'assets/images/login/invisible.png'
                                  : 'assets/images/login/visible.png',
                            ),
                          ),
                          suffixIconConstraints:
                              BoxConstraints(minWidth: 120.rpx) //  120
                          ),
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 80.rpx)),
                  MaterialButton(
                      onPressed: _okAction,
                      child: Text(
                        '下一步',
                        style: TextStyle(fontSize: 32.rpx),
                      ),
                      minWidth: double.infinity,
                      height: 50.0,
                      color: Color(0xffFFD132),
                      textColor: Color(0xff333333),
                      shape: StadiumBorder()),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _okAction() async {
    final password = _passwordController.text.trim();
    final repassword = _repasswordController.text.trim();

    if (password.length == 0) {
      toast('请输入密码');
      return;
    } else if (password.length < 6 || password.length > 20) {
      toast('登录密码长度需为6-20位');
      return;
    } else if (password != repassword) {
      toast('两次密码不一致');
      return;
    }

    _showProgressHUD('正在重置...');
    final params = {
      'cellphone': widget.arguments['tel'],
      'vcode': widget.arguments['vcode'],
      'newpasswd': password,
    };
    final response = await APIManager().put(kAPIURLResetPassword, params);
    _dismissProgressHUD();
    if (response.isSucceeded()) {
      // 回退2个页面
      int count = 0;
      Navigator.popUntil(context, (route) {
        return count++ == 2;
      });
    } else {
      APIManager().toastAPIError(response);
    }
  }
}
