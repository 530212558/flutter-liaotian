import 'dart:ui';
import 'dart:convert';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:huasheng_front_flutter/http/account_manager.dart';
import 'package:huasheng_front_flutter/http/api_defines.dart';
import 'package:huasheng_front_flutter/http/api_manager.dart';
import 'package:huasheng_front_flutter/http/user.dart';
import 'package:huasheng_front_flutter/models/login_info_model.dart';
import 'package:huasheng_front_flutter/models/model_utils.dart';
import 'package:huasheng_front_flutter/models/response_model.dart';
import 'package:huasheng_front_flutter/models/userInfo.dart';
import 'package:huasheng_front_flutter/pages/widgets/SendVerificationCode.dart';
import 'package:huasheng_front_flutter/pages/widgets/progress_hud.dart';
import 'package:huasheng_front_flutter/pages/widgets/public_ui.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';
import 'package:huasheng_front_flutter/utils/regExp.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:huasheng_front_flutter/providers/User.dart';
import 'package:huasheng_front_flutter/providers/UserInfo.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String _progressHUDText;
  bool _loading = false;
  TextEditingController _telController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  bool _passwordVisible = false;
  bool _checkboxSelected = true;

  /// 登录方式。true-验证码登录，false-密码登录
  bool _verificationCodeLogin = false;

  _showProgressHUD(String text) {
    if (mounted) {
      setState(() {
        _progressHUDText = text;
        _loading = true;
      });
    }
  }

  _dismissProgressHUD() {
    if (mounted) {
      setState(() {
        _loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return CustomProgressHUD(
      progressHUDText: _progressHUDText,
      loading: _loading,
      child: Scaffold(
        backgroundColor: Colors.white,
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child:
              /*Stack(
              fit: StackFit.expand,
              children: [*/
              SingleChildScrollView(
            child: Center(
              child: Padding(
                padding: EdgeInsets.only(
                    top: 138 + topOffset(context), left: 40, right: 40),
                child: Container(
                  child: Column(
                    children: [
                      _logoWidget(),
                      SizedBox(height: 70),
                      _telWidget(),
                      SizedBox(height: 20),
                      _passwordWidget(),
                      _centerWidget(),
                      _loginWidget(),
                      _registerWidget(),
                    ],
                  ),
                ),
              ),
            ),
          ),
          // Positioned(
          //   left: 0,
          //   right: 0,
          //   bottom: 54 + bottomOffset(context),
          //   child: _agreementWidget(),
          // ),
          //],
          //),
        ),
      ),
    );
  }

  Widget _logoWidget() {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withAlpha((0.1 * 255).toInt()),
            offset: Offset(0, 4),
            blurRadius: 20.rpx,
            spreadRadius: 0,
          ),
        ],
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(40.rpx),
        child: Container(
          width: 160.rpx,
          height: 160.rpx,
          child: Image.asset('assets/images/common/logo.png'),
        ),
      ),
    );
  }

  Widget _telWidget() {
    return TextField(
      controller: _telController,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(25),
          borderSide: BorderSide(
            color: Colors.white,
            width: 0,
            style: BorderStyle.solid,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(25),
          borderSide: BorderSide(
            color: Colors.white,
            width: 0,
            style: BorderStyle.solid,
          ),
        ),
        fillColor: Color(0xFFF5F5F5),
        filled: true,
        hintText: '请输入手机号码',
        hintStyle: TextStyle(
          color: Color(0xFF999999),
          fontSize: 16,
        ),
        contentPadding:
            EdgeInsets.only(left: 14, top: 14, right: 0, bottom: 14),
        suffixIcon: GestureDetector(
          onTap: () {
            _telController.clear();
          },
          child: Image.asset('assets/images/login/close.png'),
        ),
      ),
    );
  }

  Widget _passwordWidget() {
    return TextField(
      controller: _passwordController,
      obscureText: !_verificationCodeLogin && !_passwordVisible,
      decoration: InputDecoration(
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(25),
          borderSide: BorderSide(
            color: Colors.white,
            width: 0,
            style: BorderStyle.solid,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(25),
          borderSide: BorderSide(
            color: Colors.white,
            width: 0,
            style: BorderStyle.solid,
          ),
        ),
        fillColor: Color(0xFFF5F5F5),
        filled: true,
        hintText: _verificationCodeLogin ? '请输入验证码' : '请输入登录密码',
        hintStyle: TextStyle(
          color: Color(0xFF999999),
          fontSize: 16,
        ),
        contentPadding:
            EdgeInsets.only(left: 14, top: 14, right: 0, bottom: 14),
        suffixIcon: !_verificationCodeLogin
            ? GestureDetector(
                onTap: () {
                  if (mounted) {
                    setState(() {
                      _passwordVisible = !_passwordVisible;
                    });
                  }
                },
                child: Image.asset(
                  _passwordVisible
                      ? 'assets/images/login/invisible.png'
                      : 'assets/images/login/visible.png',
                ),
              )
            : SendVerificationCode(onTap: () async {
                return _telController.text;
              }, callback: (String vcode) {
                if (mounted) {
                  setState(() {
                    _passwordController.text = vcode;
                  });
                }
              }),
        suffixIconConstraints:
            BoxConstraints(minWidth: _verificationCodeLogin ? 102 : 48),
      ),
    );
  }

  Widget _centerWidget() {
    return Padding(
      padding: EdgeInsets.only(left: 14, top: 0, right: 14, bottom: 26),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          FlatButton(
            onPressed: () {
              if (mounted) {
                setState(() {
                  _verificationCodeLogin = !_verificationCodeLogin;
                  _passwordController.text = '';
                });
              }
            },
            padding: EdgeInsets.all(0),
            child: Text(
              _verificationCodeLogin ? '密码登录' : '短信验证码登录',
              style: TextStyle(
                color: Color(0xFF1AD8D0),
                fontSize: 14,
                fontWeight: FontWeight.normal,
              ),
            ),
          ),
          FlatButton(
            onPressed: () {
              Navigator.pushNamed(context, '/ForgetPasswordPage');
            },
            padding: EdgeInsets.all(0),
            child: Text(
              '忘记密码',
              style: TextStyle(
                color: Color(0xFF999999),
                fontSize: 14,
                fontWeight: FontWeight.normal,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _loginWidget() {
    return MaterialButton(
      onPressed: _login,
      child: Text(
        '登录',
        style: TextStyle(
          color: Color(0xFF333333),
          fontSize: 32.rpx,
          fontWeight: FontWeight.w500,
        ),
      ),
      minWidth: double.infinity,
      height: 100.rpx,
      color: Color(0xffFFD132),
      shape: StadiumBorder(),
      elevation: 0,
    );
  }

  Widget _registerWidget() {
    return FlatButton(
      onPressed: () {
        Navigator.pushNamed(context, '/RegisterPage');
      },
      padding: EdgeInsets.all(0),
      child: Text(
        // _verificationCodeLogin ? '密码登录' : '注册账号',
        '注册账号',
        style: TextStyle(
          color: Color(0xFF1AD8D0),
          fontSize: 14,
          fontWeight: FontWeight.normal,
        ),
      ),
    );
  }

  // Widget _agreementWidget() {
  //   return Row(
  //     mainAxisAlignment: MainAxisAlignment.center,
  //     children: [
  //       Container(
  //         decoration: BoxDecoration(
  //             shape: BoxShape.circle,
  //             color: _checkboxSelected ? Color(0xff1AD8D0) : Colors.white,
  //             border: Border.all(
  //                 color:
  //                     _checkboxSelected ? Color(0xff1AD8D0) : Color(0xFFCCCCCC),
  //                 width: 1,
  //                 style: BorderStyle.solid)),
  //         child: InkWell(
  //           onTap: () {
  //             if (mounted) {
  //               setState(() {
  //                 _checkboxSelected = !_checkboxSelected;
  //               });
  //             }
  //           },
  //           child: _checkboxSelected
  //               ? Icon(
  //                   Icons.check,
  //                   size: 16.0,
  //                   color: Colors.white,
  //                 )
  //               : Icon(
  //                   Icons.check_box_outline_blank,
  //                   size: 16.0,
  //                   color: Colors.white,
  //                 ),
  //         ),
  //       ),
  //       SizedBox(width: 4),
  //       _richTextWidget(),
  //     ],
  //   );
  // }

  // Widget _richTextWidget() {
  //   return RichText(
  //     text: TextSpan(
  //       children: <InlineSpan>[
  //         TextSpan(text: '登录即同意'),
  //         TextSpan(
  //           text: '《用户协议》',
  //           style: TextStyle(
  //             color: Color(0xFF1AD8D0),
  //           ),
  //           recognizer: TapGestureRecognizer()
  //             ..onTap = () {
  //               Navigator.pushNamed(
  //                 context,
  //                 '/WebViewPage',
  //                 arguments: <String, dynamic>{
  //                   'url': 'http://m.microchat.cn/doc/yonghuxieyi.html',
  //                 },
  //               );
  //             },
  //         ),
  //         TextSpan(text: '和'),
  //         TextSpan(
  //           text: '《隐私政策》',
  //           style: TextStyle(
  //             color: Color(0xFF1AD8D0),
  //           ),
  //           recognizer: TapGestureRecognizer()
  //             ..onTap = () {
  //               Navigator.pushNamed(
  //                 context,
  //                 '/WebViewPage',
  //                 arguments: <String, dynamic>{
  //                   'url': 'http://m.microchat.cn/doc/yinsizhengce.html',
  //                 },
  //               );
  //             },
  //         )
  //       ],
  //       style: TextStyle(
  //         color: Color(0xFF999999),
  //         fontSize: 12,
  //         fontWeight: FontWeight.normal,
  //       ),
  //     ),
  //   );
  // }

  _login() async {
    final phone = _telController.text.trim();
    final password = _passwordController.text.trim();

    if (phone.length == 0) {
      toast('请输入登录手机号码');
      return;
    } else if (!isValidTel(phone)) {
      toast('手机号码格式不正确');
      return;
    } else if (!_checkboxSelected) {
      toast('请同意用户协议与隐私政策后继续');
      return;
    } else if (password.length == 0) {
      final loginType = _verificationCodeLogin ? '验证码' : '登录密码';
      toast('请输入$loginType');
      return;
    }
    var data = {
      'phone': phone,
      'password': password,
    };

    requestLogin(data, onSuccess: (userInfo) async {
      final prefs = await SharedPreferences.getInstance();
      prefs.setString('userInfo', json.encode(userInfo.toJson()));
      Provider.of<UserInfoProvider>(context, listen: false)
          .setUserInfo(userInfo);
      toast("登录成功");
      Navigator.pop(context);
      // Navigator.pushReplacementNamed(context, '/');
    });
    // _dismissProgressHUD();
    // final loginInfo = LoginInfo.fromJson(data);
    // AccountManager().loginInfo = loginInfo;
    // AccountManager().storeLoginInfo();
  }
}
