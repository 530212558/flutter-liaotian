// import 'package:amap_location/amap_location.dart';
// import 'package:amap_map_fluttify/amap_map_fluttify.dart';
// import 'package:amap_search_fluttify/amap_search_fluttify.dart';
import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/http/api_defines.dart';
import 'package:huasheng_front_flutter/http/api_manager.dart';
import 'package:huasheng_front_flutter/pages/widgets/ShowDialog.dart';
import 'package:huasheng_front_flutter/pages/widgets/jhPickerTool.dart';
import 'package:huasheng_front_flutter/pages/widgets/progress_hud.dart';
import 'package:huasheng_front_flutter/pages/widgets/public_ui.dart';
import 'package:huasheng_front_flutter/utils/authorityCheck.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';

class ApplicationInvitationCode extends StatefulWidget {
  @override
  _ApplicationInvitationCodeState createState() =>
      _ApplicationInvitationCodeState();
}

class _ApplicationInvitationCodeState extends State<ApplicationInvitationCode> {
  String _progressHUDText;
  bool _loading = false;
  TextEditingController _locationController = TextEditingController();
  TextEditingController _industryController = TextEditingController();
  TextEditingController _methodController = TextEditingController();
  TextEditingController _reasonController = TextEditingController();
  double _longitude;
  double _latitude;
  String _province;
  String _city;
  String _district;
  String _address;
  String _trade;
  String _career;

  List<Map<String, Object>> _industryList = [];

  _getIndustryList() async {
    final response = await APIManager().get(kAPIURLIndustry, null);
    if (response.isSucceeded()) {
      if (mounted) {
        setState(() {
          List<Map<String, Object>> list = [];
          response.response['categories'].forEach((element) {
            list.add(element);
          });
          _industryList = list;
        });
      }
    } else {
      APIManager().toastAPIError(response);
    }
  }

  @override
  void initState() {
    super.initState();

    _getAddress();
    _getIndustryList();
  }

  @override
  void dispose() {
    // AMapLocationClient.shutdown();
    super.dispose();
  }

  _getAddress() async {
    // AMapLocationClient.startup(AMapLocationOption(
    //     desiredAccuracy: CLLocationAccuracy.kCLLocationAccuracyHundredMeters));
    // AmapCore.init('bf2775ecf0c61d253c12253ba65049bd');

    bool result = await Permissions.getLocation();
    if (result) {
      // AMapLocation location = await AMapLocationClient.getLocation(true);
      // _longitude = location.longitude;
      // _latitude = location.latitude;

      // ReGeocode reGeocode = await AmapSearch.instance.searchReGeocode(LatLng(
      //   location.latitude,
      //   location.longitude,
      // ));
      // print(await reGeocode.toFutureString());
      // _province = await reGeocode.provinceName;
      // _city = await reGeocode.cityName;
      // _district = await reGeocode.districtName;
      // _address = await reGeocode.formatAddress;

      // // 剔除完整地址里的省市区前缀
      // final s = _province + _city + _district;
      // if (_address.startsWith(s)) {
      //   _address = _address.substring(s.length);
      // }

      // if (mounted) {
      //   setState(() {
      //     _locationController.text = '$_province/$_city';
      //   });
      // }
    } else {
      alertDialog(
        context: context,
        title: Text('定位权限被拒绝'),
        content: Text('必须先提供定位权限才能申请邀请码'),
        onCancel: () {
          Navigator.of(context).pop();
        },
        onConfirm: () {
          Navigator.of(context).pop();
          Permissions.openAppSetting();
        },
      );
    }
  }

  _showProgressHUD(String text) {
    if (mounted) {
      setState(() {
        _progressHUDText = text;
        _loading = true;
      });
    }
  }

  _dismissProgressHUD() {
    if (mounted) {
      setState(() {
        _loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return CustomProgressHUD(
      progressHUDText: _progressHUDText,
      loading: _loading,
      child: Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          backgroundColor: Colors.transparent,
          elevation: 0,
          toolbarHeight: 44,
          leading: IconButton(
            icon: Image.asset('assets/images/login/back.png'),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Text(
            '申请邀请码',
            style: TextStyle(
              color: Color(0xFF333333),
              fontSize: 17,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.only(left: 60.0.rpx, right: 60.rpx),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    alignment: Alignment.topLeft,
                    child: Text('填写以下申请资料，向平台申请邀请码，申请成功将通过短信发送',
                        style: TextStyle(
                            color: Color(0xff999999),
                            fontSize: 32.rpx,
                            fontWeight: FontWeight.w400)),
                    margin: EdgeInsets.only(top: 40.0.rpx, bottom: 100.rpx),
                  ),
                  ConstrainedBox(
                    constraints: BoxConstraints(
                      maxHeight: 100.rpx,
                    ),
                    child: new TextField(
                      controller: _locationController,
                      // cursorColor: Colors.red,  //  光标颜色
                      textAlign: TextAlign.right,
                      readOnly: true,
                      decoration: InputDecoration(
                        // labelText: '请输入手机号',
                        //  失焦样式
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                                color: Color.fromRGBO(245, 245, 245, 1),
                                width: 2,
                                style: BorderStyle.solid) //没什么卵效果
                            ),
                        //  选中样式
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                                color: Color.fromRGBO(245, 245, 245, 1),
                                width: 2,
                                style: BorderStyle.solid) //没什么卵效果
                            ),
                        hintText: '根据定位自动获取城市',
                        hintStyle: TextStyle(
                            color: Color(0xFF999999), fontSize: 29.rpx),
                        // contentPadding: EdgeInsets.only(left: 20, top: 66.rpx),
                        prefixIcon: Text(
                          '所在地',
                          style: TextStyle(
                              color: Color(0xFF333333),
                              fontSize: 29.rpx,
                              fontWeight: FontWeight.w500),
                        ),
                        prefixIconConstraints: BoxConstraints(minHeight: 0),
                      ),
                    ),
                  ),
                  ConstrainedBox(
                    constraints: BoxConstraints(
                      maxHeight: 100.rpx,
                    ),
                    child: new TextField(
                      controller: _industryController,
                      // cursorColor: Colors.red,  //  光标颜色
                      textAlign: TextAlign.right,
                      readOnly: true,
                      onTap: () {
                        PickHelper.openCityPicker(context, title: '选择所在行业',
                            onConfirm:
                                (List<String> stringData, List<int> selecteds) {
                          _trade = stringData.first;
                          _career = stringData.last;
                          _industryController.text = stringData.join('-');
                          // parem['trade'] = stringData.join('-');
                        });
                      },
                      decoration: InputDecoration(
                        // labelText: '请输入手机号',
                        //  失焦样式
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                                color: Color.fromRGBO(245, 245, 245, 1),
                                width: 2,
                                style: BorderStyle.solid) //没什么卵效果
                            ),
                        //  选中样式
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                                color: Color.fromRGBO(245, 245, 245, 1),
                                width: 2,
                                style: BorderStyle.solid) //没什么卵效果
                            ),
                        hintText: '选择所在行业',
                        prefixIcon: Text(
                          '所在行业',
                          style: TextStyle(
                              color: Color(0xFF333333),
                              fontSize: 29.rpx,
                              height: 2,
                              fontWeight: FontWeight.w500),
                        ),
                        prefixIconConstraints: BoxConstraints(minHeight: 0),
                        suffixIcon: GestureDetector(
                          child: Image.asset(
                            "assets/images/register/arrow_right.png",
                          ),
                        ),
                        suffixIconConstraints:
                            BoxConstraints(minHeight: 0, maxWidth: 56),
                      ),
                    ),
                  ),
                  ConstrainedBox(
                    constraints: BoxConstraints(
                      maxHeight: 100.rpx,
                    ),
                    child: new TextField(
                      controller: _methodController,
                      // cursorColor: Colors.red,  //  光标颜色
                      readOnly: true,
                      textAlign: TextAlign.right,
                      onTap: () {
                        JhPickerTool.showStringPicker(context,
                            data: ["社区", "论坛", "好友推荐", "微信", "QQ", "线下"],
                            normalIndex: 2,
                            clickCallBack: (var index, var strData) {
                          _methodController.text = strData.toString();
                        });
                      },
                      decoration: InputDecoration(
                        // labelText: '请输入手机号',
                        //  失焦样式
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                                color: Color.fromRGBO(245, 245, 245, 1),
                                width: 2,
                                style: BorderStyle.solid) //没什么卵效果
                            ),
                        //  选中样式
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                                color: Color.fromRGBO(245, 245, 245, 1),
                                width: 2,
                                style: BorderStyle.solid) //没什么卵效果
                            ),
                        hintText: '选择获知社区的渠道',
                        hintStyle: TextStyle(
                            color: Color(0xFF999999), fontSize: 29.rpx),
                        // contentPadding: EdgeInsets.only(left: 20, top: 66.rpx),
                        prefixIcon: Text(
                          '获知渠道',
                          style: TextStyle(
                              color: Color(0xFF333333),
                              fontSize: 29.rpx,
                              height: 2,
                              fontWeight: FontWeight.w500),
                        ),
                        prefixIconConstraints: BoxConstraints(minHeight: 0),
                        suffixIcon: GestureDetector(
                          child: Image.asset(
                            "assets/images/register/arrow_right.png",
                          ),
                        ),
                        suffixIconConstraints: BoxConstraints(minHeight: 0),
                      ),
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 50.rpx)),
                  Text(
                    '申请理由',
                    style: TextStyle(
                        color: Color(0xFF333333),
                        fontSize: 29.rpx,
                        height: 2,
                        fontWeight: FontWeight.w500),
                  ),
                  ConstrainedBox(
                    constraints: BoxConstraints(
                      minHeight: 100.rpx,
                    ),
                    child: new TextField(
                      controller: _reasonController,
                      minLines: 4,
                      maxLines: 6,
                      // cursorColor: Colors.red,  //  光标颜色
                      textAlign: TextAlign.left,
                      decoration: InputDecoration(
                        // labelText: '请输入手机号',
                        //  失焦样式
                        enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                                color: Color.fromRGBO(245, 245, 245, 1),
                                width: 2,
                                style: BorderStyle.solid) //没什么卵效果
                            ),
                        //  选中样式
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(
                                color: Color.fromRGBO(245, 245, 245, 1),
                                width: 2,
                                style: BorderStyle.solid) //没什么卵效果
                            ),
                        hintText: '输入合理的申请理由，提高申请通过率',
                        hintStyle: TextStyle(
                            color: Color(0xFF999999), fontSize: 29.rpx),
                        // contentPadding: EdgeInsets.only(left: 20, top: 66.rpx),
                      ),
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 80.rpx)),
                  MaterialButton(
                    onPressed: _okAction,
                    child: Text(
                      "确认",
                      style: TextStyle(fontSize: 32.rpx),
                    ),
                    minWidth: double.infinity,
                    height: 50.0,
                    color: Color(0xffFFD132),
                    textColor: Color(0xff333333),
                    shape: StadiumBorder(),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _okAction() async {
    final location = _locationController.text.trim();
    final industry = _industryController.text.trim();
    final method = _methodController.text.trim();
    final reason = _reasonController.text.trim();

    if (location.length == 0) {
      toast('请填写所在地');
      return;
    } else if (industry.length == 0) {
      toast('请填写所在行业');
      return;
    } else if (method.length == 0) {
      toast('请填写获知渠道');
      return;
    } else if (reason.length == 0) {
      toast('请填写申请理由');
      return;
    } else if (reason.length < 4 || reason.length > 100) {
      toast('申请理由必须4-100个字符');
      return;
    }

    _showProgressHUD('正在申请...');
    final params = {
      'longitude': _longitude,
      'latitude': _latitude,
      'province': _province,
      'city': _city,
      'district': _district,
      'address': _address,
      'trade': _trade,
      'career': _career,
      'method': method,
      'reason': reason,
    };
    final response =
        await APIManager().post(kAPIURLApplyInvitationCode, params);
    _dismissProgressHUD();
    if (response.isSucceeded()) {
      Navigator.pop(context);
    } else {
      APIManager().toastAPIError(response);
    }
  }
}
