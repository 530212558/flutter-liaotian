import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/models/user_model.dart';
import 'package:huasheng_front_flutter/pages/widgets/public_ui.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';

class SelectGenderPage extends StatefulWidget {
  @override
  _SelectGenderPageState createState() => _SelectGenderPageState();
}

class _SelectGenderPageState extends State<SelectGenderPage> {
  Gender _gender = Gender.male;

  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.only(left: 30, right: 30),
            child: Column(
              children: [
                Container(
                  alignment: Alignment.topLeft,
                  margin: EdgeInsets.only(
                    top: 108 + topOffset(context),
                    bottom: 50,
                  ),
                  child: Text(
                    '你是...',
                    style: TextStyle(
                      color: Color(0xff333333),
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                _genderWidget(),
                Padding(padding: EdgeInsets.only(top: 167)),
                _okWidget(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _genderWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        GestureDetector(
            child: Container(
              width: 300.rpx,
              height: 386.rpx,
              padding: EdgeInsets.only(top: 68.0.rpx),
              decoration: new BoxDecoration(
                borderRadius: new BorderRadius.circular(32.rpx),
                color: Color.fromRGBO(26, 216, 208, 0.2),
                border: Border.all(
                  width: 4.rpx,
                  color: Color.fromRGBO(
                      26, 216, 208, _gender == Gender.male ? 1 : 0.2),
                ),
              ),
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Column(
                    children: [
                      Image.asset(
                        'assets/images/register/male.png',
                        width: 100.rpx,
                        height: 110.rpx,
                      ),
                      Image.asset('assets/images/register/male_text.png'),
                    ],
                  ),
                  new Positioned(
                    child: Text(
                      '男士',
                      style: TextStyle(
                          color: Color(0xff1AD8D0),
                          fontSize: 32.rpx,
                          height: 1.375),
                    ),
                    right: 28.rpx,
                    bottom: 20.rpx,
                  ),
                  new Positioned(
                    child: Offstage(
                      offstage: _gender == Gender.female,
                      child: Image.asset(
                        'assets/images/register/male_s.png',
                        width: 68.rpx,
                        height: 60.rpx,
                      ),
                    ),
                    left: -4.rpx,
                    bottom: -4.rpx,
                  ),
                ],
              ),
            ),
            onTap: () {
              this.setState(() {
                _gender = Gender.male;
              });
            }),
        GestureDetector(
          child: Container(
              width: 300.rpx,
              height: 386.rpx,
              padding: EdgeInsets.only(top: 68.0.rpx),
              decoration: new BoxDecoration(
                  borderRadius: new BorderRadius.circular(32.rpx),
                  color: Color.fromRGBO(254, 124, 150, 0.2),
                  border: Border.all(
                      width: 4.rpx,
                      color: Color.fromRGBO(
                          254, 124, 150, _gender == Gender.female ? 1 : 0.2))),
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Column(
                    children: [
                      Image.asset(
                        'assets/images/register/female.png',
                        width: 100.rpx,
                        height: 110.rpx,
                      ),
                      Image.asset('assets/images/register/female_text.png'),
                    ],
                  ),
                  new Positioned(
                    child: Text(
                      '女士',
                      style: TextStyle(
                          color: Color(0xffFE7C96),
                          fontSize: 32.rpx,
                          height: 1.375),
                    ),
                    right: 28.rpx,
                    bottom: 20.rpx,
                  ),
                  new Positioned(
                    child: Offstage(
                      offstage: _gender == Gender.male,
                      child: Image.asset(
                        'assets/images/register/female_s.png',
                        width: 68.rpx,
                        height: 60.rpx,
                      ),
                    ),
                    left: -4.rpx,
                    bottom: -4.rpx,
                  ),
                ],
              )),
          onTap: () {
            this.setState(() {
              _gender = Gender.female;
            });
          },
        )
      ],
    );
  }

  Widget _okWidget() {
    return MaterialButton(
      onPressed: _okAction,
      child: Text(
        "确认",
        style: TextStyle(fontSize: 32.rpx),
      ),
      minWidth: double.infinity,
      height: 50.0,
      color: Color(0xffFFD132),
      textColor: Color(0xff333333),
      shape: StadiumBorder(),
    );
  }

  _okAction() {
    Navigator.pushNamed(
      context,
      '/CompleteInfoPage',
      arguments: <String, dynamic>{
        'gender': _gender,
      },
    );
    // showConfirmDialog(
    //   '性别确定',
    //   '性别确定后无法修改，确定提交吗？',
    //   '取消',
    //   '确定',
    //   () {
    //     Navigator.pushNamed(
    //       context,
    //       '/CompleteInfoPage',
    //       arguments: <String, dynamic>{
    //         'gender': _gender,
    //       },
    //     );
    //     // if (_gender == Gender.male) {
    //     //   Navigator.pushNamed(
    //     //     context,
    //     //     '/EnterInvitationCodePage',
    //     //     arguments: <String, dynamic>{
    //     //       'gender': _gender,
    //     //     },
    //     //   );
    //     // } else {
    //     //   Navigator.pushNamed(
    //     //     context,
    //     //     '/CompleteInfoPage',
    //     //     arguments: <String, dynamic>{
    //     //       'gender': _gender,
    //     //     },
    //     //   );
    //     // }
    //   },
    // );
  }

  /// 显示确认对话框
  showConfirmDialog(
    String title,
    String message,
    String cancelText,
    String okText,
    Function() okAction,
  ) {
    showCupertinoDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text(title ?? ''),
          content: Text(message ?? ''),
          actions: <Widget>[
            CupertinoDialogAction(
              child: Text(cancelText ?? ''),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            CupertinoDialogAction(
              child: Text(okText ?? ''),
              onPressed: () {
                Navigator.of(context).pop();
                okAction();
              },
            ),
          ],
        );
      },
    );
  }
}
