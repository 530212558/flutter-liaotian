import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:huasheng_front_flutter/http/api_defines.dart';
import 'package:huasheng_front_flutter/http/api_manager.dart';
import 'package:huasheng_front_flutter/pages/widgets/progress_hud.dart';
import 'package:huasheng_front_flutter/pages/widgets/public_ui.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';

class EnterInvitationCodePage extends StatefulWidget {
  final Map<String, dynamic> arguments;

  EnterInvitationCodePage({
    this.arguments,
  });

  @override
  _EnterInvitationCodePageState createState() =>
      _EnterInvitationCodePageState();
}

class _EnterInvitationCodePageState extends State<EnterInvitationCodePage> {
  String _progressHUDText;
  bool _loading = false;
  TextEditingController _codeController = TextEditingController();

  @override
  initState() {
    super.initState();
  }

  _showProgressHUD(String text) {
    if (mounted) {
      setState(() {
        _progressHUDText = text;
        _loading = true;
      });
    }
  }

  _dismissProgressHUD() {
    if (mounted) {
      setState(() {
        _loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return CustomProgressHUD(
      progressHUDText: _progressHUDText,
      loading: _loading,
      child: Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          backgroundColor: Colors.transparent,
          elevation: 0,
          toolbarHeight: 44,
          leading: IconButton(
            icon: Image.asset('assets/images/login/back.png'),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Text(
            '输入邀请码',
            style: TextStyle(
              color: Color(0xFF333333),
              fontSize: 17,
              fontWeight: FontWeight.w500,
            ),
          ),
          actions: [
            IconButton(
              icon: Image.asset('assets/images/register/refresh.png'),
              onPressed: _refreshAction,
            ),
          ],
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.only(left: 60.0.rpx, right: 60.rpx),
              // color: Colors.amber,
              // width: 630.0.rpx,
              height: MediaQuery.of(context).size.height * 0.8,
              child: Stack(
                children: [
                  Column(children: [
                    Container(
                      alignment: Alignment.topLeft,
                      child: Text('为打造纯净私密的交友社区，保证产品稳定运营，需通过邀请码完成注册，请向社区老用户索取',
                          style: TextStyle(
                              color: Color(0xff999999),
                              fontSize: 32.rpx,
                              fontWeight: FontWeight.w400)),
                      margin: EdgeInsets.only(top: 48.0.rpx, bottom: 80.rpx),
                    ),
                    ConstrainedBox(
                      constraints: BoxConstraints(
                        maxHeight: 100.rpx,
                      ),
                      child: new TextField(
                        controller: _codeController,
                        keyboardType: TextInputType.number,
                        // cursorColor: Colors.red,  //  光标颜色
                        textAlign: TextAlign.center,
                        onChanged: (value) => {
                          this.setState(() {
                            this._codeController.text = value;
                          })
                        },
                        decoration: InputDecoration(
                          // labelText: '请输入手机号',
                          //  失焦样式
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              borderSide: BorderSide(
                                  color: Colors.white,
                                  width: 0,
                                  style: BorderStyle.solid) //没什么卵效果
                              ),
                          //  选中样式
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              borderSide: BorderSide(
                                  color: Colors.white,
                                  width: 0,
                                  style: BorderStyle.solid) //没什么卵效果
                              ),
                          fillColor: Color(0xFFF5F5F5),
                          filled: true,
                          hintText: '请输入6位邀请码',
                          hintStyle: TextStyle(
                              color: Color(0xFF999999), fontSize: 32.rpx),
                          contentPadding:
                              EdgeInsets.only(left: 20, top: 66.rpx),
                          // prefixIcon: Icon(
                          //   Icons.phone,
                          //   color: Color.fromRGBO(204, 204, 204, 1),
                          // ),
                          // prefixIconConstraints: BoxConstraints(minWidth: 60),
                          suffixIcon: GestureDetector(
                            onTap: () {
                              print(_codeController.text);
                              _codeController.clear();
                            },
                            child: Image.asset('assets/images/login/close.png'),
                          ),
                          // suffixIcon: Icon(
                          //   Icons.close,
                          //   color: Color.fromRGBO(204, 204, 204, 1),
                          // ),
                          // suffixIconConstraints:
                          //     BoxConstraints(minWidth: 60)
                        ),
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(top: 40.rpx)),
                    Opacity(
                      opacity: _codeController.text.length <= 0 ? 0.5 : 1,
                      child: MaterialButton(
                          onPressed: _okAction,
                          child: Text(
                            "确认",
                            style: TextStyle(fontSize: 32.rpx),
                          ),
                          minWidth: double.infinity,
                          height: 50.0,
                          color: Color(0xffFFD132),
                          textColor: Color(0xff333333),
                          shape: StadiumBorder(
                              // side: BorderSide(width: 2, style: BorderStyle.solid, color: Color(0xFF00FFFF))
                              )),
                    ),
                  ]),
                  Positioned(
                    child: Column(children: [
                      GestureDetector(
                        // child: ConstrainedBox(
                        child: Image.asset(
                          "assets/images/register/buy_vip.png",
                          fit: BoxFit.cover,
                          width: 610.rpx,
                        ),
                        //   constraints: BoxConstraints.expand(),
                        // ),
                        onTap: () {
                          print('openMembership');
                        },
                      ),
                      Padding(padding: EdgeInsets.only(top: 28.rpx)),
                      GestureDetector(
                        child: Image.asset(
                          "assets/images/register/apply_invitation_code.png",
                          fit: BoxFit.cover,
                          width: 610.rpx,
                        ),
                        onTap: () {
                          Navigator.pushNamed(
                              context, '/ApplyInvitationCodePage');
                        },
                      ),
                    ]),
                    bottom: 80.rpx,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _refreshAction() async {
    _showProgressHUD('正在刷新...');
    final response = await APIManager().post(kAPIURLApplyInvitationCode, null);
    _dismissProgressHUD();
    if (response.isSucceeded()) {
      _codeController.text = response.response['incode'];
    } else {
      APIManager().toastAPIError(response);
    }
  }

  _okAction() async {
    final code = _codeController.text.trim();

    if (code.length == 0) {
      toast('请输入邀请码');
      return;
    }

    _showProgressHUD('正在验证...');
    final params = {
      'incode': code,
    };
    final response =
        await APIManager().get(kAPIURLVerifyInvitationCode, params);
    _dismissProgressHUD();
    if (response.isSucceeded()) {
      Navigator.pushNamed(
        context,
        '/CompleteInfoPage',
        arguments: widget.arguments,
      );
    } else {
      APIManager().toastAPIError(response);
    }
  }
}
