import 'dart:convert';
import 'dart:io';

import 'package:amap_location/amap_location.dart';
import 'package:amap_search_fluttify/amap_search_fluttify.dart';
import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/http/dio.dart';
import 'package:huasheng_front_flutter/http/user.dart';
import 'package:huasheng_front_flutter/models/userInfo.dart';
import 'package:huasheng_front_flutter/models/user_model.dart';
import 'package:huasheng_front_flutter/pages/widgets/ShowDialog.dart';
import 'package:huasheng_front_flutter/pages/widgets/progress_hud.dart';
import 'package:huasheng_front_flutter/pages/widgets/public_ui.dart';
import 'package:huasheng_front_flutter/pages/widgets/showAreaSelection.dart';
import 'package:huasheng_front_flutter/utils/authorityCheck.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';
import 'package:huasheng_front_flutter/pages/widgets/jhPickerTool.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CompleteInfoPage extends StatefulWidget {
  final Map<String, dynamic> arguments;

  CompleteInfoPage({
    this.arguments,
  });

  @override
  _CompleteInfoPageState createState() => _CompleteInfoPageState();
}

class _CompleteInfoPageState extends State<CompleteInfoPage> {
  String _progressHUDText;
  bool _loading = false;
  TextEditingController _nicknameController = TextEditingController();
  TextEditingController _birthdayController = TextEditingController();
  TextEditingController _heightWeightController = TextEditingController();
  TextEditingController _locationController = TextEditingController();
  TextEditingController _industryController = TextEditingController();
  TextEditingController _preferenceController = TextEditingController();
  TextEditingController _introductionController = TextEditingController();
  Widget _image;
  final _imagePicker = ImagePicker();
  List<List<String>> _heightsWeightsList = [];

  Map<String, dynamic> _params = {
    'user_id': 0,
    'gender': 0,
    'nickname': '',
    'avatar': '',
    'birth_year': 0,
    'birth_month': 0,
    'birth_day': 0,
    'hight': 0,
    'weight': 0,
    'longitude': 0,
    'latitude': 0,
    'province': '', //四川
    'city': '', //成都
    'district': '', //县区
    'address': '', //详细地址
    'trade': '', //行业
    'career': '', //职业
    'preference': '',
    'introduction': '',
  };

  @override
  void initState() {
    super.initState();
    _getAddress();

    Gender gender = widget.arguments['gender'];
    _params['gender'] = gender.index;

    List<String> heightsList = [];
    for (var i = 150; i <= 200; i++) {
      heightsList.add('${i}cm');
    }
    _heightsWeightsList.add(heightsList);

    List<String> weightsList = [];
    for (var i = 50; i <= 200; i++) {
      weightsList.add('$i斤');
    }
    _heightsWeightsList.add(weightsList);

    SharedPreferences.getInstance().then((value) {
      UserInfo userInfo = userInfoFromJson(value.getString('userInfo'));
      _params['user_id'] = userInfo.id;
    });
  }

  _getImage() async {
    // final pickedFile = await picker.getImage(source: ImageSource.camera);
    bool result = await Permissions.getCamera();
    if (result) {
      PickedFile pickedFile =
          await _imagePicker.getImage(source: ImageSource.gallery);
      Dios.uploadImage(File(pickedFile.path), images: 'avatar/',
          success: (String url) {
        this.setState(() {
          _params['avatar'] = url;
          _image = ClipOval(
            child: Image.file(
              File(pickedFile.path),
              fit: BoxFit.cover,
              width: 160.rpx,
              height: 160.rpx,
            ),
          );
        });
      });
    } else {
      alertDialog(
        context: context,
        title: Text('相册权限被拒绝'),
        content: Text('是否需要跳转到打开相册权限？'),
        onCancel: () {
          Navigator.of(context).pop();
        },
        onConfirm: () {
          Navigator.of(context).pop();
          Permissions.openAppSetting();
        },
      );
    }
  }

  _getAddress() async {
    //  开启 amap_location 定位
    AMapLocationClient.startup(new AMapLocationOption(
        desiredAccuracy: CLLocationAccuracy.kCLLocationAccuracyHundredMeters));

    bool result = await Permissions.getLocation();
    if (result) {
      AMapLocation location = await AMapLocationClient.getLocation(true);
      final longitude = location.longitude;
      final latitude = location.latitude;

      /// 逆地理编码（坐标转地址）amap_search_fluttify
      ReGeocode reGeocode = await AmapSearch.instance.searchReGeocode(LatLng(
        location.latitude,
        location.longitude,
      ));
      // print(await reGeocode.toFutureString());
      final province = reGeocode.provinceName;
      final city = reGeocode.cityName;
      final district = reGeocode.districtName;
      var address = reGeocode.formatAddress;

      // 剔除完整地址里的省市区前缀
      final s = province + city + district;
      if (address.startsWith(s)) {
        address = address.substring(s.length);
      }

      if (mounted) {
        setState(() {
          _locationController.text = '$province/$city';
        });
      }

      _params['longitude'] = longitude;
      _params['latitude'] = latitude;
      _params['province'] = province;
      _params['city'] = city;
      _params['district'] = district;
      _params['address'] = address;
    } else {
      alertDialog(
        context: context,
        title: Text('定位权限被拒绝'),
        content: Text('必须先提供定位权限才能完成注册'),
        onCancel: () {
          Navigator.of(context).pop();
        },
        onConfirm: () {
          Navigator.of(context).pop();
          Permissions.openAppSetting();
        },
      );
    }
  }

  @override
  void dispose() {
    AMapLocationClient.shutdown();
    super.dispose();
  }

  _showProgressHUD(String text) {
    if (mounted) {
      setState(() {
        _progressHUDText = text;
        _loading = true;
      });
    }
  }

  _dismissProgressHUD() {
    if (mounted) {
      setState(() {
        _loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return CustomProgressHUD(
      progressHUDText: _progressHUDText,
      loading: _loading,
      child: Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          backgroundColor: Colors.transparent,
          elevation: 0,
          toolbarHeight: 44,
          leading: IconButton(
            icon: Image.asset('assets/images/login/back.png'),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Text(
            '完善信息',
            style: TextStyle(
              color: Color(0xFF333333),
              fontSize: 17,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.only(left: 60.0.rpx, right: 60.rpx),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GestureDetector(
                    child: Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(top: 60.rpx, bottom: 46.rpx),
                      child: Container(
                        width: 160.rpx,
                        height: 160.rpx,
                        decoration: BoxDecoration(
                          color: Color(0xFFf5f5f5), // 底色
                          borderRadius:
                              new BorderRadius.circular((160.rpx)), // 圆角度
                        ),
                        child: _image ??
                            Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.camera_alt,
                                  size: 48.rpx,
                                  color: Color(0xffCCCCCC),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      top: 6.rpx, bottom: 24.rpx),
                                  child: Text(
                                    "上传头像",
                                    style: TextStyle(
                                      fontSize: 24.0.rpx,
                                      color: Color(0xff999999),
                                    ),
                                  ),
                                )
                              ],
                            ),
                      ),
                    ),
                    onTap: () {
                      this._getImage();
                    },
                  ),
                  ConstrainedBox(
                    constraints: BoxConstraints(
                      maxHeight: 100.rpx,
                    ),
                    child: new TextField(
                      controller: _nicknameController,
                      textAlign: TextAlign.right,
                      onTap: () {},
                      decoration: InputDecoration(
                        border:
                            UnderlineInputBorder(borderSide: BorderSide.none),
                        hintText: '点击输入昵称',
                        prefixIcon: Text(
                          '昵称',
                          style: TextStyle(
                              color: Color(0xFF333333),
                              fontSize: 32.rpx,
                              fontWeight: FontWeight.w500),
                        ),
                        prefixIconConstraints:
                            BoxConstraints(minHeight: 0, maxWidth: 350.rpx),
                      ),
                    ),
                  ),
                  ConstrainedBox(
                    constraints: BoxConstraints(
                      maxHeight: 100.rpx,
                    ),
                    child: new TextField(
                      controller: _birthdayController,
                      textAlign: TextAlign.right,
                      readOnly: true,
                      onTap: () {
                        JhPickerTool.showDatePicker(context, clickCallback:
                            (var str, var time, DateTime dateTime) {
                          this.setState(() {
                            this._birthdayController.text = str;
                            _params['birth_year'] = dateTime.year.toInt();
                            _params['birth_month'] = dateTime.month.toInt();
                            _params['birth_day'] = dateTime.day.toInt();
                          });
                        });
                      },
                      decoration: InputDecoration(
                        border:
                            UnderlineInputBorder(borderSide: BorderSide.none),
                        hintText: '选择生日',
                        prefixIcon: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(
                              '生日',
                              style: TextStyle(
                                  color: Color(0xFF333333),
                                  fontSize: 32.rpx,
                                  fontWeight: FontWeight.w500),
                            ),
                            Text(
                              ' (仅对外显示年龄)',
                              style: TextStyle(
                                  color: Color(0xff999999), fontSize: 28.rpx),
                            ),
                          ],
                        ),
                        prefixIconConstraints:
                            BoxConstraints(minHeight: 0, maxWidth: 350.rpx),
                        suffixIcon: GestureDetector(
                          child: Image.asset(
                            "assets/images/register/arrow_right.png",
                          ),
                        ),
                        suffixIconConstraints:
                            BoxConstraints(minHeight: 0, maxWidth: 56),
                      ),
                    ),
                  ),
                  // widget.arguments['gender'] == Gender.male
                  //     ? Container()
                  //     :
                  ConstrainedBox(
                    constraints: BoxConstraints(
                      maxHeight: 100.rpx,
                    ),
                    child: new TextField(
                      controller: _heightWeightController,
                      textAlign: TextAlign.right,
                      readOnly: true,
                      onTap: () {
                        JhPickerTool.showArrayPicker(
                          context,
                          data: _heightsWeightsList,
                          title: '选择身高和体重',
                          clickCallBack: (var index, var stringData) {
                            _params['hight'] = index.first + 150;
                            _params['weight'] = index.last + 80;
                            _heightWeightController.text = stringData.join('/');
                          },
                        );
                      },
                      decoration: InputDecoration(
                        border:
                            UnderlineInputBorder(borderSide: BorderSide.none),
                        hintText: '选择身高和体重',
                        prefixIcon: Text(
                          '身高和体重',
                          style: TextStyle(
                              color: Color(0xFF333333),
                              fontSize: 32.rpx,
                              fontWeight: FontWeight.w500),
                        ),
                        prefixIconConstraints: BoxConstraints(minHeight: 0),
                        suffixIcon: GestureDetector(
                          child: Image.asset(
                            "assets/images/register/arrow_right.png",
                          ),
                        ),
                        suffixIconConstraints:
                            BoxConstraints(minHeight: 0, maxWidth: 56),
                      ),
                    ),
                  ),
                  ConstrainedBox(
                    constraints: BoxConstraints(
                      maxHeight: 100.rpx,
                    ),
                    child: new TextField(
                      controller: _locationController,
                      textAlign: TextAlign.right,
                      readOnly: true,
                      decoration: InputDecoration(
                        border:
                            UnderlineInputBorder(borderSide: BorderSide.none),
                        hintText: '根据定位自动获取城市',
                        hintStyle: TextStyle(
                            color: Color(0xFF999999), fontSize: 29.rpx),
                        prefixIcon: Text(
                          '所在地',
                          style: TextStyle(
                              color: Color(0xFF333333),
                              fontSize: 32.rpx,
                              fontWeight: FontWeight.w500),
                        ),
                        prefixIconConstraints: BoxConstraints(minHeight: 0),
                      ),
                    ),
                  ),
                  ConstrainedBox(
                    constraints: BoxConstraints(
                      maxHeight: 100.rpx,
                    ),
                    child: new TextField(
                      controller: _industryController,
                      // cursorColor: Colors.red,  //  光标颜色
                      textAlign: TextAlign.right,
                      readOnly: true,
                      onTap: () {
                        showAreaSelection(context, onSelect: (data) {
                          List value = [];
                          List<String> keys = [];
                          data['values'].forEach((f) => value.add(f));
                          data['keys'].forEach((f) => keys.add(f));
                          _params['trade'] = value[0] + value[1];
                          _params['career'] = value[2];
                          this._industryController.text = keys.join('-');
                        });
                      },
                      decoration: InputDecoration(
                        border:
                            UnderlineInputBorder(borderSide: BorderSide.none),
                        hintText: '选择所在行业',
                        prefixIcon: Text(
                          '所在行业',
                          style: TextStyle(
                              color: Color(0xFF333333),
                              fontSize: 32.rpx,
                              fontWeight: FontWeight.w500),
                        ),
                        prefixIconConstraints: BoxConstraints(minHeight: 0),
                        suffixIcon: GestureDetector(
                          child: Image.asset(
                            "assets/images/register/arrow_right.png",
                          ),
                        ),
                        suffixIconConstraints:
                            BoxConstraints(minHeight: 0, maxWidth: 56),
                      ),
                    ),
                  ),
                  ConstrainedBox(
                    constraints: BoxConstraints(
                      maxHeight: 100.rpx,
                    ),
                    child: new TextField(
                      controller: _preferenceController,
                      readOnly: true,
                      textAlign: TextAlign.right,
                      onTap: () {
                        showModalBottomSheet(
                          context: context,
                          builder: (BuildContext context) {
                            return ShowCupertinoDialog(
                              items: ['1', '2', '3'],
                              onTap: (Map result) {
                                this.setState(() {
                                  _preferenceController.text =
                                      result.values.toList().join(',');
                                  _params['preference'] =
                                      result.values.toList().join('&&');
                                });
                              },
                            );
                          },
                        );
                      },
                      decoration: InputDecoration(
                        border:
                            UnderlineInputBorder(borderSide: BorderSide.none),
                        hintText: '选择交友偏好',
                        hintStyle: TextStyle(
                            color: Color(0xFF999999), fontSize: 29.rpx),
                        prefixIcon: Text(
                          '交友偏好',
                          style: TextStyle(
                              color: Color(0xFF333333),
                              fontSize: 32.rpx,
                              fontWeight: FontWeight.w500),
                        ),
                        prefixIconConstraints: BoxConstraints(minHeight: 0),
                        suffixIcon: GestureDetector(
                          child: Image.asset(
                            "assets/images/register/arrow_right.png",
                          ),
                        ),
                        suffixIconConstraints: BoxConstraints(minHeight: 0),
                      ),
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 20.rpx)),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        '个人简介',
                        style: TextStyle(
                            color: Color(0xFF333333),
                            fontSize: 32.rpx,
                            fontWeight: FontWeight.w500),
                      ),
                      Text(
                        ' (选填)',
                        style: TextStyle(
                            color: Color(0xff999999), fontSize: 28.rpx),
                      ),
                    ],
                  ),
                  ConstrainedBox(
                    constraints: BoxConstraints(
                      minHeight: 100.rpx,
                    ),
                    child: TextField(
                      controller: _introductionController,
                      minLines: 3,
                      maxLines: 6,
                      maxLength: 100,
                      textAlign: TextAlign.left,
                      decoration: InputDecoration(
                        border:
                            UnderlineInputBorder(borderSide: BorderSide.none),
                        hintText: '请输入个人简介，让别人快速认识你',
                        hintStyle: TextStyle(
                            color: Color(0xFF999999), fontSize: 29.rpx),
                      ),
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 20.rpx)),
                  MaterialButton(
                      onPressed: _okAction,
                      child: Text(
                        "确认",
                        style: TextStyle(fontSize: 32.rpx),
                      ),
                      minWidth: double.infinity,
                      height: 50.0,
                      color: Color(0xffFFD132),
                      textColor: Color(0xff333333),
                      shape: StadiumBorder()),
                  Container(
                    margin: EdgeInsets.only(top: 28.rpx),
                    alignment: Alignment.center,
                    child: Text(
                      '请勿通过平台发布违规信息，一经举报将进行封号处理',
                      style:
                          TextStyle(color: Color(0xFF999999), fontSize: 24.rpx),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _okAction() async {
    if (_params['avatar'].length == 0) {
      toast('请选择头像');
      return;
    }

    final nickname = _nicknameController.text.trim();
    final birthday = _birthdayController.text.trim();
    final heightWeight = _heightWeightController.text.trim();
    final location = _locationController.text.trim();
    final industry = _industryController.text.trim();
    final preference = _preferenceController.text.trim();
    final introduction = _introductionController.text.trim();
    if (nickname.length == 0) {
      toast('请填写昵称');
      return;
    } else if (birthday.length == 0) {
      toast('请选择生日');
      return;
    } else if (
        // widget.arguments['gender'] == Gender.female &&
        heightWeight.length == 0) {
      toast('请选择身高和体重');
      return;
    } else if (location.length == 0) {
      toast('请填写所在地');
      return;
    } else if (industry.length == 0) {
      toast('请选择所在行业');
      return;
    } else if (preference.length == 0) {
      toast('请选择交友偏好');
      return;
    } else if (introduction.length > 0 &&
        (introduction.length < 4 || introduction.length > 100)) {
      toast('个人简介必须4-100个字符');
      return;
    }

    _params['nickname'] = nickname;
    _params['introduction'] = introduction;
    requesInfo(_params, onSuccess: (t) {
      toast("保存信息成功");
      requesGetInfo(_params['user_id'], onSuccess: (t) async {
        if (t == null) return;

        SharedPreferences getInstance = await SharedPreferences.getInstance();
        var userInfo = userInfoFromJson(getInstance.getString('userInfo'));
        userInfo.info = t;
        getInstance.setString("userInfo", json.encode(userInfo));
        Navigator.of(context)
            .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
      });
    });
    // _verifyNickname(nickname);
  }
}

//用来显示底部弹出 可滚动视图的
class ShowCupertinoDialog extends StatefulWidget {
  //内容
  List<String> items;
  //选中回调 (int,String) 对应的下标和对应的值
  Function onTap;

  ShowCupertinoDialog({
    @required this.items,
    this.onTap,
  });
  @override
  _ShowCupertinoDialogState createState() => _ShowCupertinoDialogState();
}

class _ShowCupertinoDialogState extends State<ShowCupertinoDialog> {
  Map<String, String> result = {};
  List<Widget> widgets() {
    return [
      '萝莉',
      '御姐',
      '小仙女',
      '老司机',
      '气质',
      '性感',
      '嗲妹子',
      '妩媚',
      '直爽',
      '随性',
      '清纯',
      '高冷',
      '吃货',
      '温柔',
      '爱干净',
      '萌蠢',
      '爱游戏',
      '爱运动',
      'cosplay'
    ]
        .map((item) => GestureDetector(
              onTap: () {
                // print(item);
                this.setState(() {
                  if (result.containsValue(item)) {
                    result.remove(item);
                  } else if (result.length >= 4) {
                    return false;
                  } else {
                    result[item] = item;
                  }
                });
              },
              child: Container(
                width: 150.rpx,
                height: 100.rpx,
                alignment: Alignment.center,
                decoration: new BoxDecoration(
                    borderRadius: new BorderRadius.circular(50.rpx),
                    // color: Color.fromRGBO(26, 216, 208, 0.2),
                    border: Border.all(
                        width: 1,
                        color: result.containsValue(item)
                            ? Color.fromRGBO(26, 216, 208, 1)
                            : Color(0xFF333333))),
                child: Text(item,
                    style: TextStyle(
                        color: result.containsValue(item)
                            ? Color.fromRGBO(26, 216, 208, 1)
                            : Color(0xFF333333),
                        fontSize: 24.rpx,
                        fontWeight: FontWeight.w500)),
              ),
            ))
        .toList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(width: 1, color: Color(0xffe5e5e5)))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                FlatButton(
                  child: Text('取消'),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                FlatButton(
                  onPressed: () {
                    // Navigator.pop(context);
                  },
                  child: Text(
                    '选择交友偏好',
                    // style: TextStyle(
                    //     color: Color(0xFF333333),
                    //     fontSize: 32.rpx,
                    //     fontWeight: FontWeight.w500)
                  ),
                ),
                FlatButton(
                  child: Text(
                    '确定',
                  ),
                  splashColor: Colors.grey,
                  highlightColor: Colors.white,
                  onPressed: () {
                    if (widget.onTap != null) {
                      widget.onTap(result);
                    }
                    Navigator.pop(context);
                    // if (selectIndex == null && widget.items.length > 0) {
                    //   selectIndex = 0;
                    // }
                    // if (widget.onTap != null) {
                    //   widget.onTap(selectIndex, widget.items[selectIndex]);
                    // }
                  },
                ),
              ],
            ),
          ),

          Container(
            height: 250,
            padding: EdgeInsets.only(left: 24.rpx, right: 24.rpx, top: 40.rpx),
            child: SingleChildScrollView(
              child: Wrap(
                spacing: 24.rpx, // 主轴(水平)方向间距
                runSpacing: 32.rpx, // 纵轴（垂直）方向间距
                alignment: WrapAlignment.start, //沿主轴方向居中
                children: widgets(),
              ),
            ),
          )

          // Expanded(
          //   child: CupertinoPicker(
          //     children: widget.items.map((item) {
          //       return Text(item);
          //     }).toList(),
          //     onSelectedItemChanged: (index) {
          //       print('$index');
          //       selectIndex = index;
          //     },
          //     itemExtent: 36,
          //   ),
          // ),
        ],
      ),
    );
  }
}
