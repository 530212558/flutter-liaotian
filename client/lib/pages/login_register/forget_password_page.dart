import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/http/api_defines.dart';
import 'package:huasheng_front_flutter/http/api_manager.dart';
import 'package:huasheng_front_flutter/pages/widgets/SendVerificationCode.dart';
import 'package:huasheng_front_flutter/pages/widgets/progress_hud.dart';
import 'package:huasheng_front_flutter/pages/widgets/public_ui.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';
import 'package:huasheng_front_flutter/utils/regExp.dart';

class ForgetPasswordPage extends StatefulWidget {
  @override
  _ForgetPasswordPageState createState() => _ForgetPasswordPageState();
}

class _ForgetPasswordPageState extends State<ForgetPasswordPage> {
  String _progressHUDText;
  bool _loading = false;
  TextEditingController _telController = TextEditingController();
  TextEditingController _codeController = TextEditingController();

  _showProgressHUD(String text) {
    if (mounted) {
      setState(() {
        _progressHUDText = text;
        _loading = true;
      });
    }
  }

  _dismissProgressHUD() {
    if (mounted) {
      setState(() {
        _loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return CustomProgressHUD(
      progressHUDText: _progressHUDText,
      loading: _loading,
      child: Scaffold(
        appBar: AppBar(
          brightness: Brightness.light,
          backgroundColor: Colors.transparent,
          elevation: 0,
          toolbarHeight: 44,
          leading: IconButton(
            icon: Image.asset('assets/images/login/back.png'),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Text(
            '忘记密码',
            style: TextStyle(
              color: Color(0xFF333333),
              fontSize: 17,
              fontWeight: FontWeight.w500,
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Center(
            child: Container(
              width: 590.0.rpx,
              child: Column(
                children: <Widget>[
                  Padding(padding: EdgeInsets.only(bottom: 80.rpx)),
                  ConstrainedBox(
                    constraints: BoxConstraints(
                      maxHeight: 100.rpx,
                    ),
                    child: new TextField(
                      controller: _telController,
                      obscureText: false,
                      decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              borderSide: BorderSide(
                                  color: Colors.white,
                                  width: 0,
                                  style: BorderStyle.solid)),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              borderSide: BorderSide(
                                  color: Colors.white,
                                  width: 0,
                                  style: BorderStyle.solid)),
                          fillColor: Color(0xFFF5F5F5),
                          filled: true,
                          hintText: '请输入手机号码',
                          hintStyle: TextStyle(
                              color: Color(0xFF999999), fontSize: 32.rpx),
                          contentPadding:
                              EdgeInsets.only(left: 20, top: 66.rpx),
                          suffixIcon: GestureDetector(
                            onTap: () {
                              print(_telController.text);
                              _telController.clear();
                            },
                            child: Image.asset('assets/images/login/close.png'),
                          ),
                          suffixIconConstraints: BoxConstraints(minWidth: 60)),
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 40.rpx)),
                  ConstrainedBox(
                    constraints: BoxConstraints(
                      maxHeight: 100.rpx,
                    ),
                    child: new TextField(
                      controller: _codeController,
                      style: TextStyle(textBaseline: TextBaseline.alphabetic),
                      decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              borderSide: BorderSide(
                                  color: Colors.white,
                                  width: 0,
                                  style: BorderStyle.solid)),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              borderSide: BorderSide(
                                  color: Colors.white,
                                  width: 0,
                                  style: BorderStyle.solid)),
                          fillColor: Color(0xFFF5F5F5),
                          filled: true,
                          hintText: '请输入验证码',
                          hintStyle: TextStyle(
                              color: Color(0xFF999999), fontSize: 32.rpx),
                          contentPadding:
                              EdgeInsets.only(left: 20, top: 66.rpx),
                          suffixIcon: SendVerificationCode(onTap: () async {
                            // print(this.cellphone.text);
                            return this._telController.text;
                          }, callback: (String vcode) {
                            // print(vcode);
                            this.setState(() {
                              this._codeController.text = vcode;
                            });
                          }),
                          suffixIconConstraints:
                              BoxConstraints(minWidth: 205.rpx)),
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 80.rpx)),
                  MaterialButton(
                      onPressed: _okAction,
                      child: Text(
                        '下一步',
                        style: TextStyle(fontSize: 32.rpx),
                      ),
                      minWidth: double.infinity,
                      height: 50.0,
                      color: Color(0xffFFD132),
                      textColor: Color(0xff333333),
                      shape: StadiumBorder()),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _okAction() async {
    final tel = _telController.text.trim();
    final code = _codeController.text.trim();

    if (tel.length == 0) {
      toast('请输入手机号码');
      return;
    } else if (!isValidTel(tel)) {
      toast('手机号码格式不正确');
      return;
    } else if (code.length == 0) {
      toast('请输入验证码');
      return;
    }

    _showProgressHUD('正在验证...');
    final params = {
      'cellphone': tel,
      'vcode': code,
    };
    final response =
        await APIManager().get(kAPIURLVerifyVerificationCode, params);
    _dismissProgressHUD();
    if (response.isSucceeded()) {
      Navigator.pushNamed(
        context,
        '/ResetPasswordPage',
        arguments: <String, dynamic>{
          'tel': tel,
          'vcode': code,
        },
      );
    } else {
      APIManager().toastAPIError(response);
    }
  }
}
