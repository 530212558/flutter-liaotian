import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/http/account_manager.dart';
import 'package:huasheng_front_flutter/http/api_defines.dart';
import 'package:huasheng_front_flutter/http/api_manager.dart';
import 'package:huasheng_front_flutter/http/dio.dart';
import 'package:huasheng_front_flutter/http/user.dart';
import 'package:huasheng_front_flutter/models/login_info_model.dart';
import 'package:huasheng_front_flutter/models/response_model.dart';
import 'package:huasheng_front_flutter/pages/widgets/SendVerificationCode.dart';
import 'package:huasheng_front_flutter/pages/widgets/progress_hud.dart';
import 'package:huasheng_front_flutter/pages/widgets/public_ui.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';
import 'package:huasheng_front_flutter/utils/regExp.dart';
import 'package:provider/provider.dart';
import 'package:huasheng_front_flutter/providers/User.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  String _progressHUDText;
  bool _loading = false;
  TextEditingController _telController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _codeController = TextEditingController();
  bool _passwordVisible = false;
  bool _checkboxSelected = true;

  _showProgressHUD(String text) {
    if (mounted) {
      setState(() {
        _progressHUDText = text;
        _loading = true;
      });
    }
  }

  _dismissProgressHUD() {
    if (mounted) {
      setState(() {
        _loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return CustomProgressHUD(
      progressHUDText: _progressHUDText,
      loading: _loading,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          brightness: Brightness.light,
          backgroundColor: Colors.transparent,
          elevation: 0,
          toolbarHeight: 44,
          leading: IconButton(
            icon: Image.asset('assets/images/login/back.png'),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: SingleChildScrollView(
            child: Center(
                child: Container(
              // margin: EdgeInsets.all(60.0.rpx),
              // color: Colors.amber,
              width: 590.0.rpx,
              // height: 100.0,
              child: Column(
                children: <Widget>[
                  SizedBox(height: 50),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withAlpha((0.1 * 255).toInt()),
                          offset: Offset(0, 4),
                          blurRadius: 8,
                          spreadRadius: 0,
                        ),
                      ],
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: Container(
                        width: 80,
                        height: 80,
                        child: Image.asset('assets/images/common/logo.png'),
                      ),
                    ),
                  ),
                  SizedBox(height: 70),
                  ConstrainedBox(
                    constraints: BoxConstraints(
                      maxHeight: 100.rpx,
                    ),
                    child: new TextField(
                      controller: _telController,
                      keyboardType: TextInputType.number,
                      // cursorColor: Colors.red,  //  光标颜色
                      decoration: InputDecoration(
                          // labelText: '请输入手机号',
                          //  失焦样式
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              borderSide: BorderSide(
                                  color: Colors.white,
                                  width: 0,
                                  style: BorderStyle.solid) //没什么卵效果
                              ),
                          //  选中样式
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              borderSide: BorderSide(
                                  color: Colors.white,
                                  width: 0,
                                  style: BorderStyle.solid) //没什么卵效果
                              ),
                          fillColor: Color(0xFFF5F5F5),
                          filled: true,
                          hintText: '请输入手机号码',
                          hintStyle: TextStyle(
                              color: Color(0xFF999999), fontSize: 32.rpx),
                          contentPadding:
                              EdgeInsets.only(left: 20, top: 66.rpx),

                          // prefixIcon: Icon(
                          //   Icons.phone,
                          //   color: Color.fromRGBO(204, 204, 204, 1),
                          // ),
                          // prefixIconConstraints: BoxConstraints(minWidth: 60),
                          // suffix: new IconButton(
                          //     icon: Icon(Icons.close,
                          //         size: 16,
                          //         color: Color.fromRGBO(204, 204, 204, 1)),
                          //     iconSize: 16,
                          //     padding: EdgeInsets.all(4),
                          //     alignment: Alignment.bottomCenter,
                          //     onPressed: () {
                          //       print(cellphone.text);
                          //       cellphone.clear();
                          //     }),
                          suffixIcon: GestureDetector(
                            onTap: () {
                              print(_telController.text);
                              _telController.clear();
                            },
                            child: Image.asset('assets/images/login/close.png'),
                          ),
                          suffixIconConstraints: BoxConstraints(minWidth: 60)),
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 40.rpx)),
                  ConstrainedBox(
                    constraints: BoxConstraints(
                      maxHeight: 100.rpx,
                    ),
                    child: new TextField(
                      controller: _codeController,
                      keyboardType: TextInputType.number,
                      // obscureText: !this.check,
                      // cursorColor: Colors.red,  //  光标颜色
                      style: TextStyle(textBaseline: TextBaseline.alphabetic),
                      decoration: InputDecoration(
                          // labelText: '请输入手机号',
                          //  失焦样式
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              borderSide: BorderSide(
                                  color: Colors.white,
                                  width: 0,
                                  style: BorderStyle.solid) //没什么卵效果
                              ),
                          //  选中样式
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              borderSide: BorderSide(
                                  color: Colors.white,
                                  width: 0,
                                  style: BorderStyle.solid) //没什么卵效果
                              ),
                          fillColor: Color(0xFFF5F5F5),
                          filled: true,
                          hintText: '请输入验证码',
                          hintStyle: TextStyle(
                              color: Color(0xFF999999),
                              fontSize: 32.rpx,
                              textBaseline: TextBaseline.alphabetic),
                          contentPadding:
                              EdgeInsets.only(left: 20, top: 66.rpx),

                          // prefixIcon: Icon(
                          //   Icons.phone,
                          //   color: Color.fromRGBO(204, 204, 204, 1),
                          // ),
                          // prefixIconConstraints:
                          //     BoxConstraints(minWidth: 60),
                          // suffix: new IconButton(
                          //     icon: Icon(Icons.close,
                          //         size: 16,
                          //         color: Color.fromRGBO(204, 204, 204, 1)),
                          //     iconSize: 16,
                          //     alignment: Alignment.bottomCenter,
                          //     onPressed: () {
                          //       print(password.text);
                          //       password.clear();
                          //     }),
                          suffixIcon: SendVerificationCode(onTap: () async {
                            // print(this.cellphone.text);
                            return this._telController.text;
                          }, callback: (String vcode) {
                            // print(vcode);
                            this.setState(() {
                              this._codeController.text = vcode;
                            });
                          }),
                          suffixIconConstraints:
                              BoxConstraints(minWidth: 205.rpx)),
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(bottom: 40.rpx)),
                  ConstrainedBox(
                    constraints: BoxConstraints(
                      maxHeight: 100.rpx,
                    ),
                    child: new TextField(
                      controller: _passwordController,
                      obscureText: !_passwordVisible,
                      // cursorColor: Colors.red,  //  光标颜色
                      style: TextStyle(textBaseline: TextBaseline.alphabetic),
                      decoration: InputDecoration(
                          // labelText: '请输入手机号',
                          //设置输入框前面有一个电话的按钮 suffixIcon
                          //  失焦样式
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              borderSide: BorderSide(
                                  color: Colors.white,
                                  width: 0,
                                  style: BorderStyle.solid) //没什么卵效果
                              ),
                          //  选中样式
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              borderSide: BorderSide(
                                  color: Colors.white,
                                  width: 0,
                                  style: BorderStyle.solid) //没什么卵效果
                              ),
                          fillColor: Color(0xFFF5F5F5),
                          filled: true,
                          hintText: '设置登录密码',
                          hintStyle: TextStyle(
                              color: Color(0xFF999999), fontSize: 32.rpx),
                          contentPadding:
                              EdgeInsets.only(left: 20, top: 66.rpx),

                          // prefixIcon: Icon(
                          //   Icons.phone,
                          //   color: Color.fromRGBO(204, 204, 204, 1),
                          // ),
                          // prefixIconConstraints:
                          //     BoxConstraints(minWidth: 60),
                          // suffix: IconButton(
                          //     icon: Icon(Icons.close,
                          //         size: 16,
                          //         color: Color.fromRGBO(204, 204, 204, 1)),
                          //     iconSize: 16,
                          //     alignment: Alignment.bottomCenter,
                          //     onPressed: () {
                          //       print(password.text);
                          //       password.clear();
                          //     }),
                          suffixIcon: GestureDetector(
                            onTap: () {
                              setState(() {
                                _passwordVisible = !_passwordVisible;
                              });
                            },
                            child: Image.asset(
                              _passwordVisible
                                  ? 'assets/images/login/visible.png'
                                  : 'assets/images/login/invisible.png',
                            ),
                          ),
                          // Switch(
                          //   value: this.check,
                          //   activeColor: Colors.blue, // 激活时原点颜色
                          //   onChanged: (bool val) {
                          //     print('onChanged: check');
                          //     print('${400.rpx} wangshengDemo');
                          //     // print('${Global.themes} wangshengDemo');
                          //     this.setState(() {
                          //       this.check = !this.check;
                          //     });
                          //   },
                          // ),
                          suffixIconConstraints:
                              BoxConstraints(minWidth: 120.rpx) //  120
                          ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: _checkboxSelected
                                  ? Color(0xff1AD8D0)
                                  : Colors.white,
                              border: Border.all(
                                  color: _checkboxSelected
                                      ? Color(0xff1AD8D0)
                                      : Color(0xFFCCCCCC),
                                  width: 1,
                                  style: BorderStyle.solid)),
                          child: InkWell(
                            onTap: () {
                              setState(() {
                                _checkboxSelected = !_checkboxSelected;
                              });
                            },
                            child: _checkboxSelected
                                ? Icon(
                                    Icons.check,
                                    size: 16.0,
                                    color: Colors.white,
                                  )
                                : Icon(
                                    Icons.check_box_outline_blank,
                                    size: 16.0,
                                    color: Colors.white,
                                  ),
                          ),
                        ),
                        SizedBox(width: 4),
                        RichText(
                          text: TextSpan(
                            children: <InlineSpan>[
                              TextSpan(text: '已阅读并同意'),
                              TextSpan(
                                text: '《用户协议》',
                                style: TextStyle(
                                  color: Color(0xFF1AD8D0),
                                ),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    Navigator.pushNamed(
                                      context,
                                      '/WebViewPage',
                                      arguments: <String, dynamic>{
                                        'url':
                                            'http://m.microchat.cn/doc/yonghuxieyi.html?20200928',
                                      },
                                    );
                                  },
                              ),
                              TextSpan(text: '和'),
                              TextSpan(
                                text: '《隐私政策》',
                                style: TextStyle(
                                  color: Color(0xFF1AD8D0),
                                ),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    Navigator.pushNamed(
                                      context,
                                      '/WebViewPage',
                                      arguments: <String, dynamic>{
                                        'url':
                                            'http://m.microchat.cn/doc/yinsizhengce.html?20200928',
                                      },
                                    );
                                  },
                              )
                            ],
                            style: TextStyle(
                              color: Color(0xFF999999),
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        ),
                      ],
                    ),
                    margin: EdgeInsets.only(top: 32.0.rpx, bottom: 80.rpx),
                  ),
                  MaterialButton(
                      onPressed: _register,
                      child: Text(
                        "确认",
                        style: TextStyle(fontSize: 32.rpx),
                      ),
                      minWidth: double.infinity,
                      height: 50.0,
                      color: Color(0xffFFD132),
                      textColor: Color(0xff333333),
                      shape: StadiumBorder(
                          // side: BorderSide(width: 2, style: BorderStyle.solid, color: Color(0xFF00FFFF))
                          )),
                ],
              ),
            )),
          ),
        ),
      ),
    );
  }

  _register() {
    final tel = _telController.text.trim();
    final password = _passwordController.text.trim();
    final code = _codeController.text.trim();

    if (tel.length <= 0) {
      toast('请输入手机号码');
      return;
    } else if (!isValidTel(tel)) {
      toast('手机号码格式不正确');
      return;
    } else if (!_checkboxSelected) {
      toast('请同意用户协议与隐私政策后继续');
      return;
    } else if (password.length < 6 || password.length > 20) {
      toast('密码长度需为6-20位');
      return;
    }
    var params = {
      'phone': tel,
      'password': password,
      // 'vcode': code,
    };
    requesRegister(params, onSuccess: (t) {
      // print(t['data']);
      toast("注册成功");
      Navigator.pop(context);
      // Navigator.pushNamed(context, '/LoginPage');
    });
  }
}
