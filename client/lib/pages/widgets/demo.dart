import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Demo extends StatefulWidget {
  @override
  _DemoState createState() => _DemoState();
}

class _DemoState extends State<Demo> {
  var industry;
  var pickerChildren = ['1', '2'];
  int selectedValue = 0;
  String selectedGender = "男";

  void _didClickSelectedGender() {
    selectedValue = 0;
    showCupertinoModalPopup(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: 250,
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    FlatButton(
                      color: Colors.white,
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text("Cancle"),
                    ),
                    FlatButton(
                      color: Colors.white,
                      onPressed: () {
                        Navigator.pop(context);
                        setState(() {
                          selectedGender = pickerChildren[selectedValue];
                        });
                      },
                      child: Text("OK"),
                    ),
                  ],
                ),
                Expanded(
                  child: DefaultTextStyle(
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 22,
                    ),
                    child: Container(
                      height: 200,
                      child: CupertinoPicker(
                        // diameterRatio: 1.5,
                        // offAxisFraction: 0.2, //轴偏离系数
                        // useMagnifier: true, //使用放大镜
                        // magnification: 1.5, //当前选中item放大倍数
                        itemExtent: 50, //行高
                        backgroundColor: Colors.amber, //选中器背景色
                        onSelectedItemChanged: (value) {
                          print("value = $value, 性别：${pickerChildren[value]}");
                        },
                        children: pickerChildren.map((data) {
                          return Center(
                            child: Text(data.toString()),
                          );
                        }).toList(),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Visibility(
        visible: false,
        child: Positioned(
          child: DropdownButtonHideUnderline(
            child: DropdownButton(
              icon: Text(''),
              items: [
                {
                  'id': 1,
                  'name': '全部',
                },
                {
                  'id': 2,
                  'name': '已发货',
                },
                {'id': 3, 'name': '未发货'}
              ]
                  .map((item) => DropdownMenuItem(
                      value: item,
                      child: Text(
                        item['name'],
                        style: TextStyle(),
                      )))
                  .toList(),
              onChanged: (value) {
                print(value);
                this.setState(() {
                  industry.text = value['name'];
                });
              },
            ),
          ),
          right: 0,
          top: 0,
        ),
      ),
    );
  }
}
