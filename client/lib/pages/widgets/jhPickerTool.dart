import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:date_format/date_format.dart';
import 'package:huasheng_front_flutter/models/chinaArea.dart';
import 'package:huasheng_front_flutter/providers/GlobalData.dart';

const double kPickerHeight = 216.0;
const double kItemHeight = 40.0;
const Color kBtnColor = Color(0xFF323232); //50
const Color kTitleColor = Color(0xFF787878); //120
const double kTextFontSize = 17.0;

typedef StringClickCallback = void Function(int selectIndex, Object selectStr);
typedef ArrayClickCallback = void Function(
    List<int> selecteds, List<dynamic> strData);
typedef DateClickCallback = void Function(
    dynamic selectDateStr, dynamic selectDate, DateTime time);

enum DateType {
  YMD, // y, m, d
  YM, // y ,m
  YMD_HM, // y, m, d, hh, mm
  YMD_AP_HM, // y, m, d, ap, hh, mm
}

class JhPickerTool {
  /** 单列*/
  static void showStringPicker<T>(
    BuildContext context, {
    @required List<T> data,
    String title,
    int normalIndex,
    PickerDataAdapter adapter,
    @required StringClickCallback clickCallBack,
  }) {
    openModalPicker(context,
        adapter: adapter ?? PickerDataAdapter(pickerdata: data, isArray: false),
        clickCallBack: (Picker picker, List<int> selecteds) {
      //          print(picker.adapter.text);
      clickCallBack(selecteds[0], data[selecteds[0]]);
    }, selecteds: [normalIndex ?? 0], title: title);
  }

  /** 多列 */
  static void showArrayPicker<T>(
    BuildContext context, {
    @required List<T> data,
    String title,
    List<int> normalIndex,
    PickerDataAdapter adapter,
    Function(Picker, int, List<int>) onSelect,
    @required ArrayClickCallback clickCallBack,
  }) {
    openModalPicker(context,
        adapter: adapter ?? PickerDataAdapter(pickerdata: data, isArray: true),
        clickCallBack: (Picker picker, List<int> selecteds) {
      clickCallBack(selecteds, picker.getSelectedValues());
    }, selecteds: normalIndex, title: title, onSelect: onSelect);
  }

  static void openModalPicker(
    BuildContext context, {
    @required PickerAdapter adapter,
    String title,
    List<int> selecteds,
    @required PickerConfirmCallback clickCallBack,
    Function(Picker p1, int p2, List<int> p3) onSelect,
  }) {
    new Picker(
            adapter: adapter,
            title: new Text(title ?? "请选择",
                style: TextStyle(color: kTitleColor, fontSize: kTextFontSize)),
            selecteds: selecteds,
            cancelText: '取消',
            confirmText: '确定',
            cancelTextStyle:
                TextStyle(color: kBtnColor, fontSize: kTextFontSize),
            confirmTextStyle:
                TextStyle(color: kBtnColor, fontSize: kTextFontSize),
            textAlign: TextAlign.right,
            itemExtent: kItemHeight,
            height: kPickerHeight,
            selectedTextStyle: TextStyle(color: Colors.black),
            onSelect: onSelect,
            // (Picker picker, index, selecteds) {
            //   print(selecteds);
            //   print(picker);
            //   print(index);
            // },
            onConfirm: clickCallBack)
        .showModal(context);
  }

  /** 日期选择器*/
  static void showDatePicker(
    BuildContext context, {
    DateType dateType,
    String title,
    DateTime maxValue,
    DateTime minValue,
    DateTime value,
    DateTimePickerAdapter adapter,
    @required DateClickCallback clickCallback,
  }) {
    int timeType;
    if (dateType == DateType.YM) {
      timeType = PickerDateTimeType.kYM;
    } else if (dateType == DateType.YMD_HM) {
      timeType = PickerDateTimeType.kYMDHM;
    } else if (dateType == DateType.YMD_AP_HM) {
      timeType = PickerDateTimeType.kYMD_AP_HM;
    } else {
      timeType = PickerDateTimeType.kYMD;
    }

    openModalPicker(context,
        adapter: adapter ??
            DateTimePickerAdapter(
              type: timeType,
              isNumberMonth: true,
              yearSuffix: "年",
              monthSuffix: "月",
              daySuffix: "日",
              strAMPM: const ["上午", "下午"],
              maxValue: maxValue,
              minValue: minValue,
              value: value ?? DateTime.now(),
            ),
        title: title, clickCallBack: (Picker picker, List<int> selecteds) {
      var time = (picker.adapter as DateTimePickerAdapter).value;
      var timeStr;
      if (dateType == DateType.YM) {
        timeStr = time.year.toString() + "年" + time.month.toString() + "月";
      } else if (dateType == DateType.YMD_HM) {
        timeStr = time.year.toString() +
            "年" +
            time.month.toString() +
            "月" +
            time.day.toString() +
            "日" +
            time.hour.toString() +
            "时" +
            time.minute.toString() +
            "分";
      } else if (dateType == DateType.YMD_AP_HM) {
        var str = formatDate(time, [am]) == "AM" ? "上午" : "下午";
        timeStr = time.year.toString() +
            "年" +
            time.month.toString() +
            "月" +
            time.day.toString() +
            "日" +
            str +
            time.hour.toString() +
            "时" +
            time.minute.toString() +
            "分";
      } else {
        timeStr = time.year.toString() +
            "年" +
            time.month.toString() +
            "月" +
            time.day.toString() +
            "日";
      }
//          print(formatDate(DateTime(1989, 02, 21), [yyyy, '-', mm, '-', dd]));
      clickCallback(timeStr, picker.adapter.text, time);
    });
  }
}

const double _kPickerSheetHeight = 216.0;
const double _kPickerItemHeight = 36.0;
typedef PickerConfirmCityCallback = void Function(
    List<String> stringData, List<int> selecteds);

class PickHelper {
  ///地址选择器
  static void openCityPicker(BuildContext context,
      {String title,
      @required PickerConfirmCityCallback onConfirm,
      // @required List<Map<String, Object>> data,
      String name = 'parentLevelCategory',
      String nextLevel = 'subLevelCategory',
      String selectCity = ""}) {
    var proIndex = 0;
    var cityIndex = 0;
    // var name = 'parentLevelCategory';
    // var nextLevel = 'subLevelCategory';
    List<ChinaArea> data = GlobalData.chinaAreaList;
    openModalPicker(context,
        adapter: PickerDataAdapter(
            data: data.asMap().keys.map((provincePos) {
          ChinaArea province = data[provincePos];
          List<ChinaAreaChild> citys = province.children;
          return PickerItem(
              text: Text(
                province.name,
                style: TextStyle(height: 1.6),
              ),
              // value: province['name'],
              children: citys.asMap().keys.map((cityPos) {
                // children: citys.map((cityPos) {
                var city = citys[cityPos];
                // var city = cityPos;
                // if (city == selectCity) {
                //   proIndex = provincePos;
                //   cityIndex = cityPos;
                // }
                return PickerItem(
                  //  city[name]
                  text: Text(city.name, style: TextStyle(height: 1.6)),
                  // value: '${province['name']}-->123',
                );
              }).toList());
        }).toList()),
        title: title, onConfirm: (pick, value) {
      var p = data[value[0]];
      ChinaAreaChild citys = p.children[value[1]];
      onConfirm([p.name, citys.name], value);
    }, selecteds: [proIndex, cityIndex]);
  }

  static void openModalPicker(
    BuildContext context, {
    @required PickerAdapter adapter,
    String title,
    List<int> selecteds,
    @required PickerConfirmCallback onConfirm,
  }) {
    new Picker(
      adapter: adapter,
      title: new Text(title ?? ""),
      selecteds: selecteds,
      cancelText: '取消',
      confirmText: '确定',
      cancelTextStyle: TextStyle(color: Colors.black, fontSize: 16.0),
      confirmTextStyle: TextStyle(color: Colors.black, fontSize: 16.0),
      textAlign: TextAlign.right,
      itemExtent: _kPickerItemHeight,
      height: _kPickerSheetHeight,
      selectedTextStyle: TextStyle(color: Colors.black),
      onConfirm: onConfirm,
    ).showModal(context);
  }
}
