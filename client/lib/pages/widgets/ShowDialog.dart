import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';

chooseGender(BuildContext context,
    {@required Function(int index, String sex) selectSex}) {
  showModalBottomSheet(
    backgroundColor: Color.fromRGBO(0, 0, 0, 0.1),
    context: context,
    builder: (BuildContext context) {
      return Container(
        padding: EdgeInsets.only(top: 26.rpx, bottom: 26.rpx),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(32.rpx),
              topRight: Radius.circular(32.rpx)),
        ),
        height: 594.rpx,
        child: Column(
          children: [
            Container(
              alignment: Alignment.center,
              height: 98.rpx,
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border(
                      bottom: BorderSide(
                          width: 2.rpx, color: Color((0xfff5f5f5))))),
              child: Text(
                '选择性别',
                style: TextStyle(
                    color: Color(0xff333333),
                    fontSize: 32.rpx,
                    fontWeight: FontWeight.w500),
              ),
            ),
            GestureDetector(
              onTap: () {
                selectSex(1, '不限性别');
                Navigator.pop(context);
              },
              child: Container(
                alignment: Alignment.center,
                height: 98.rpx,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(
                        bottom: BorderSide(
                            width: 2.rpx, color: Color((0xfff5f5f5))))),
                child: Text(
                  '不限性别',
                  style: TextStyle(
                      color: Color(0xff999999),
                      fontSize: 32.rpx,
                      fontWeight: FontWeight.w500),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                selectSex(2, '男');
                Navigator.pop(context);
              },
              child: Container(
                alignment: Alignment.center,
                height: 98.rpx,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(
                        bottom: BorderSide(
                            width: 2.rpx, color: Color((0xfff5f5f5))))),
                child: Text(
                  '男',
                  style: TextStyle(
                      color: Color(0xff333333),
                      fontSize: 32.rpx,
                      fontWeight: FontWeight.w500),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                selectSex(3, '女');
                Navigator.pop(context);
              },
              child: Container(
                alignment: Alignment.center,
                height: 98.rpx,
                decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border(
                        bottom: BorderSide(
                            width: 2.rpx, color: Color((0xfff5f5f5))))),
                child: Text(
                  '女',
                  style: TextStyle(
                      color: Color(0xff999999),
                      fontSize: 32.rpx,
                      fontWeight: FontWeight.w500),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                alignment: Alignment.center,
                height: 98.rpx,
                decoration: BoxDecoration(
                  color: Colors.white,
                ),
                child: Text(
                  '取消',
                  style: TextStyle(
                      color: Color(0xff999999),
                      fontSize: 32.rpx,
                      fontWeight: FontWeight.w500),
                ),
              ),
            ),
          ],
        ),
      );
    },
  );
}

showComments(BuildContext context) {
  TextEditingController _telController = TextEditingController();
  showModalBottomSheet(
      backgroundColor: Color.fromRGBO(0, 0, 0, 0.1),
      context: context,
      builder: (BuildContext context) {
        return Container(
            // height: 400.rpx,
            child: Container(
                // color: Colors.white,
                padding: EdgeInsets.only(left: 28.rpx, right: 28.rpx),
                child: Row(
                  children: [
                    Expanded(
                      child: TextField(
                        controller: _telController,
                        autofocus: true,
                        keyboardType: TextInputType.multiline,
                        minLines: 1,
                        maxLines: 5,
                        decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(44.rpx),
                            borderSide: BorderSide(
                              color: Colors.white,
                              width: 0,
                              style: BorderStyle.solid,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(44.rpx),
                            borderSide: BorderSide(
                              color: Colors.white,
                              width: 0,
                              style: BorderStyle.solid,
                            ),
                          ),
                          fillColor: Color(0xFFF5F5F5),
                          filled: true,
                          hintText: 'say hi',
                          hintStyle: TextStyle(
                            color: Color(0xFF999999),
                            fontSize: 34.rpx,
                          ),
                          contentPadding: EdgeInsets.only(
                              left: 14, top: 14, right: 0, bottom: 14),
                          suffixIcon: GestureDetector(
                            onTap: () {
                              _telController.clear();
                            },
                            child: Image.asset('assets/images/login/close.png'),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 20.rpx, right: 20.rpx),
                      width: 60.rpx,
                      child: FlatButton(
                        padding: EdgeInsets.all(0),
                        onPressed: () {},
                        child: Image.asset(
                          'assets/images/square/expression.png',
                          width: 60.rpx,
                          height: 60.rpx,
                        ),
                      ),
                    ),
                    Container(
                      // margin: EdgeInsets.only(left: 20.rpx, right: 20.rpx),
                      width: 100.rpx,
                      height: 60.rpx,
                      decoration: BoxDecoration(
                          color: Color(0xffFFD132),
                          borderRadius:
                              BorderRadius.all(Radius.circular(10.rpx))
                          // shape: BoxShape.circle,
                          ),
                      child: FlatButton(
                        padding: EdgeInsets.all(0),
                        onPressed: () {},
                        child: Text(
                          '发送',
                          style: TextStyle(
                            color: Color(0xFF333333),
                            fontSize: 28.rpx,
                          ),
                        ),
                      ),
                    ),
                  ],
                )));
      });
}

c(BuildContext context) {
  showDialog(
      context: context,
      builder: (ctx) {
        return SimpleDialog(
          title: Text("SimpleDialog"),
          titlePadding: EdgeInsets.all(10),
          backgroundColor: Colors.amber,
          elevation: 5,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(12))),
          children: <Widget>[
            ListTile(
              title: Center(
                child: Text("Item_1"),
              ),
            ),
            ListTile(
              title: Center(
                child: Text("Item_2"),
              ),
            ),
            ListTile(
              title: Center(
                child: Text("Item_3"),
              ),
            ),
            ListTile(
              title: Center(
                child: Text("Close"),
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      });
}

alertDialog({
  @required BuildContext context,
  Widget title,
  Widget content,
  Function onCancel,
  Function onConfirm,
}) {
  showDialog(
      context: context,
      // useSafeArea: false,
      useRootNavigator: true,
      builder: (context) => AlertDialog(
            title: title,
            content: content,
            actions: <Widget>[
              new FlatButton(
                child: new Text("取消"),
                onPressed: () {
                  onCancel();
                },
              ),
              new FlatButton(
                child: new Text("确定"),
                onPressed: () {
                  onConfirm();
                },
              ),
            ],
          ));
}

class ProgressDialog extends StatelessWidget {
  //子布局
  final Widget child;

  //加载中是否显示
  final bool loading;

  //进度提醒内容
  final String msg;

  //加载中动画
  final Widget progress;

  //背景透明度
  final double alpha;

  //字体颜色
  final Color textColor;

  ProgressDialog(
      {Key key,
      @required this.loading,
      this.msg,
      this.progress = const CircularProgressIndicator(),
      this.alpha = 0.6,
      this.textColor = Colors.white,
      @required this.child})
      : assert(child != null),
        assert(loading != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> widgetList = [];
    widgetList.add(child);
    if (loading) {
      Widget layoutProgress;
      if (msg == null) {
        layoutProgress = Center(
          child: progress,
        );
      } else {
        layoutProgress = Center(
          child: Container(
            padding: const EdgeInsets.all(20.0),
            decoration: BoxDecoration(
                color: Colors.black87,
                borderRadius: BorderRadius.circular(4.0)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                progress,
                Container(
                  padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0),
                  child: Text(
                    msg,
                    style: TextStyle(color: textColor, fontSize: 16.0),
                  ),
                )
              ],
            ),
          ),
        );
      }
      widgetList.add(Opacity(
        opacity: alpha,
        child: new ModalBarrier(color: Colors.black87),
      ));
      widgetList.add(layoutProgress);
    }
    return Stack(
      children: widgetList,
    );
  }
}
