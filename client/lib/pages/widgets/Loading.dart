import 'package:flutter/material.dart';

import 'package:huasheng_front_flutter/utils/extension.dart';

Set dict = Set();
bool loadingStatus = false;

class Loading {
  static BuildContext ctx;

  static void before(uri) {
    dict.add(uri); // 放入set变量中
    // 已有弹窗，则不再显示弹窗, dict.length >= 2 保证了有一个执行弹窗即可，
    if (loadingStatus == true || dict.length >= 2) return;

    loadingStatus = true; // 修改状态
    // 请求前显示弹窗
    _incrementCounter(ctx);
    // showDialog(
    //   context: ctx,
    //   builder: (context) {
    //     return Index(text: text);
    //   },
    // );
  }

  static void complete(uri) {
    dict.remove(uri);
    // 所有接口接口返回并有弹窗
    if (dict.length == 0 && loadingStatus == true) {
      loadingStatus = false; // 修改状态
      // 完成后关闭loading窗口
      Navigator.of(ctx, rootNavigator: true).pop();
    }
  }
}

void _incrementCounter(context) {
  showDialog(
    context: context,
    // 点击 dialog 外部是否可消失
    // barrierDismissible:true,
    builder: (context) {
      // 用Scaffold返回显示的内容，能跟随主题
      return Scaffold(
        backgroundColor: Colors.transparent, // 设置透明背影
        body: Center(
          // 居中显示
          child: Column(
            // 定义垂直布局
            mainAxisAlignment:
                MainAxisAlignment.center, // 主轴居中布局，相关介绍可以搜下flutter-ui的内容
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(40.rpx),
                child: Container(
                  width: 160.rpx,
                  height: 160.rpx,
                  child: Image.asset('assets/images/common/logo.png'),
                ),
              ),
              // CircularProgressIndicator自带loading效果，需要宽高设置可在外加一层sizedbox，设置宽高即可
              CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Color(0xffF25742)),
                backgroundColor: Color(0xffF5F5F5),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                'loading',
                style: TextStyle(
                    fontSize: 24,
                    color: Color(0xffffffff),
                    fontWeight: FontWeight.bold),
              ), // 文字
              // 触发关闭窗口
              // RaisedButton(
              //   child: Text(
              //     'close dialog',
              //   ),
              //   onPressed: () {
              //     print('close');
              //     Navigator.of(context, rootNavigator: true).pop();
              //   },
              // ),
            ],
          ), // 自带loading效果，需要宽高设置可在外加一层sizedbox，设置宽高即可
        ),
      );
    },
  );
}
