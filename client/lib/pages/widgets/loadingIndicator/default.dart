import 'package:flutter/material.dart';
// import 'package:huasheng_front_flutter/utils/extension.dart';

class LoadingIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: 70,
            height: 70,
            child: Image.asset('assets/images/common/logo.png'),
          ),
          Text('Loading',
              style: TextStyle(
                fontSize: 24,
                color: Color(0xFF666666),
              )),
        ],
      ),
    );
  }
}
