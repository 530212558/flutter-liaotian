import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/http/dio.dart';
import 'package:huasheng_front_flutter/pages/widgets/ShowDialog.dart';
import 'package:huasheng_front_flutter/utils/authorityCheck.dart';

import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

class PhotoView extends StatefulWidget {
  PhotoView(this.files,
      {Key key,
      this.minScale = 0.3,
      this.maxScale = 3.0,
      this.initialScale = 0.8,
      this.initialIndex = 0,
      this.scrollDirection = Axis.horizontal})
      : super(key: key);
  final List<String> files;
  final dynamic initialScale;
  final dynamic minScale;
  final dynamic maxScale;
  final int initialIndex;
  final Decoration backgroundDecoration = BoxDecoration(
    color: Color(0xff333333),
  );
  final Axis scrollDirection;

  _PhotoViewState createState() => _PhotoViewState();
}

class _PhotoViewState extends State<PhotoView> {
  int currentIndex = 0;
  @override
  void initState() {
    super.initState();
    this.setState(() {
      currentIndex = widget.initialIndex;
    });
  }

  void onPageChanged(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  _onPressed() async {
    if (await Permissions.getCamera()) {
      return Dios.savenNetworkImage(widget.files[currentIndex]);
    } else {
      alertDialog(
        context: context,
        title: Text('相册权限被拒绝'),
        content: Text('是否需要跳转到打开相册权限？'),
        onCancel: () {
          Navigator.of(context).pop();
        },
        onConfirm: () {
          Navigator.of(context).pop();
          Permissions.openAppSetting();
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
          // decoration: widget.backgroundDecoration,
          constraints: BoxConstraints.expand(
            height: MediaQuery.of(context).size.height,
          ),
          child: Stack(
              // alignment: Alignment.bottomRight,
              children: <Widget>[
                PhotoViewGallery.builder(
                  scrollPhysics: const BouncingScrollPhysics(),
                  builder: (BuildContext context, int index) {
                    return PhotoViewGalleryPageOptions(
                      imageProvider: NetworkImage(widget.files[index]),
                      initialScale: PhotoViewComputedScale.contained *
                          widget.initialScale,
                      minScale:
                          PhotoViewComputedScale.contained * widget.minScale,
                      maxScale:
                          PhotoViewComputedScale.contained * widget.maxScale,
                      heroAttributes: PhotoViewHeroAttributes(tag: index),
                    );
                  },
                  backgroundDecoration: widget.backgroundDecoration,
                  scrollDirection: widget.scrollDirection,
                  pageController:
                      PageController(initialPage: widget.initialIndex),
                  onPageChanged: onPageChanged,
                  itemCount: widget.files.length,
                  loadingBuilder: (context, event) => Center(
                    child: Container(
                      width: 20.0,
                      height: 20.0,
                      child: CircularProgressIndicator(
                        value: event == null
                            ? 0
                            : event.cumulativeBytesLoaded /
                                event.expectedTotalBytes,
                      ),
                    ),
                  ),
                ),
                //图片index显示
                Positioned(
                  top: MediaQuery.of(context).padding.top + 30,
                  width: MediaQuery.of(context).size.width,
                  child: Center(
                    child: Text("${currentIndex + 1}/${widget.files.length}",
                        style: TextStyle(color: Colors.white, fontSize: 16)),
                  ),
                ),
                //右上角关闭按钮
                Positioned(
                  right: 10,
                  top: MediaQuery.of(context).padding.top + 10,
                  child: IconButton(
                    icon: Icon(
                      Icons.close,
                      size: 30,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
                Positioned(
                  width: MediaQuery.of(context).size.width,
                  bottom: MediaQuery.of(context).padding.bottom + 16,
                  child: Center(
                      child: RaisedButton.icon(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0)),
                    color: Colors.blue,
                    icon: Icon(Icons.add, color: Colors.white),
                    label: Text(
                      "保存图片",
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: _onPressed,
                  )),
                )
              ])),
    );
  }
}
