import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewPage extends StatefulWidget {
  final Map<String, dynamic> arguments;

  WebViewPage({
    this.arguments,
  });

  @override
  _WebViewPageState createState() => _WebViewPageState();
}

class _WebViewPageState extends State<WebViewPage> {
  WebViewController _webViewController;
  String _title;

  @override
  initState() {
    super.initState();
  }

  @override
  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.transparent,
        elevation: 0,
        toolbarHeight: 44,
        leading: IconButton(
          icon: Image.asset('assets/images/login/back.png'),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          _title ?? '',
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
      ),
      body: WebView(
        onWebViewCreated: (WebViewController webViewController) {
          _webViewController = webViewController;
        },
        initialUrl: widget.arguments['url'],
        javascriptMode: JavascriptMode.unrestricted,
        onPageFinished: _onPageFinished,
      ),
    );
  }

  _onPageFinished(String url) async {
    final title = await _webViewController.getTitle();
    if (mounted) {
      setState(() {
        _title = title;
      });
    }
  }
}
