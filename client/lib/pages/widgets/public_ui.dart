import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

toast(String msg) {
  if (msg != null && msg.length > 0) {
    Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_SHORT,
      timeInSecForIosWeb: 1,
      fontSize: 16.0,
      gravity: ToastGravity.CENTER,
      backgroundColor: Colors.black,
      textColor: Colors.white,
    );
  }
}

/// 由于设计稿是iPhone X系列的，所以做相当于顶部的布局时，要加上偏移量。MediaQuery.of(context).padding.top是顶部状态栏的高度，iPhone X系列的状态栏高度是44。
topOffset(BuildContext context) {
  return MediaQuery.of(context).padding.top - 44;
}

/// 由于设计稿是iPhone X系列的，所以做相当于底部的布局时，要加上偏移量。MediaQuery.of(context).padding.bottom是底部Home Indicator的高度，iPhone X系列的Home Indicator高度是34。
bottomOffset(BuildContext context) {
  return MediaQuery.of(context).padding.bottom - 34;
}
