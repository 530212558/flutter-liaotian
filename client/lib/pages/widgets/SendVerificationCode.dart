import 'dart:async';
import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/http/api_defines.dart';
import 'package:huasheng_front_flutter/http/api_manager.dart';
import 'package:huasheng_front_flutter/pages/widgets/public_ui.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';
import 'package:huasheng_front_flutter/utils/regExp.dart';

class SendVerificationCode extends StatefulWidget {
  SendVerificationCode({@required this.onTap, this.callback});

  Function onTap;
  Function callback;
  @override
  _SendVerificationCodeState createState() =>
      _SendVerificationCodeState(onTap: onTap, callback: callback);
}

class _SendVerificationCodeState extends State<SendVerificationCode> {
  _SendVerificationCodeState({@required this.onTap, this.callback});
  Function onTap;
  Function callback;
  int count = 60;
  bool startTimer = false;
  Timer timer;

  @override
  void dispose() {
    super.dispose();
    if (this.timer != null) {
      this.timer.cancel();
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _tapAction,
      child: Container(
        alignment: Alignment(0, 0),
        height: 50,
        width: 50,
        decoration: new BoxDecoration(
          color: Color(startTimer ? 0xFFF5F5F5 : 0xffFFD132),
          borderRadius: BorderRadius.all(Radius.circular(25.0)),
        ),
        child: startTimer
            ? Text(
                "${count}秒后重发",
                style: TextStyle(color: Color(0xff999999), fontSize: 29.rpx),
              )
            : Text(
                "发送验证码",
                style: TextStyle(color: Color(0xff333333), fontSize: 29.rpx),
              ),
      ),
    );
  }

  _tapAction() async {
    String cellphone = await this.onTap();
    if (cellphone.length == 0) {
      toast('请输入手机号码');
      return;
    } else if (!isValidTel(cellphone)) {
      toast('手机号码格式不正确');
      return;
    }

    final params = {
      'cellphone': cellphone,
    };
    final response = await APIManager().get(kAPIURLVerificationCode, params);
    if (response.isSucceeded()) {
      String vcode = response.response['vcode'];
      if (callback is Function) {
        this.callback(vcode);
      }
      if (startTimer == true) return false;
      if (startTimer == false) {
        this.setState(() {
          startTimer = true;
        });
        toast('短信验证码已发送');
        Timer.periodic(Duration(milliseconds: 1000), (timer) {
          this.timer = timer;
          this.setState(() {
            count--;
          });
          if (count == 0) {
            this.setState(() {
              startTimer = false;
              count = 60;
            });
            this.timer.cancel();
          }
        });
      }
    } else {
      APIManager().toastAPIError(response);
    }
  }
}
