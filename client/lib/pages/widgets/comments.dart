import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';

comments(
    {TextEditingController controller,
    FocusNode focusNode,
    Function(String str) onSend}) {
  return Container(
      // color: Colors.white,
      // padding: EdgeInsets.only(left: 28.rpx, right: 28.rpx),
      // padding: EdgeInsets.only(bottom: 20.rpx),
      child: Row(
    children: [
      Expanded(
        child: TextField(
          focusNode: focusNode,
          controller: controller,
          textInputAction: TextInputAction.send,
          // autofocus: true,
          keyboardType: TextInputType.multiline,
          onSubmitted: (s) {
            print('$s ------------------------');
          },
          minLines: 1,
          maxLines: 5,
          decoration: InputDecoration(
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(44.rpx),
              borderSide: BorderSide(
                color: Colors.white,
                width: 0,
                style: BorderStyle.solid,
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(44.rpx),
              borderSide: BorderSide(
                color: Colors.white,
                width: 0,
                style: BorderStyle.solid,
              ),
            ),
            fillColor: Color(0xFFF5F5F5),
            filled: true,
            hintText: 'say hi',
            hintStyle: TextStyle(
              color: Color(0xFF999999),
              fontSize: 34.rpx,
            ),
            contentPadding:
                EdgeInsets.only(left: 14, top: 14, right: 0, bottom: 14),
            // suffixIcon: GestureDetector(
            //   onTap: () {
            //     _telController.clear();
            //   },
            //   child: Image.asset('assets/images/login/2.0x/close.png'),
            // ),
          ),
        ),
      ),
      Container(
        margin: EdgeInsets.only(left: 20.rpx, right: 20.rpx),
        width: 60.rpx,
        child: FlatButton(
          padding: EdgeInsets.all(0),
          onPressed: () {},
          child: Image.asset(
            'assets/images/square/expression.png',
            width: 60.rpx,
            height: 60.rpx,
          ),
        ),
      ),
      Container(
        // margin: EdgeInsets.only(left: 20.rpx, right: 20.rpx),
        width: 100.rpx,
        height: 60.rpx,
        decoration: BoxDecoration(
            color: Color(0xffFFD132),
            borderRadius: BorderRadius.all(Radius.circular(10.rpx))
            // shape: BoxShape.circle,
            ),
        child: FlatButton(
          padding: EdgeInsets.all(0),
          onPressed: () {
            if (onSend != null) {
              onSend(controller.text);
            }
          },
          child: Text(
            '发送',
            style: TextStyle(
              color: Color(0xFF333333),
              fontSize: 28.rpx,
            ),
          ),
        ),
      ),
    ],
  ));
}
