import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';

class ImgUpload extends StatefulWidget {
  @override
  _ImgUploadState createState() => _ImgUploadState();
}

class _ImgUploadState extends State<ImgUpload> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.topLeft,
      child: Container(
        width: 120,
        height: 120,
        decoration: BoxDecoration(
          color: Color(0xFFf5f5f5),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Icon(
          Icons.add,
          size: 60,
          color: Color(0xFFCBCBCB),
        ),
      ),
    );
  }
}
