import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class CustomProgressHUD extends StatefulWidget {
  final String progressHUDText;
  final bool loading;
  final Widget child;

  CustomProgressHUD({
    Key key,
    this.progressHUDText,
    this.loading,
    @required this.child,
  })  : assert(child != null),
        super(key: key);

  @override
  _CustomProgressHUDState createState() => _CustomProgressHUDState();
}

class _CustomProgressHUDState extends State<CustomProgressHUD> {
  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
      inAsyncCall: widget.loading,
      color: Colors.black.withAlpha((0.4 * 255).toInt()),
      progressIndicator: Scaffold(
        backgroundColor: Colors.black.withAlpha((0.4 * 255).toInt()),
        body: Stack(
          children: <Widget>[
            Center(
              child: Container(
                width: 100,
                height: 100,
                decoration: BoxDecoration(
                  color: Colors.black,
                  borderRadius: BorderRadius.all(
                    Radius.circular(8),
                  ),
                ),
              ),
            ),
            Center(
              child: _getCenterContent(),
            )
          ],
        ),
      ),
      child: widget.child,
    );
  }

  Widget _getCenterContent() {
    if (widget.progressHUDText == null || widget.progressHUDText.isEmpty) {
      return _getCircularProgress();
    }

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _getCircularProgress(),
          Container(
            margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
            child: Text(
              widget.progressHUDText,
              style: TextStyle(color: Colors.white),
            ),
          )
        ],
      ),
    );
  }

  Widget _getCircularProgress() {
    return CircularProgressIndicator(
      valueColor: AlwaysStoppedAnimation(Colors.white),
    );
  }
}
