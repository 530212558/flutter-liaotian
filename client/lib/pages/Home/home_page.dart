import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/http/user.dart';
import 'package:huasheng_front_flutter/models/userInfo.dart';
import 'package:huasheng_front_flutter/models/user_list_model.dart';
import 'package:huasheng_front_flutter/pages/widgets/jhPickerTool.dart';
import 'package:huasheng_front_flutter/providers/UserInfo.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  TabController controller;
  var tabs = <Tab>[];
  var tabsList = ['附近', '新注册', '女神'];
  bool _onlinePriority = false;
  bool _gender = false;
  String region = '深圳市';

  /// _nearby 附近
  List<UserList> _nearby = [];
  bool _initRequestNearby = false;

  /// _newRegistration 新注册
  List<UserList> _newRegistration = [];
  bool _initRequestnewRegistration = false;

  /// _recommend 真人或女神
  List<UserList> _recommend = [];
  bool _initRequestRecommend = false;

  // List<List<UserList>> _list = [_nearby, _newRegistration, _recommend];
  initList() {
    this._nearby = [];
    this._newRegistration = [];
    this._recommend = [];
  }

  getUserList() {
    UserInfo userInfo =
        Provider.of<UserInfoProvider>(context, listen: false).userInfo;
    double longitude = userInfo.info.longitude;
    double latitude = userInfo.info.latitude;

    int gender = _gender ? 2 : 1;

    int index = controller.index;

    requesGetUsers(
        gender: gender,
        longitude: longitude,
        latitude: latitude,
        onSuccess: (list) {
          this.setState(() {
            if (index == 0) {
              this._nearby.addAll(list);
            } else if (index == 1) {
              this._newRegistration.addAll(list);
            } else if (index == 2) {
              this._recommend.addAll(list);
            }
          });
        });
  }

  changeOnline() {
    this.setState(() {
      _onlinePriority = !_onlinePriority;
      this.initList();
    });
    _resetInitRequest();
    _initializationRequest();
    this.getUserList();
  }

  changeGender() {
    this.setState(() {
      _gender = !_gender;
      this.initList();
    });
    _resetInitRequest();
    _initializationRequest();
    this.getUserList();
  }

  changeIndustry() {
    PickHelper.openCityPicker(context,
        title: '选择所在城市', name: 'province', nextLevel: 'cites',
        onConfirm: (List<String> stringData, List<int> selecteds) {
      // this.industry.text = stringData.join('-');
      // this.parem['trade'] = stringData.join('-');
      var region = stringData[1];

      this.setState(() {
        this.region = region;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    UserInfo userInfo =
        Provider.of<UserInfoProvider>(context, listen: false).userInfo;
    if (userInfo.info != null) {
      region = userInfo.info.city;
    }

    tabs = tabsList
        .map((String item) => Tab(
              child: Container(
                child: Text(
                  item,
                ),
              ),
            ))
        .toList();

    //initialIndex初始选中第几个
    controller =
        TabController(initialIndex: 0, length: tabs.length, vsync: this);
    controller.addListener(() {
      // 添加不触发两次执行
      if (controller.index == controller.animation.value) {
        if (_initializationRequest() == true) {
          this.getUserList();
        }
      }
    });
    this.setState(() {
      _nearby = [];
      _newRegistration = [];
      _recommend = [];
    });
    _initializationRequest();
    this.getUserList();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _initializationRequest() {
    var req = false;
    if (controller.index == 0 && _initRequestNearby == false) {
      _initRequestNearby = true;
      req = true;
    } else if (controller.index == 1 && _initRequestnewRegistration == false) {
      _initRequestnewRegistration = true;
      req = true;
    } else if (controller.index == 2 && _initRequestRecommend == false) {
      _initRequestRecommend = true;
      req = true;
    }
    return req;
  }

  _resetInitRequest() {
    _initRequestNearby = false;
    _initRequestnewRegistration = false;
    _initRequestRecommend = false;
  }

  @override
  Widget build(BuildContext context) {
    var viewIndex = 0;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        centerTitle: false,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(
              width: 128.rpx,
              height: 48.rpx,
              child: FlatButton(
                onPressed: () {
                  this.changeOnline();
                },
                padding: EdgeInsets.all(0),
                child: Text(
                  '在线优先',
                  style: TextStyle(
                      color: _onlinePriority
                          ? Color(0xff1AD8D0)
                          : Color(0xff999999),
                      fontSize: 24.rpx),
                ),
                color: Colors.white,
                shape: RoundedRectangleBorder(
                    side: BorderSide(
                      color: _onlinePriority
                          ? Color(0xff1AD8D0)
                          : Color(0xffF5F5F5),
                      width: 1,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(24.rpx))),
              ),
            ),
            GestureDetector(
              onTap: () {
                this.changeIndustry();
              },
              child: Row(
                children: [
                  Text(
                    '$region',
                    style: TextStyle(
                        color: Color(0xff333333),
                        fontSize: 28.rpx,
                        fontWeight: FontWeight.w500),
                  ),
                  Image.asset(
                    'assets/images/home/down.png',
                    width: 28.rpx,
                    height: 28.rpx,
                  )
                ],
              ),
            ),
            SizedBox(
              width: 128.rpx,
              height: 48.rpx,
              child: FlatButton(
                onPressed: () {
                  this.changeGender();
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Image.asset(
                      _gender == false
                          ? 'assets/images/home/MaleSelect.png'
                          : 'assets/images/home/Male.png',
                      width: 24.rpx,
                      height: 24.rpx,
                    ),
                    Image.asset(
                      _gender
                          ? 'assets/images/home/FemaleSelected.png'
                          : 'assets/images/home/Female.png',
                      width: 24.rpx,
                      height: 24.rpx,
                    )
                  ],
                ),
                color: Colors.white,
                shape: RoundedRectangleBorder(
                    side: BorderSide(
                      color: Color(0xffF5F5F5),
                      width: 2.rpx,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(24.rpx))),
              ),
            ),
          ],
        ),
      ),
      body: Container(
          color: Colors.white,
          padding: EdgeInsets.only(left: 28.rpx, right: 28.rpx),
          child: Column(children: <Widget>[
            TabBar(
              controller: controller,
              tabs: tabs,
              labelColor: Color(0xff333333), //选中标签颜色
              labelStyle: TextStyle(
                fontSize: 36.0.rpx,
                fontWeight: FontWeight.w500,
              ),
              unselectedLabelColor: Color(0xff979797),
              unselectedLabelStyle: TextStyle(
                fontSize: 32.0.rpx,
                fontWeight: FontWeight.w400,
              ),
              indicatorColor: Color(0xffFFD132),
              indicatorWeight: 6.rpx,
              indicatorPadding:
                  EdgeInsets.only(left: (48.rpx), right: (48.rpx)),
              // labelPadding: EdgeInsets.symmetric(horizontal: (30)),
              // unselectedLabelColor: Colors.blue,
              isScrollable: true, //能否滑动,false：tab宽度则等比，true：tab宽度则包裹item
            ),
            Padding(padding: EdgeInsets.only(bottom: 28.rpx)),
            Expanded(
              flex: 1,
              child: TabBarView(
                  controller: controller,
                  children: tabs.map((Tab tab) {
                    List<UserList> list = [];

                    if (viewIndex == 0) {
                      list = this._nearby;
                    } else if (viewIndex == 1) {
                      list = this._newRegistration;
                    } else if (viewIndex == 2) {
                      list = this._recommend;
                    }
                    viewIndex++;
                    // print('${list}');
                    return GridView.builder(
                      // padding: const EdgeInsets.all(10.0),
                      // List<UserList> _nearby = [];
                      // List<UserList> _newRegistration = [];
                      // List<UserList> _recommend = [];
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        childAspectRatio: 0.76, //宽高比为1时，子widget
                        mainAxisSpacing: 26.rpx,
                        crossAxisSpacing: 26.rpx,
                      ),
                      itemCount: list.length,
                      itemBuilder: (BuildContext context, int index) {
                        // if (index == picList.length - 1) {
                        //   _getPicList();
                        // }
                        UserList item = list[index];

                        return GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(
                              context,
                              '/UserPage',
                              arguments: <String, dynamic>{
                                'UserList': item.toJson(),
                              },
                            );
                          },
                          child: Stack(
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  // color: Colors.red,
                                  image: DecorationImage(
                                    image: NetworkImage(item.avatar),
                                    fit: BoxFit.cover,
                                  ),
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(20.0.rpx),
                                  ),
                                ),
                              ),
                              Positioned(
                                // top: 300.rpx,
                                // right: 100,
                                bottom: 0,
                                left: 0,
                                right: 0,
                                child: Container(
                                  // width: 300.rpx,
                                  height: 220.rpx,
                                  decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                      begin: Alignment.topCenter,
                                      end: Alignment.bottomCenter,
                                      colors: [
                                        Color.fromRGBO(0, 0, 0, 0),
                                        Color.fromRGBO(0, 0, 0, 1),
                                      ],
                                    ),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(20.0.rpx),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.all(16.rpx),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      children: [
                                        Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Offstage(
                                                offstage: false,
                                                child: Container(
                                                  width: 80.rpx,
                                                  height: 36
                                                      .rpx, //  auth 是否已认证，1:未认证，2:已真人认证，3:已女神认证
                                                  decoration: BoxDecoration(
                                                      gradient: LinearGradient(
                                                          colors: 2 == 2
                                                              ? [
                                                                  Color(
                                                                      0xff1AD8D0),
                                                                  Color(
                                                                      0xff1AD89E)
                                                                ]
                                                              : [
                                                                  Color(
                                                                      0xffFE7C96),
                                                                  Color(
                                                                      0xffFE997C)
                                                                ]), // 渐变色
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              24.rpx)),
                                                  child: FlatButton(
                                                    padding: EdgeInsets.all(0),
                                                    onPressed: () {},
                                                    child: Text(
                                                      2 == 2 ? '真人' : '女神',
                                                      style: TextStyle(
                                                          color:
                                                              Color(0xffffffff),
                                                          fontSize: 24.rpx),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                width: 40.rpx,
                                                height: 40.rpx,
                                                child: FlatButton(
                                                  padding: EdgeInsets.all(0),
                                                  onPressed: () {},
                                                  child: Image.asset(
                                                    //  like 是否喜欢，1:未喜欢，2:喜欢
                                                    2 == 2
                                                        ? 'assets/images/home/ChooseLike.png'
                                                        : 'assets/images/home/NoClickLike.png',
                                                    width: 40.rpx,
                                                    height: 40.rpx,
                                                  ),
                                                  // color: Colors.black,
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  24.rpx))),
                                                ),
                                              )
                                            ]),
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        Row(
                                            // mainAxisAlignment:
                                            //     MainAxisAlignment.start,
                                            children: [
                                              Expanded(
                                                  flex: 3,
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      Text(
                                                        item.nickname,
                                                        maxLines: 1,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: TextStyle(
                                                            color: Color(
                                                                0xffffffff),
                                                            fontWeight:
                                                                FontWeight.w500,
                                                            fontSize: 28.rpx),
                                                      ),
                                                    ],
                                                  )),
                                              Offstage(
                                                // 是否为VIP，1:非VIP用户，2:VIP用户
                                                offstage: 1 == 1 ?? false,
                                                child: SizedBox(
                                                  width: 50.rpx,
                                                  height: 28.rpx,
                                                  child: FlatButton(
                                                    padding: EdgeInsets.all(0),
                                                    onPressed: () {},
                                                    child: Image.asset(
                                                      'assets/images/home/vip.png',
                                                      width: 50.rpx,
                                                      height: 28.rpx,
                                                    ),
                                                    // color: Colors.black,
                                                    shape: RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius.all(
                                                                Radius.circular(
                                                                    24.rpx))),
                                                  ),
                                                ),
                                              )
                                            ]),
                                        Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Opacity(
                                                opacity: 0.8,
                                                child: Text(
                                                  '${item.age}岁 • ${item.career} • ',
                                                  style: TextStyle(
                                                      color: Color(0xffffffff),
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontSize: 24.rpx),
                                                ),
                                              ),
                                              Expanded(
                                                child: Text(
                                                  '${item.preference}',
                                                  maxLines: 1,
                                                  textAlign: TextAlign.right,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                      color: Color(0xffffffff),
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontSize: 24.rpx),
                                                ),
                                              )
                                            ]),
                                        Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Row(
                                                children: [
                                                  Image.asset(
                                                    'assets/images/home/position.png',
                                                    width: 40.rpx,
                                                    height: 40.rpx,
                                                  ),
                                                  Text(
                                                    '${item.distance.getDistance}${item.distance.getDistanceUnit}',
                                                    style: TextStyle(
                                                        color:
                                                            Color(0xffffffff),
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 24.rpx),
                                                  ),
                                                  Text(
                                                    ' • 在线',
                                                    style: TextStyle(
                                                        color: '在线' != '在线'
                                                            ? Color.fromRGBO(
                                                                255,
                                                                255,
                                                                255,
                                                                0.5)
                                                            : Color(0xff1AD8D0),
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 24.rpx),
                                                  ),
                                                ],
                                              ),
                                              Row(
                                                children: [
                                                  Text(
                                                    '${item.album.length}',
                                                    style: TextStyle(
                                                        color:
                                                            Color(0xffffffff),
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        fontSize: 24.rpx),
                                                  ),
                                                  Image.asset(
                                                    'assets/images/home/freephotos.png',
                                                    width: 40.rpx,
                                                    height: 40.rpx,
                                                  ),
                                                ],
                                              ),
                                            ]),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        );
                        // buildItem(picList[index]);
                      },
                    );
                  }).toList()),
            ),
          ])),
    );
  }

  // getAsyncData() async {
  //   // 获取实例
  //   var prefs = await SharedPreferences.getInstance();
  //   // 获取存储数据
  //   var count = (prefs.getInt('count') ?? 0) + 1;
  //   // print(count);
  //   // 设置存储数据
  //   await prefs.setInt('count', count);
  // }
}
