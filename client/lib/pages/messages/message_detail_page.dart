import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:huasheng_front_flutter/http/news.dart';
import 'package:huasheng_front_flutter/http/socket/news.dart';
import 'package:huasheng_front_flutter/models/FriendsNewsList.dart';
import 'package:huasheng_front_flutter/providers/FriendsNewsList.dart';
import 'package:huasheng_front_flutter/providers/UserInfo.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';
import 'package:huasheng_front_flutter/pages/widgets/comments.dart';
import 'package:provider/provider.dart';

class MessagesDetailPage extends StatefulWidget {
  final Map<String, dynamic> arguments;
  MessagesDetailPage({this.arguments});
  _MessagesDetailPageState createState() => _MessagesDetailPageState();
}

class _MessagesDetailPageState extends State<MessagesDetailPage> {
  FocusNode _focusNode = FocusNode();
  TextEditingController _telController = TextEditingController();

  int _id;
  String _nickname;
  String _avatar;

  @override
  void initState() {
    super.initState();
    _id = widget.arguments['id'];
    _nickname = widget.arguments['nickname'];
    _avatar = widget.arguments['avatar'];
    // 监听输入框焦点变化
    _focusNode.addListener(_onFocus);
    SchedulerBinding.instance.addPostFrameCallback((_) {
      //build完成后的回调

      var getFriendsNewsList =
          Provider.of<FriendsNewsListProvider>(context, listen: false)
              .getFriendsNewsList;
      for (var i = 0; i < getFriendsNewsList.length; i++) {
        var item = getFriendsNewsList[i];
        friendsNewsList = item;
        if (item.userId == _id) {
          if (item.newsList == null) {
            int id = Provider.of<UserInfoProvider>(context, listen: false)
                .userInfo
                .id;
            getNewsList(id, _id, 1, 1, 10, onSuccess: (List<News> listNews) {
              this.setState(() {
                this.listNews = listNews;
                _scrollToend();
              });
              item.newsList = listNews;
            });
          } else {
            this.setState(() {
              this.listNews = item.newsList;
              _scrollToend();
            });
          }
          break;
        }
      }
    });
  }

  @override
  void dispose() {
    // 组件销毁时，释放资源（一定不能忘，否则可能会引起内存泄露）
    super.dispose();
    _focusNode.dispose();
  }

  List<News> listNews = [];
  FriendsNewsList friendsNewsList;

  // 焦点变化时触发的函数
  _onFocus() {
    if (_focusNode.hasFocus) {
      // print('hasFocus');
      // 聚焦时候的操作
      return;
    } else {
      // print('unhasFocus');
    }
  }

  ScrollController scrollController = ScrollController();
  void _scrollToend() {
    var widgetsBinding = WidgetsBinding.instance;
    widgetsBinding.addPostFrameCallback((callback) {
      // print("addPostFrameCallback be invoke");
      scrollController.jumpTo(scrollController.position.maxScrollExtent);
    });
  }

  Future<bool> _refreshPull() async {
    // setState(() {
    //   _list.clear();
    //   _serviceList.clear();
    //   _describeList.clear();
    // });
    // _getCourse();
    // _getAdvertisement();
    // _getTuitionService();
    return false;
  }

  _onSend(String content) {
    //创建时间对象，获取当前时间
    DateTime now = new DateTime.now();
    print("当前时间：$now -----content: $content");
    int from =
        Provider.of<UserInfoProvider>(context, listen: false).userInfo.id;
    var item = {
      // "id": 102,
      "from": from,
      "to": _id,
      "message_type": 1,
      "content": content,
      "creation_time": '$now'
    };

    NewsIo.sendFriend(item, callback: (int id) {
      item['id'] = id;
      var news = News.fromJson(item);
      this.setState(() {
        this.listNews.add(news);
        _scrollToend();
      });
      if (friendsNewsList != null) {
        friendsNewsList.news = news;
      }
    });
  }

  Widget _appBar() {
    return AppBar(
        iconTheme: IconThemeData(
          color: Color(0xFF333333),
        ),
        leading: GestureDetector(
          child: Icon(Icons.arrow_back_ios),
          onTap: () => Navigator.pop(context),
        ),
        backgroundColor: Colors.white,
        elevation: 0.0,
        centerTitle: true,
        title: Text(
          '$_nickname',
          style: TextStyle(color: Colors.black),
        ),
        actions: <Widget>[
          SizedBox(
            width: 88.rpx,
            height: 48.rpx,
            child: FlatButton(
              onPressed: () {
                // _openModalBottomSheet();
              },
              padding: EdgeInsets.all(0),
              child: Image.asset(
                'assets/images/square/MoreBlack.png',
                width: 40.rpx,
                height: 8.rpx,
              ),
            ),
          ),
        ]);
  }

  Widget _buildListItem(BuildContext context, int index) {
    News item = this.listNews[index];
    var sex = 1 == 1
        ? 'assets/images/square/GenderMale.png'
        : 'assets/images/square/GenderFemale.png';
    var realAuth = 1 == 1
        ? [Color(0xff1AD8D0), Color(0xff1AD89E)]
        : [Color(0xffFE7C96), Color(0xffFE997C)];
    var realAuthStr = '';
    // 0 未认证 1 真人 2 女神 3男神
    if (1 == 1)
      realAuthStr = '真人';
    else if (1 == 2)
      realAuthStr = '女神';
    else if (1 == 3) realAuthStr = '男神';
    var selfPublish = 1 == 1 ? false : true;
    var likeStatus = 1 == 1
        ? 'assets/images/square/PraiseAsh.png'
        : 'assets/images/square/PraiseAshActive.png';
    var commentStatus = 1 == 1
        ? 'assets/images/square/CommentGrey.png'
        : 'assets/images/square/CommentGreyActive.png';
    var row = item.from == _id;
    String avatar = row
        ? _avatar
        : Provider.of<UserInfoProvider>(context, listen: true)
            .userInfo
            .info
            .avatar;
    return Container(
      margin: EdgeInsets.only(bottom: 28.rpx),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        textDirection: row ? TextDirection.ltr : TextDirection.rtl,
        children: [
          Container(
              width: 80.rpx,
              height: 80.rpx,
              margin: EdgeInsets.only(
                right: row ? 20.rpx : 28.rpx,
                left: row ? 28.rpx : 20.rpx,
              ),
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      image: NetworkImage(avatar), fit: BoxFit.cover))),
          Container(
            // alignment: row ? Alignment.centerLeft : Alignment.centerRight,
            decoration: BoxDecoration(
                color: Color(row ? 0xffF5F5F5 : 0xff1AD8D0),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(row ? 0 : 40.rpx),
                  topRight: Radius.circular(!row ? 0 : 40.rpx),
                  bottomLeft: Radius.circular(40.rpx),
                  bottomRight: Radius.circular(40.rpx),
                )),
            padding: EdgeInsets.only(
                bottom: 18.rpx, top: 18.rpx, left: 28.rpx, right: 28.rpx),
            constraints: BoxConstraints(
                minHeight: 80.rpx,
                // minWidth: SIZE_SCALE(20),
                maxWidth: 494.rpx),
            child: Text(
              // row ? 'hi，小姐姐，你好啊👋 $index' : '你好，在干嘛呢 $index',
              item.content,
              // textAlign: item.from == _id ? TextAlign.left : TextAlign.right,
              // textDirection: TextDirection.ltr, //文本方向
              style: TextStyle(
                  color: Color(row ? 0xff333333 : 0xffffffff),
                  fontSize: 32.rpx,
                  fontWeight: FontWeight.w400),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: _appBar(),
        backgroundColor: Colors.white,
        // floatingActionButton: FloatingActionButton(
        //   onPressed: _scrollToend,
        //   tooltip: 'Scroll to end',
        //   child: Icon(Icons.add),
        // ),
        // bottomNavigationBar: comments(),
        body: Column(
          children: [
            Expanded(
              child: GestureDetector(
                onTap: () {
                  _focusNode.unfocus();
                },
                child: // RefreshIndicator(
                    //   backgroundColor: Colors.red,
                    //   child:
                    CustomScrollView(
                  controller: scrollController,
                  // reverse: true,

                  ///反弹效果 BouncingScrollPhysics
                  physics: BouncingScrollPhysics(),
                  slivers: <Widget>[
                    //AppBar，包含一个导航栏
                    // SliverAppBar(
                    //   pinned: true,
                    //   // expandedHeight: 250.0,
                    //   // title: comments(),
                    //   flexibleSpace: FlexibleSpaceBar(
                    //     title: Text('Demo'),
                    //     // title: comments(),
                    //   ),
                    // ),
                    // 如果不是Sliver家族的Widget，需要使用 SliverToBoxAdapter 做层包裹
                    // SliverToBoxAdapter(
                    //   child: Container(
                    //       height: 120.rpx,
                    //       color: Colors.green,
                    //       child: Column(
                    //         children: [
                    //           Text('HeaderView'),
                    //         ],
                    //       )),
                    // ),
                    CupertinoSliverRefreshControl(
                      /// 刷新过程中悬浮高度
                      refreshIndicatorExtent: 60,

                      ///触发刷新的距离
                      refreshTriggerPullDistance: 100,

                      onRefresh: () async {
                        setState(() {
                          // _list.add(_list.length + 1);
                          // childCount += 3;
                        });
                        //模拟网络请求
                        await Future.delayed(Duration(milliseconds: 200));
                        //结束刷新
                        return Future.value(true);
                      },
                    ),
                    SliverList(
                      delegate: SliverChildBuilderDelegate(_buildListItem,
                          childCount: listNews.length),
                    ),
                  ],
                ),
                //   onRefresh: () {
                //     // //下拉刷新具体操作
                //     return _refreshPull();
                //   },
                // ),)
              ),
            ),
            comments(
                controller: _telController,
                focusNode: _focusNode,
                onSend: _onSend)
          ],
        ));
    // return Center(
    //   // child:  Text("分类页面")
    //   child: Consumer<Counter>(
    //       builder: (context, counter, _) => Text('`分类页面${counter.count}')),
    // );
  }
}
