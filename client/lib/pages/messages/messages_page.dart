import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:huasheng_front_flutter/models/FriendsNewsList.dart';
import 'package:huasheng_front_flutter/providers/FriendsNewsList.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';
import 'package:provider/provider.dart';
import '../../providers/Counter.dart';

class MessagesPage extends StatefulWidget {
  MessagesPage({Key key}) : super(key: key);

  _MessagesPageState createState() => _MessagesPageState();
}

class _MessagesPageState extends State<MessagesPage> {
  ScrollController scrollController = ScrollController();

  @override
  void dispose() {
    // 组件销毁时，释放资源（一定不能忘，否则可能会引起内存泄露）
    super.dispose();
    this.scrollController.dispose();
  }

  Widget _appBar() {
    return AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        centerTitle: false,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              '消息',
              style: TextStyle(color: Colors.black),
            )
          ],
        ),
        leading: GestureDetector(
          child: Icon(Icons.arrow_back_ios),
          // onTap: () => Navigator.pop(context),
        ),
        actions: <Widget>[
          SizedBox(
            width: 88.rpx,
            height: 48.rpx,
            child: FlatButton(
              onPressed: () {
                // _openModalBottomSheet();
              },
              padding: EdgeInsets.all(0),
              child: Icon(
                Icons.add_circle_outline,
                size: 48.rpx,
                color: Colors.grey[800],
              ),
            ),
          ),
        ]);
  }

  Widget _buildListItem(BuildContext context, int index) {
    List<FriendsNewsList> friendsNewsList =
        Provider.of<FriendsNewsListProvider>(context, listen: true)
            .getFriendsNewsList;
    FriendsNewsList item = friendsNewsList[index];
    var sex = 1 == 1
        ? 'assets/images/square/GenderMale.png'
        : 'assets/images/square/GenderFemale.png';
    var realAuth = 1 == 1
        ? [Color(0xff1AD8D0), Color(0xff1AD89E)]
        : [Color(0xffFE7C96), Color(0xffFE997C)];
    var realAuthStr = '';
    // 0 未认证 1 真人 2 女神 3男神
    if (1 == 1)
      realAuthStr = '真人';
    else if (1 == 2)
      realAuthStr = '女神';
    else if (1 == 3) realAuthStr = '男神';
    return Slidable(
      closeOnScroll: true,
      actionPane: SlidableScrollActionPane(), //滑出选项的面板 动画
      actionExtentRatio: 0.25,
      child: GestureDetector(
          onTap: () {
            // print('object');
            Navigator.pushNamed(
              context,
              '/message_detail_page',
              arguments: <String, dynamic>{
                'nickname': item.nickname,
                'id': item.userId,
                'avatar': item.avatar,
              },
            );
          },
          child: Container(
            margin: EdgeInsets.only(bottom: 28.rpx),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                    width: 120.rpx,
                    height: 120.rpx,
                    margin: EdgeInsets.only(right: 28.rpx, left: 28.rpx),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: NetworkImage(item.avatar),
                            fit: BoxFit.cover))),
                Expanded(
                    child: Container(
                  height: 120.rpx,
                  padding: EdgeInsets.only(right: 28.rpx),
                  // decoration: BoxDecoration(
                  //   border: Border(
                  //       bottom:
                  //           BorderSide(width: 1, color: Color(0xffF5F5F5))),
                  // ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Row(
                              children: [
                                Container(
                                  constraints: BoxConstraints(
                                    maxWidth: 300.rpx,
                                  ),
                                  child: Text(
                                    item.nickname,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        color: Color(0xff333333),
                                        fontSize: 32.rpx,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: 12.rpx, right: 12.rpx),
                                  child: Image.asset(
                                    sex,
                                    width: 36.rpx,
                                    height: 36.rpx,
                                  ),
                                ),
                                Offstage(
                                  offstage: realAuthStr == '' ? true : false,
                                  child: Container(
                                    width: 80.rpx,
                                    height: 36.rpx,
                                    decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                            colors: realAuth), // 渐变色
                                        borderRadius:
                                            BorderRadius.circular(24.rpx)),
                                    child: FlatButton(
                                      padding: EdgeInsets.all(0),
                                      onPressed: () {},
                                      child: Text(
                                        realAuthStr,
                                        style: TextStyle(
                                            color: Color(0xffffffff),
                                            fontSize: 24.rpx),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Text(item.news.creationTime.toString(),
                              style: TextStyle(
                                  color: Color(0xff999999),
                                  fontWeight: FontWeight.w400,
                                  fontSize: 28.rpx))
                        ],
                      ),
                      // Padding(
                      //   padding: EdgeInsets.only(top: 8.rpx),
                      // ),
                      Text(
                        item.news.content,
                        style: TextStyle(
                            color: Color(0xff999999),
                            fontWeight: FontWeight.w400,
                            fontSize: 28.rpx),
                      )
                    ],
                  ),
                )),
              ],
            ),
          )),

      actions: <Widget>[
        //左侧按钮列表
        IconSlideAction(
          caption: 'Archive',
          color: Colors.blue,
          icon: Icons.archive,
          onTap: () {
            print('_showSnackBar(Archive)');
          },
        ),
        IconSlideAction(
          caption: 'Share',
          color: Colors.indigo,
          icon: Icons.share,
          onTap: () {
            print('_showSnackBar(Archive)');
          },
        ),
      ],
      secondaryActions: <Widget>[
        //右侧按钮列表
        IconSlideAction(
          caption: 'More',
          color: Colors.black45,
          icon: Icons.more_horiz,
          onTap: () {
            print('_showSnackBar(Archive)');
          },
        ),
        IconSlideAction(
          caption: 'Delete',
          color: Colors.red,
          icon: Icons.delete,
          closeOnTap: false,
          onTap: () {
            print('_showSnackBar(Archive)');

            // Slidable.of(context)?.renderingMode == SlidableRenderingMode.none
            //     ? Slidable.of(context)?.open()
            //     : Slidable.of(context)?.close();
          },
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    List<FriendsNewsList> friendsNewsList =
        Provider.of<FriendsNewsListProvider>(context, listen: true)
            .getFriendsNewsList;
    return Scaffold(
      appBar: _appBar(),
      backgroundColor: Colors.white,
      body: CustomScrollView(
        slivers: <Widget>[
          // 如果不是Sliver家族的Widget，需要使用 SliverToBoxAdapter 做层包裹
          SliverToBoxAdapter(
            child: Container(
                height: 120.rpx,
                color: Colors.green,
                child: Column(
                  children: [
                    Text('HeaderView'),
                  ],
                )),
          ),
          // 当列表项高度固定时，使用 SliverFixedExtendList 比 SliverList 具有更高的性能
          SliverFixedExtentList(
              delegate: SliverChildBuilderDelegate(_buildListItem,
                  childCount: friendsNewsList.length),
              itemExtent: 120.rpx)
        ],
      ),
      // ListView.builder(
      //     controller: this.scrollController,
      //     // separatorBuilder: (BuildContext context, int index) {
      //     //   return Divider(color: Color(0xffF5F5F5), thickness: 2
      //     //       // color: Colors.red,
      //     //       );
      //     // },
      //     itemCount: 20,
      //     itemBuilder: (BuildContext context, int index) {

      //     }),
    );
    // return Center(
    //   // child:  Text("分类页面")
    //   child: Consumer<Counter>(
    //       builder: (context, counter, _) => Text('`分类页面${counter.count}')),
    // );
  }
}
