import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/models/user_list_model.dart';
import 'package:huasheng_front_flutter/pages/user/widgets/user_page_album_grid_item.dart';
import 'package:huasheng_front_flutter/pages/widgets/PhotoView.dart';
import 'package:huasheng_front_flutter/pages/widgets/public_ui.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';

class UserPage extends StatefulWidget {
  @override
  _UserPageState createState() => _UserPageState();

  final Map<String, dynamic> arguments;

  UserPage({
    this.arguments,
  });
}

class _UserPageState extends State<UserPage> {
  List<String> tagsList = ['萝莉', '女神', '气质女神'];
  List<String> postImagesList = ['萝莉', '女神', '气质女神'];
  UserList _userList;

  @override
  initState() {
    super.initState();
    _userList = UserList.fromJson(widget.arguments['UserList']);
  }

  @override
  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF5F5F5),
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.transparent,
        elevation: 0,
        toolbarHeight: 44,
        leading: IconButton(
          icon: Image.asset('assets/images/user/back.png'),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          IconButton(
            icon: Image.asset('assets/images/user/more.png'),
            onPressed: () {},
          ),
        ],
      ),
      extendBodyBehindAppBar: true,
      body: Stack(
        children: [
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            bottom: 83 + bottomOffset(context),
            child: ListView(
              padding: EdgeInsets.all(0),
              children: [
                _topWidget(),
                _albumWidget(),
                SizedBox(height: 10),
                _postsWidget(),
                SizedBox(height: 10),
                _infoWidget(),
                // Container(
                //   height: 40,
                //   child: Center(
                //     child: Text(
                //       '请忽通过平台进行不法交易，一经举报核实将做封号处理',
                //       style: TextStyle(
                //         color: Color(0xFF999999),
                //         fontSize: 12,
                //       ),
                //     ),
                //   ),
                // ),
              ],
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: _bottomBar(),
          )
        ],
      ),
    );
  }

  Widget _topWidget() {
    return Container(
      height: 328 + topOffset(context),
      child: Stack(
        children: [
          Positioned(
            left: 0,
            top: 0,
            right: 0,
            bottom: 0,
            child: Image.network(
              _userList.avatar,
              fit: BoxFit.cover,
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: Container(
              height: 201,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.transparent, Colors.black],
                  stops: [0, 1],
                ),
              ),
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 16,
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 4),
                  child: Text(
                    _userList.nickname,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 7),
                  child: Text(
                    '${_userList.city} ${_userList.age}岁 ${_userList.career}',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 12),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset('assets/images/user/location.png'),
                      Text(
                        '${_userList.distance.getDistance}${_userList.distance.getDistanceUnit}',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                        ),
                      ),
                      Text(
                        '在线',
                        style: TextStyle(
                          color: Color(0xFF1AD8D0),
                          fontSize: 12,
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 4),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 8, right: 8),
                        height: 18,
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                            colors: [
                              Color(0xFFFE7C96),
                              Color(0xFFFE997C),
                            ],
                            stops: [0, 1],
                          ),
                          borderRadius: BorderRadius.circular(9),
                        ),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              '女神',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 12,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(width: 6),
                      Text(
                        '她通过了系统女神认证',
                        style: TextStyle(
                          color: Colors.white.withAlpha((0.8 * 255).toInt()),
                          fontSize: 12,
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 14),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        padding: EdgeInsets.only(left: 8, right: 8),
                        height: 18,
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                            colors: [
                              Color(0xFF1AD8D0),
                              Color(0xFF1AD89E),
                            ],
                            stops: [0, 1],
                          ),
                          borderRadius: BorderRadius.circular(9),
                        ),
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              '真人',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 12,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(width: 6),
                      Text(
                        '她通过了真人人脸识别',
                        style: TextStyle(
                          color: Colors.white.withAlpha((0.8 * 255).toInt()),
                          fontSize: 12,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16),
                topRight: Radius.circular(16),
              ),
              child: Container(
                color: Colors.white,
                height: 16,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _albumWidget() {
    return Container(
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(14, 4, 0, 0),
            child: Text(
              '她的相册',
              style: TextStyle(
                color: Color(0xFF333333),
                fontSize: 18,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          GridView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            padding: EdgeInsets.all(14),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 4,
              mainAxisSpacing: 9,
              crossAxisSpacing: 9,
            ),
            itemBuilder: (context, index) => UserPageAlbumGridItem(
              imageURL: _userList.album[index].url,
              onTap: () {
                List<String> _files = [];
                _userList.album.forEach((item) {
                  _files.add(item.url);
                });
                Navigator.of(context).push(PageRouteBuilder(
                    pageBuilder: (c, a, s) => PhotoView(
                          _files,
                          initialIndex: index,
                        )));
              },
            ),
            itemCount: _userList.album.length,
          ),
        ],
      ),
    );
  }

  Widget _postsWidget() {
    return Container(
      color: Colors.white,
      height: 50,
      child: Flex(
        direction: Axis.horizontal,
        children: [
          SizedBox(width: 14),
          Text(
            '她的动态',
            style: TextStyle(
              color: Color(0xFF333333),
              fontSize: 18,
              fontWeight: FontWeight.w500,
            ),
          ),
          Expanded(
            child: Wrap(
              alignment: WrapAlignment.end,
              spacing: 6,
              runSpacing: 6,
              children:
                  postImagesList.map((tag) => _postImageItem(tag)).toList(),
            ),
          ),
          SizedBox(width: 14),
          Image.asset('assets/images/user/arrow_right.png'),
          SizedBox(width: 14),
        ],
      ),
    );
  }

  Widget _postImageItem(String text) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(20),
      child: Container(
        color: Color(0xFFEEEEEE),
        width: 40,
        height: 40,
        child: Image.network(
          'https://pic1.zhimg.com/80/v2-7bd11fc667e00aedfc5bf3fa62d8dabd_1440w.jpg',
          fit: BoxFit.cover,
        ),
      ),
    );
  }

  Widget _infoWidget() {
    return Container(
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(14, 13, 0, 0),
            child: Text(
              '她的信息',
              style: TextStyle(
                color: Color(0xFF333333),
                fontSize: 18,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(14, 20, 0, 0),
            child: Row(
              children: [
                Container(
                  width: 78,
                  child: Text(
                    '身高',
                    style: TextStyle(
                      color: Color(0xFF999999),
                      fontSize: 16,
                    ),
                  ),
                ),
                Text(
                  '${_userList.hight}cm',
                  style: TextStyle(
                    color: Color(0xFF666666),
                    fontSize: 16,
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(14, 8, 0, 0),
            child: Row(
              children: [
                Container(
                  width: 78,
                  child: Text(
                    '体重',
                    style: TextStyle(
                      color: Color(0xFF999999),
                      fontSize: 16,
                    ),
                  ),
                ),
                Text(
                  '${_userList.weight}kg',
                  style: TextStyle(
                    color: Color(0xFF666666),
                    fontSize: 16,
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(14, 8, 14, 0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 78,
                  child: Text(
                    '个人标签',
                    style: TextStyle(
                      color: Color(0xFF999999),
                      fontSize: 16,
                    ),
                  ),
                ),
                Expanded(
                  child: Wrap(
                    spacing: 6,
                    runSpacing: 6,
                    children: _userList.preference
                        .split('-')
                        .map((tag) => _tagItem(tag))
                        .toList(),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(14, 8, 14, 14),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 78,
                  child: Text(
                    '个人简介',
                    style: TextStyle(
                      color: Color(0xFF999999),
                      fontSize: 16,
                    ),
                  ),
                ),
                Expanded(
                  child: Text(
                    _userList.introduction,
                    style: TextStyle(
                      color: Color(0xFF666666),
                      fontSize: 16,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _tagItem(String text) {
    return Container(
      padding: EdgeInsets.only(left: 8, right: 8),
      height: 22,
      decoration: BoxDecoration(
        color: Color(0xFFFE7C96).withAlpha((0.2 * 255).toInt()),
        borderRadius: BorderRadius.circular(11),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            text,
            style: TextStyle(
              color: Color(0xFFFE7C96),
              fontSize: 14,
              fontWeight: FontWeight.w500,
            ),
          ),
        ],
      ),
    );
  }

  Widget _bottomBar() {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Color(0xFF666666).withAlpha((0.2 * 255).toInt()),
            offset: Offset(0, 2),
            blurRadius: 8,
            spreadRadius: 0,
          ),
        ],
      ),
      height: 83 + bottomOffset(context),
      child: Stack(
        children: [
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            height: 44,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                _bottomBarItem('assets/images/user/star.png', '评价', () {}),
                _bottomBarItem('assets/images/user/chat.png', '私聊', () {
                  Navigator.pushNamed(
                    context,
                    '/message_detail_page',
                    arguments: <String, dynamic>{
                      'nickname': _userList.nickname,
                      'id': _userList.id,
                      'avatar': _userList.avatar,
                    },
                  );
                }),
                _bottomBarItem('assets/images/user/mic.png', '连麦', () {}),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _bottomBarItem(String imageAssetName, String text, Function() func) {
    return FlatButton(
      onPressed: func,
      child: Row(
        children: [
          Image.asset(imageAssetName),
          SizedBox(width: 2),
          Text(
            text,
            style: TextStyle(
              color: Color(0xFF333333),
              fontSize: 16,
            ),
          ),
        ],
      ),
    );
  }
}
