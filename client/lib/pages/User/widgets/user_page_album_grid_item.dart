import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UserPageAlbumGridItem extends StatefulWidget {
  final String imageURL;
  final Function() onTap;

  UserPageAlbumGridItem({
    Key key,
    this.imageURL,
    this.onTap,
  }) : super(key: key);

  @override
  _UserPageAlbumGridItemState createState() => _UserPageAlbumGridItemState();
}

class _UserPageAlbumGridItemState extends State<UserPageAlbumGridItem> {
  @override
  initState() {
    super.initState();
  }

  @override
  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Container(
          color: Color(0xFFEEEEEE),
          child: Stack(
            children: <Widget>[
              Positioned(
                left: 0,
                top: 0,
                right: 0,
                bottom: 0,
                child: Image.network(
                  // 'https://pic1.zhimg.com/80/v2-7bd11fc667e00aedfc5bf3fa62d8dabd_1440w.jpg',
                  widget.imageURL,
                  fit: BoxFit.cover,
                ),
              ),
              // Positioned(
              //   left: 8,
              //   bottom: 6,
              //   child: Text(
              //     '本人',
              //     style: TextStyle(
              //       color: Color(0xFF1AD8D0),
              //       fontSize: 12,
              //     ),
              //   ),
              // ),
              // Positioned(
              //   left: 0,
              //   top: 0,
              //   right: 0,
              //   bottom: 0,
              //   child: BackdropFilter(
              //     filter: ImageFilter.blur(sigmaX: 10, sigmaY: 10),
              //     child: Center(
              //       child: Text(
              //         '阅后即焚',
              //         style: TextStyle(
              //           color: Colors.white.withAlpha((0.5 * 255).toInt()),
              //           fontSize: 14,
              //           fontWeight: FontWeight.w500,
              //         ),
              //       ),
              //     ),
              //   ),
              // ),
              // Positioned(
              //   left: 0,
              //   top: 0,
              //   right: 0,
              //   bottom: 0,
              //   child: Container(
              //     decoration: BoxDecoration(
              //       color: Colors.black.withAlpha((0.6 * 255).toInt()),
              //     ),
              //     child: Center(
              //       child: Text(
              //         '更多+4',
              //         style: TextStyle(
              //           color: Colors.white,
              //           fontSize: 14,
              //           fontWeight: FontWeight.w500,
              //         ),
              //       ),
              //     ),
              //   ),
              // ),
            ],
          ),
        ),
      ),
    );
  }
}
