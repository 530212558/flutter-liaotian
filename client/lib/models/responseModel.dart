import 'package:huasheng_front_flutter/models/model_utils.dart';

class ResponseModel {
  /// 错误码
  int statusCode;

  // 错误信息
  String message;

  /// 原始响应JSON对象
  dynamic data;

  /// 是否响应成功
  // bool isSucceeded() {
  //   return status == kAPIResultStatusSucceeded;
  // }

  /// JSON转换成对象，接口响应时用
  ResponseModel.fromJson(Map<String, dynamic> json)
      : statusCode = json["statusCode"],
        message = json['message'] == null ? null : json['message'],
        data = json["data"];
}
