import 'package:huasheng_front_flutter/models/model_utils.dart';

class LoginInfo {
  String openID = '';
  String resourceToken = '';
  String reflushToken = '';

  LoginInfo();

  /// JSON转换成对象，接口响应时用
  LoginInfo.fromJson(Map<String, dynamic> json)
      : openID = parseToString(json['openid']),
        resourceToken = parseToString(json['resourcetoken']),
        reflushToken = parseToString(json['reflushtoken']);

  /// JSON转换成对象，存储时用
  LoginInfo.fromJson2(Map<String, dynamic> json)
      : openID = parseToString(json['openID']),
        resourceToken = parseToString(json['resourceToken']),
        reflushToken = parseToString(json['reflushToken']);

  /// 转换成JSON对象，存储时用
  Map<String, dynamic> toJson() => <String, dynamic>{
        'openID': openID,
        'resourceToken': resourceToken,
        'reflushToken': reflushToken,
      };
}
