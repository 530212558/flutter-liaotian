/// 性别
enum Gender {
  /// 未知
  none,

  /// 男
  male,

  /// 女
  female
}

class User {
  Gender gender;
  User();
}
