// To parse this JSON data, do
//
//     final userInfo = userInfoFromJson(jsonString);

import 'dart:convert';

UserInfo userInfoFromJson(String str) => UserInfo.fromJson(json.decode(str));

String userInfoToJson(UserInfo data) => json.encode(data.toJson());

class UserInfo {
  UserInfo({
    this.id,
    this.name,
    this.phone,
    this.info,
    this.token,
  });

  int id;
  String name;
  String phone;
  Info info;
  String token;

  factory UserInfo.fromJson(Map<String, dynamic> json) => UserInfo(
        id: json["id"],
        name: json["name"],
        phone: json["phone"],
        info: json["info"] == null ? null : Info.fromJson(json["info"]),
        token: json["token"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "phone": phone,
        "info": info == null ? null : info.toJson(),
        "token": token,
      };
}

Info infoFromJson(String str) => Info.fromJson(json.decode(str));

String infoToJson(Info data) => json.encode(data.toJson());

class Info {
  Info({
    this.id,
    this.userId,
    this.gender,
    this.nickname,
    this.avatar,
    this.birthYear,
    this.birthMonth,
    this.birthDay,
    this.hight,
    this.weight,
    this.longitude,
    this.latitude,
    this.province,
    this.city,
    this.district,
    this.address,
    this.trade,
    this.career,
    this.preference,
    this.introduction,
    this.updateTime,
  });

  int id;
  int userId;
  String gender;
  String nickname;
  String avatar;
  int birthYear;
  int birthMonth;
  int birthDay;
  int hight;
  int weight;
  double longitude;
  double latitude;
  String province;
  String city;
  String district;
  String address;
  String trade;
  String career;
  String preference;
  String introduction;
  DateTime updateTime;

  factory Info.fromJson(Map<String, dynamic> json) => Info(
        id: json["id"],
        userId: json["user_id"],
        gender: json["gender"],
        nickname: json["nickname"],
        avatar: json["avatar"],
        birthYear: json["birth_year"],
        birthMonth: json["birth_month"],
        birthDay: json["birth_day"],
        hight: json["hight"],
        weight: json["weight"],
        longitude: json["longitude"],
        latitude: json["latitude"],
        province: json["province"],
        city: json["city"],
        district: json["district"],
        address: json["address"],
        trade: json["trade"],
        career: json["career"],
        preference: json["preference"],
        introduction: json["introduction"],
        updateTime: DateTime.parse(json["update_time"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "gender": gender,
        "nickname": nickname,
        "avatar": avatar,
        "birth_year": birthYear,
        "birth_month": birthMonth,
        "birth_day": birthDay,
        "hight": hight,
        "weight": weight,
        "longitude": longitude,
        "latitude": latitude,
        "province": province,
        "city": city,
        "district": district,
        "address": address,
        "trade": trade,
        "career": career,
        "preference": preference,
        "introduction": introduction,
        "update_time": updateTime.toIso8601String(),
      };
}
