/// 安全转换成int类型
int parseToInt(dynamic x) {
  if (x != null) {
    if (x is int) {
      return x;
    } else if (x is bool) {
      return x ? 1 : 0;
    } else if (x is double) {
      return x.toInt();
    } else if (x is String) {
      return int.tryParse(x) ?? 0;
    } else {
      return int.tryParse(x) ?? 0; // TODO
    }
  } else {
    return 0;
  }
}

/// 安全转换成bool类型
bool parseToBool(dynamic x) {
  if (x != null) {
    return parseToInt(x) == 1;
  } else {
    return false;
  }
}

/// 安全转换成String类型
String parseToString(dynamic x) {
  if (x != null) {
    return x.toString();
  } else {
    return '';
  }
}

/// 安全转换成List类型
List<T> parseToList<T>(List x) {
  if (x != null && x.length > 0) {
    if (x.first is int ||
        x.first is bool ||
        x.first is double ||
        x.first is String) {
      final list = x.cast<T>();
      return list ?? [];
      // } else if (T is Model) {
      //   final list = x.map((i) => Model.fromJson(i)).toList().cast<T>(); // TODO
      //   return list ?? [];
    } else {
      return [];
    }
  } else {
    return [];
  }
}
