// To parse this JSON data, do
//
//     final dynamics = dynamicsFromJson(jsonString);

import 'dart:convert';

List<Dynamics> dynamicsFromJson(String str) =>
    List<Dynamics>.from(json.decode(str).map((x) => Dynamics.fromJson(x)));

String dynamicsToJson(List<Dynamics> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Dynamics {
  Dynamics({
    this.id,
    this.content,
    this.enableComment,
    this.sameSexHide,
    this.verifyState,
    this.verifyStateName,
    this.createdOn,
    this.province,
    this.city,
    this.district,
    this.fileUrls,
    this.openId,
    this.nickName,
    this.realAuth,
    this.selfPublish,
    this.sex,
    this.avatar,
    this.lat,
    this.lon,
    this.showOption,
    this.likeStatus,
    this.likedTotal,
    this.commentStatus,
    this.commentTotal,
  });

  int id;
  String content;
  int enableComment;
  int sameSexHide;
  int verifyState;
  String verifyStateName;
  String createdOn;
  String province;
  String city;
  String district;
  List<String> fileUrls;
  String openId;
  String nickName;
  int realAuth;
  int selfPublish;
  int sex;
  String avatar;
  int lat;
  int lon;
  bool showOption;
  int likeStatus;
  int likedTotal;
  int commentStatus;
  int commentTotal;

  factory Dynamics.fromJson(Map<String, dynamic> json) => Dynamics(
        id: json["ID"],
        content: json["Content"],
        enableComment: json["EnableComment"],
        sameSexHide: json["SameSexHide"],
        verifyState: json["VerifyState"],
        verifyStateName: json["VerifyStateName"],
        createdOn: json["CreatedOn"],
        province: json["Province"],
        city: json["City"],
        district: json["District"],
        fileUrls: List<String>.from(json["FileUrls"].map((x) => x)),
        openId: json["OpenId"],
        nickName: json["NickName"],
        realAuth: json["RealAuth"],
        selfPublish: json["SelfPublish"],
        sex: json["Sex"],
        avatar: json["Avatar"],
        lat: json["Lat"],
        lon: json["Lon"],
        showOption: json["ShowOption"],
        likeStatus: json["LikeStatus"],
        likedTotal: json["LikedTotal"],
        commentStatus: json["CommentStatus"],
        commentTotal: json["CommentTotal"],
      );

  Map<String, dynamic> toJson() => {
        "ID": id,
        "Content": content,
        "EnableComment": enableComment,
        "SameSexHide": sameSexHide,
        "VerifyState": verifyState,
        "VerifyStateName": verifyStateName,
        "CreatedOn": createdOn,
        "Province": province,
        "City": city,
        "District": district,
        "FileUrls": List<dynamic>.from(fileUrls.map((x) => x)),
        "OpenId": openId,
        "NickName": nickName,
        "RealAuth": realAuth,
        "SelfPublish": selfPublish,
        "Sex": sex,
        "Avatar": avatar,
        "Lat": lat,
        "Lon": lon,
        "ShowOption": showOption,
        "LikeStatus": likeStatus,
        "LikedTotal": likedTotal,
        "CommentStatus": commentStatus,
        "CommentTotal": commentTotal,
      };
}
