// To parse this JSON data, do
//
//     final friendsNewsList = friendsNewsListFromJson(jsonString);

import 'dart:convert';

List<FriendsNewsList> friendsNewsListFromJson(String str) =>
    List<FriendsNewsList>.from(
        json.decode(str).map((x) => FriendsNewsList.fromJson(x)));

String friendsNewsListToJson(List<FriendsNewsList> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FriendsNewsList {
  FriendsNewsList({
    this.userId,
    this.nickname,
    this.avatar,
    this.news,
    List<News> newsList,
  });

  int userId;
  String nickname;
  String avatar;
  News news;
  List<News> newsList;

  factory FriendsNewsList.fromJson(Map<String, dynamic> json) =>
      FriendsNewsList(
          userId: json["user_id"],
          nickname: json["nickname"],
          avatar: json["avatar"],
          news: newsFromJson(json["news"]),
          newsList: json["newsList"] == null
              ? null
              : newsListFromJson(json["newsList"]));

  Map<String, dynamic> toJson() => {
        "user_id": userId,
        "nickname": nickname,
        "avatar": avatar,
        "news": news,
        "newsList": newsList == null ? null : newsList,
      };
}

List<News> newsListFromJson(String str) =>
    List<News>.from(json.decode(str).map((x) => News.fromJson(x)));

News newsFromJson(String str) => News.fromJson(json.decode(str));

String newsToJson(News data) => json.encode(data.toJson());

class News {
  News({
    this.id,
    this.to,
    this.from,
    this.content,
    this.messageType,
    this.creationTime,
  });

  int id;
  int to;
  int from;
  String content;
  int messageType;
  DateTime creationTime;

  factory News.fromJson(Map<String, dynamic> json) => News(
        id: json["id"],
        to: json["to"],
        from: json["from"],
        content: json["content"],
        messageType: json["message_type"],
        creationTime: DateTime.parse(json["creation_time"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "to": to,
        "from": from,
        "content": content,
        "message_type": messageType,
        "creation_time": creationTime.toIso8601String(),
      };
}
