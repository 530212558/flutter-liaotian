// To parse this JSON data, do
//
//     final chinaArea = chinaAreaFromJson(jsonString);

import 'dart:convert';

List<ChinaArea> chinaAreaFromJson(String str) =>
    List<ChinaArea>.from(json.decode(str).map((x) => ChinaArea.fromJson(x)));

String chinaAreaToJson(List<ChinaArea> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ChinaArea {
  ChinaArea({
    this.children,
    this.id,
    this.code,
    this.name,
    this.province,
  });

  List<ChinaAreaChild> children;
  String id;
  int code;
  String name;
  int province;

  factory ChinaArea.fromJson(Map<String, dynamic> json) => ChinaArea(
        children: List<ChinaAreaChild>.from(
            json["children"].map((x) => ChinaAreaChild.fromJson(x))),
        id: json["_id"],
        code: json["code"],
        name: json["name"],
        province: json["province"],
      );

  Map<String, dynamic> toJson() => {
        "children": List<dynamic>.from(children.map((x) => x.toJson())),
        "_id": id,
        "code": code,
        "name": name,
        "province": province,
      };
}

class ChinaAreaChild {
  ChinaAreaChild({
    this.code,
    this.name,
    this.province,
    this.children,
    this.city,
    this.area,
  });

  dynamic code;
  String name;
  dynamic province;
  List<ChildChild> children;
  String city;
  String area;

  factory ChinaAreaChild.fromJson(Map<String, dynamic> json) => ChinaAreaChild(
        code: json["code"],
        name: json["name"],
        province: json["province"],
        children: json["children"] == null
            ? null
            : List<ChildChild>.from(
                json["children"].map((x) => ChildChild.fromJson(x))),
        city: json["city"] == null ? null : json["city"],
        area: json["area"] == null ? null : json["area"],
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "name": name,
        "province": province,
        "children": children == null
            ? null
            : List<dynamic>.from(children.map((x) => x.toJson())),
        "city": city == null ? null : city,
        "area": area == null ? null : area,
      };
}

class ChildChild {
  ChildChild({
    this.code,
    this.name,
    this.province,
    this.city,
    this.area,
  });

  String code;
  String name;
  String province;
  String city;
  String area;

  factory ChildChild.fromJson(Map<String, dynamic> json) => ChildChild(
        code: json["code"],
        name: json["name"],
        province: json["province"],
        city: json["city"],
        area: json["area"],
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "name": name,
        "province": province,
        "city": city,
        "area": area,
      };
}
