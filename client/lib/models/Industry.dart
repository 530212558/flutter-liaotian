// To parse this JSON data, do
//
//     final industry = industryFromJson(jsonString);

import 'dart:convert';

List<Industry> industryFromJson(String str) =>
    List<Industry>.from(json.decode(str).map((x) => Industry.fromJson(x)));

String industryToJson(List<Industry> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Industry {
  Industry({
    this.code,
    this.name,
    this.subLevelModelList,
  });

  int code;
  String name;
  List<Industry> subLevelModelList;

  factory Industry.fromJson(Map<String, dynamic> json) => Industry(
        code: json["code"],
        name: json["name"],
        subLevelModelList: json["subLevelModelList"] == null
            ? null
            : List<Industry>.from(
                json["subLevelModelList"].map((x) => Industry.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "name": name,
        "subLevelModelList": subLevelModelList == null
            ? null
            : List<dynamic>.from(subLevelModelList.map((x) => x.toJson())),
      };
}
