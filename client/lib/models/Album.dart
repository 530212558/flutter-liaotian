// To parse this JSON data, do
//
//     final album = albumFromJson(jsonString);

import 'dart:convert';

List<Album> albumFromJson(String str) =>
    List<Album>.from(json.decode(str).map((x) => Album.fromJson(x)));

String albumToJson(List<Album> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Album {
  Album({
    this.id,
    this.url,
    this.userId,
    this.purpose,
  });

  int id;
  String url;
  int userId;
  String purpose;

  factory Album.fromJson(Map<String, dynamic> json) => Album(
        id: json["id"],
        url: json["url"],
        userId: json["user_id"],
        purpose: json["purpose"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "url": url,
        "user_id": userId,
        "purpose": purpose,
      };
}
