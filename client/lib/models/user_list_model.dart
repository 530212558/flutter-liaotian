// To parse this JSON data, do
//
//     final userList = userListFromJson(jsonString);

import 'dart:convert';

import 'package:huasheng_front_flutter/models/Album.dart';

List<UserList> userListFromJson(String str) =>
    List<UserList>.from(json.decode(str).map((x) => UserList.fromJson(x)));

String userListToJson(List<UserList> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class UserList {
  UserList(
      {this.distance,
      this.id,
      this.name,
      this.userId,
      this.gender,
      this.nickname,
      this.avatar,
      this.age,
      this.hight,
      this.weight,
      this.longitude,
      this.latitude,
      this.province,
      this.city,
      this.district,
      this.address,
      this.trade,
      this.career,
      this.preference,
      this.introduction,
      this.updateTime,
      this.album});

  double distance;
  int id;
  String name;
  int userId;
  String gender;
  String nickname;
  String avatar;
  int age;
  int hight;
  int weight;
  double longitude;
  double latitude;
  String province;
  String city;
  String district;
  String address;
  String trade;
  String career;
  String preference;
  String introduction;
  DateTime updateTime;
  List<Album> album;

  factory UserList.fromJson(Map<String, dynamic> json) => UserList(
        distance: json["distance"].toDouble(),
        id: json["id"],
        name: json["name"],
        userId: json["user_id"],
        gender: json["gender"],
        nickname: json["nickname"],
        avatar: json["avatar"],
        age: json["age"],
        hight: json["hight"],
        weight: json["weight"],
        longitude: json["longitude"].toDouble(),
        latitude: json["latitude"].toDouble(),
        province: json["province"],
        city: json["city"],
        district: json["district"],
        address: json["address"],
        trade: json["trade"],
        career: json["career"],
        preference: json["preference"],
        introduction: json["introduction"],
        updateTime: DateTime.parse(json["update_time"]),
        album: List<Album>.from(json["album"].map((x) => Album.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "distance": distance,
        "id": id,
        "name": name,
        "user_id": userId,
        "gender": gender,
        "nickname": nickname,
        "avatar": avatar,
        "age": age,
        "hight": hight,
        "weight": weight,
        "longitude": longitude,
        "latitude": latitude,
        "province": province,
        "city": city,
        "district": district,
        "address": address,
        "trade": trade,
        "career": career,
        "preference": preference,
        "introduction": introduction,
        "update_time": updateTime.toIso8601String(),
        "album": List<dynamic>.from(album.map((x) => x.toJson())),
      };
}
