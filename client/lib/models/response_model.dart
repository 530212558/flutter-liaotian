import 'package:huasheng_front_flutter/http/api_defines.dart';
import 'package:huasheng_front_flutter/models/model_utils.dart';

class ResponseModel {
  /// 错误码
  String status;

  /// 错误信息
  String message;

  /// 原始响应JSON对象
  Map<String, dynamic> response;

  /// 是否响应成功
  bool isSucceeded() {
    return status == kAPIResultStatusSucceeded;
  }

  /// JSON转换成对象，接口响应时用
  ResponseModel.fromJson(Map<String, dynamic> json)
      : status = json != null ? parseToString(json[kAPIResultStatus]) : '',
        message = json != null ? parseToString(json[kAPIResultMessage]) : '',
        response = json;
}
