// import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/utils/extension.dart';

TextStyle title_1() {
  return TextStyle(
      fontSize: 32.rpx, color: Color(0xff333333), fontWeight: FontWeight.w500);
}

TextStyle title_1_auxiliary(
    {double fontSize = 28, dynamic color = 0xff999999}) {
  return TextStyle(
      fontSize: fontSize.rpx, color: Color(color), fontWeight: FontWeight.w400);
}

BorderSide default_border({double width = 2}) {
  return BorderSide(width: width.rpx, color: Color(0xfff5f5f5));
}
