import 'package:amap_location/amap_location.dart';
import 'package:amap_search_fluttify/amap_search_fluttify.dart';
// import 'package:amap_map_fluttify/amap_map_fluttify.dart';
import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/providers/Counter.dart';
import 'package:huasheng_front_flutter/providers/FriendsNewsList.dart';
import 'package:huasheng_front_flutter/providers/GlobalData.dart';
import 'package:huasheng_front_flutter/providers/User.dart';
import 'package:huasheng_front_flutter/providers/UserInfo.dart';
import 'package:huasheng_front_flutter/routes/index.dart';
// import 'package:huasheng_front_flutter/test.dart';
import 'package:huasheng_front_flutter/utils/global.dart';
import 'package:provider/provider.dart';

void main() => Global.init().then((_) {
      //  王生--> 高德 IOS key 2533d6fbb2c79cbadc847d20263404fc
      //  王生--> 高德 Android key ee985d25d47fa386d15c992a4bac2008
      // await AmapService.instance.init(
      //   iosKey: 'bf2775ecf0c61d253c12253ba65049bd',
      //   androidKey: 'ee985d25d47fa386d15c992a4bac2008',
      //   webApiKey: 'a9e8afb275650673a7e50ff7880badaf', //web
      // );
      // amap_search_fluttify: ^0.16.2 #高德地图搜索组件（初始化）
      AmapCore.init('2533d6fbb2c79cbadc847d20263404fc');

      //  关闭打印
      WidgetsFlutterBinding.ensureInitialized();
      //  集成高德地图定位ios版本 amap_location
      AMapLocationClient.setApiKey("2533d6fbb2c79cbadc847d20263404fc");
      //  开启 amap_location 定位
      AMapLocationClient.startup(new AMapLocationOption(
          desiredAccuracy:
              CLLocationAccuracy.kCLLocationAccuracyHundredMeters));

      return runApp(MultiProvider(providers: [
        ChangeNotifierProvider.value(value: Counter()),
        ChangeNotifierProvider.value(value: User()),
        ChangeNotifierProvider.value(value: UserInfoProvider()),
        ChangeNotifierProvider.value(value: GlobalData()),
        ChangeNotifierProvider.value(value: FriendsNewsListProvider()),
      ], child: MyApp()));
      // child: MaterialApp(
      //   home: HeadImageUploadPage(),
      // )));
    });

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/',
      onGenerateRoute: onGenerateRoute,
      debugShowCheckedModeBanner: false,
    );
  }
}
