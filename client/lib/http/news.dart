import 'package:huasheng_front_flutter/http/dio.dart';
import 'package:huasheng_front_flutter/models/FriendsNewsList.dart';

const prefix = "/news";

requesGetFriendsNewsList(int userId,
    {Function(List<FriendsNewsList> t) onSuccess}) {
  Dios.request(prefix + '/getFriendsNewsList?user_id=$userId',
      method: Dios.GET,
      onSuccess: onSuccess,
      fromJson: friendsNewsListFromJson);
}

getNewsList(int from, int to, int messageType, int page, int size,
    {Function(List<News> t) onSuccess}) {
  Dios.request(
      prefix +
          '/getNewsList?from=$from&to=$to&message_type=$messageType&page=$page&size=$size',
      method: Dios.GET,
      onSuccess: onSuccess,
      fromJson: newsListFromJson);
}
