import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:crypto/crypto.dart';
import 'package:dio/dio.dart';
import 'package:huasheng_front_flutter/models/responseModel.dart';
import 'package:huasheng_front_flutter/pages/widgets/Loading.dart';
import 'package:huasheng_front_flutter/pages/widgets/public_ui.dart';
import 'package:http_parser/http_parser.dart';

/*
 * 封装 restful 请求
 *
 * GET、POST、DELETE、PATCH
 * 主要作用为统一处理相关事务：
 *  - 统一处理请求前缀；
 *  - 统一打印请求信息；
 *  - 统一打印响应信息；
 *  - 统一打印报错信息；
 */
class Dios {
  /// global dio object
  static Dio dio;

  /// default options
  static const String API_PREFIX = 'http://192.168.120.245:7001';
  // static const String API_PREFIX = 'http://192.168.101.108:7001';

  static const int CONNECT_TIMEOUT = 10000;
  static const int RECEIVE_TIMEOUT = 3000;

  /// http request methods
  static const String GET = 'get';
  static const String POST = 'post';
  static const String PUT = 'put';
  static const String PATCH = 'patch';
  static const String DELETE = 'delete';

  ///Get请求测试
  static void get() async {
    try {
      Response response = await dio.get("http://www.google.cn");
      print("response$response");
    } catch (e) {
      print(e);
    }
  }

  ///Post请求测试
  static void postHttp<T>(
    String url, {
    parameters,
    Function(T t) onSuccess,
    Function(String error) onError,
  }) async {
    ///定义请求参数
    parameters = parameters ?? {};
    //参数处理
    parameters.forEach((key, value) {
      if (url.indexOf(key) != -1) {
        url = url.replaceAll(':$key', value.toString());
      }
    });

    try {
      Response response;
      Dio dio = createInstance();
      response = await dio.post(url, data: parameters);
      if (response.statusCode == 200) {
        if (onSuccess != null) {
          onSuccess(response.data);
        }
      } else {
        throw Exception('statusCode:${response.statusCode}');
      }
      print('响应数据：' + response.toString());
    } catch (e) {
      print('请求出错：' + e.toString());
      onError(e.toString());
    }
  }

  /// request method
  //url 请求链接
  //parameters 请求参数
  //metthod 请求方式
  //onSuccess 成功回调
  //onError 失败回调
  static request<T>(String url,
      {parameters,
      String method,
      Function(T t) onSuccess,
      Function(ResponseModel error) onError,
      bool isShowLoading = false,
      dynamic fromJson}) async {
    parameters = parameters ?? {};
    method = method ?? 'GET';

    /// 请求处理
    parameters.forEach((key, value) {
      if (url.indexOf(key) != -1) {
        url = url.replaceAll(':$key', value.toString());
      }
    });

    /// 打印:请求地址-请求方式-请求参数
    print('请求地址：【' + method + '  ' + url + '】');
    print('请求参数：' + parameters.toString());

    Dio dio = createInstance();
    if (isShowLoading) {
      Loading.before(url);
    }
    await new Future.delayed(new Duration(milliseconds: 1000));
    //请求结果
    var result;
    try {
      Response response = await dio.request(url,
          data: parameters, options: Options(method: method));
      if (isShowLoading) {
        Loading.complete(url);
      }
      result = response.data;
      if (response.statusCode == 200) {
        print('✔成功响应数据：' + result.toString());
        ResponseModel res = ResponseModel.fromJson(result);
        if (res.statusCode == 200) {
          result = res.data;
          if (fromJson != null) {
            result = fromJson(json.encode(result));
          }
          if (onSuccess != null) {
            onSuccess(result);
          }
        } else {
          toast('❌' + res.message + ' (${res.statusCode})');
        }
      } else {
        print('❌失败响应数据：' + response.toString());
        toast('请求失败：$url lstatusCode:${response.statusCode}');
        throw Exception('statusCode:${response.statusCode}');
      }
    } on DioError catch (e) {
      // print('请求出错：' + e.toString());
      toast('❌请求出错：' + e.toString());
      if (isShowLoading) {
        Loading.complete(url);
      }
      // onError(e.toString());
    } finally {}

    return result;
  }

  /// 创建 dio 实例对象
  static Dio createInstance() {
    if (dio == null) {
      BaseOptions options = BaseOptions(
        /// 连接服务器超时时间，单位是毫秒.
        connectTimeout: 15000,

        /// 2.x中为接收数据的最长时限.
        receiveTimeout: 15000,

        /// 表示期望以那种格式(方式)接受响应数据。
        responseType: ResponseType.json,

        validateStatus: (status) {
          // 不使用http状态码判断状态，使用AdapterInterceptor来处理（适用于标准REST风格）
          return true;
        },
        baseUrl: API_PREFIX,
      );

      dio = new Dio(options);
      dio.interceptors
          .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
        // 在请求被发送之前做一些事情
        return options; //continue
        // 如果你想完成请求并返回一些自定义数据，可以返回一个`Response`对象或返回`dio.resolve(data)`。
        // 这样请求将会被终止，上层then会被调用，then中返回的数据将是你的自定义数据data.
        //
        // 如果你想终止请求并触发一个错误,你可以返回一个`DioError`对象，或返回`dio.reject(errMsg)`，
        // 这样请求将被中止并触发异常，上层catchError会被调用。
      }, onResponse: (Response response) async {
        // 在返回响应数据之前做一些预处理
        return response; // continue
      }, onError: (DioError e) async {
        // 当请求失败时做一些预处理
        return e; //continue
      }));
    }

    return dio;
  }

  /// 清空 dio 对象
  static clear() {
    dio = null;
  }

  // 上传dio
  static Dio _uploadImagesdio = new Dio(BaseOptions(
    /// 连接服务器超时时间，单位是毫秒.
    connectTimeout: 15000,

    /// 2.x中为接收数据的最长时限.
    receiveTimeout: 15000,

    /// 表示期望以那种格式(方式)接受响应数据。
    responseType: ResponseType.plain,
  ));
  //上传图片
  static uploadImage(File file,
      {Function success, Function error, String images = ""}) async {
    //验证文本域 这里设置是过期时间
    String policyText =
        '{"expiration": "2090-01-01T12:00:00.000Z","conditions": [["content-length-range", 0, 1048576000]]}';

    //进行utf8编码
    List<int> policyTextUtf8 = utf8.encode(policyText);

    //进行base64编码
    String policyBase64 = base64.encode(policyTextUtf8);

    //再次进行utf8编码
    List<int> policy = utf8.encode(policyBase64);

    String accesskey = 'addwoURwSZUGIVDBDgdv3JTAxFS6Sl'; //  你自己的accesskeysecret

    //进行utf8 编码
    List<int> key = utf8.encode(accesskey);

    //通过hmac,使用sha1进行加密
    List<int> signaturePre = new Hmac(sha1, key).convert(policy).bytes;

    //最后一步，将上述所得进行base64 编码
    String signature = base64.encode(signaturePre);

    //上传到文件名
    String fileName =
        file.path.substring(file.path.lastIndexOf("/") + 1, file.path.length);
    //文件后缀
    String suffix =
        fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length);
    //创建一个formdata，作为dio的参数
    FormData data = new FormData.fromMap({
      'Filename': fileName, //  这里随便写的
      'key': "$images" + fileName, //  这里才是上传的名称
      'policy': policyBase64,
      'OSSAccessKeyId': "LTAI4GFi9a1PnRifzzGWGKeo", // 你自己的id
      'success_action_status': '200', //让服务端返回200，不然，默认会返回204
      'signature': signature,
      'file': await MultipartFile.fromFile(file.path,
          filename: "imageFileName", contentType: MediaType("image", suffix)),
      // 通过FormData上传多个文件:
      // "files": [
      //   await MultipartFile.fromFile("./text1.txt", filename: "text1.txt"),
      //   await MultipartFile.fromFile("./text2.txt", filename: "text2.txt"),
      // ]
    });
    const url = "https://towangsheng.oss-accelerate.aliyuncs.com";
    // const url = "https://laoziaini.vip";
    // 阿里云图片裁剪功能
    // ?x-oss-process=image/resize,m_fixed,h_800,w_800
    try {
      Response response = await _uploadImagesdio.post(url, data: data);
      // print(response);
      if (response.statusCode == 200) {
        String result = "$url/$images" + fileName;
        print(result);
        if (success != null) success(result);
      } else {
        if (error != null) error();
        toast('❌上传图片失败');
      }
    } on DioError catch (e) {
      print(e.message);
      print(e.response.data);
      print(e.response.headers);
      print(e.response.request);
      print(accesskey);
    }
  }

  /// @{quality} 质量
  /// @{name} 图片名称
  static savenNetworkImage(url, {int quality: 80, String name}) async {
    var response = await Dio()
        .get(url, options: Options(responseType: ResponseType.bytes));
    Map result = await ImageGallerySaver.saveImage(
        Uint8List.fromList(response.data),
        quality: quality,
        name: name);
    print(result['filePath']);
    if (result['isSuccess']) {
      toast('保存成功');
    } else {
      toast('保存失败');
    }
  }
}
