/* 生产环境 */
// final kURLServer1 = '';
// final kURLServer2 = '';
/* 测试环境 */
final kURLServer1 = 'http://172.16.51.209:25500/huasheng';
final kURLServer2 = 'http://172.16.51.222:8000/api/v1';
final kURLServer3 = 'http://172.16.51.222:9090/microchat/common';
final kURLServer4 = 'http://172.16.51.209:22500/huasheng';

/// 接口HTTP方法
enum APIMethod {
  /// GET方法
  get,

  /// POST方法
  post,

  /// PUT方法
  put
}

/* 接口响应固定字段 */
final kAPIResultStatus = 'status';
final kAPIResultMessage = 'message';

/* 接口响应成功固定status */
final kAPIResultStatusSucceeded = '00000';
/* 接口响应失败默认message */
final kAPIResultStatusMessageFailed = '请求失败，请稍后重试';

/* 具体接口URL */
final kAPIURLRegister = kURLServer1 + '/register';
final kAPIURLVerificationCode = kURLServer1 + '/user/vcode';
final kAPIURLResetPassword = kURLServer1 + '/user/passwd';
final kAPIURLLoginByPassword = kURLServer1 + '/login/passwd';
final kAPIURLLoginByVerificationCode = kURLServer1 + '/login/vcode';
final kAPIURLSaveUserInfo = kURLServer1 + '/user/info';
final kAPIURLVerifyUserInfoCompletion = kURLServer1 + '/user/scanner';
final kAPIURLVerifyVerificationCode = kURLServer1 + '/user/vcode/scanner';
final kAPIURLVerifyNickname = kURLServer1 + '/user/nickname/scanner';
final kAPIURLIndustry = kURLServer1 + '/industry';
final kAPIURLProvinceCityList = kURLServer1 + '/province/city/list';
final kAPIURLApplyInvitationCode = kURLServer1 + '/user/invcode';
final kAPIURLVerifyInvitationCode = kURLServer1 + '/user/invcode/scanner';

final kAPIURLUserDynamics = kURLServer2 + '/user/dynamics';
final kAPIPathUserList = kURLServer4 + '/user/list';

final kAPIURLUploadFile = kURLServer3 + '/upload';
