import 'package:huasheng_front_flutter/http/dio.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

class NewsIo {
  static IO.Socket io;
  static Function(String event, dynamic Function(dynamic) handler) on;
  static Function(String event, dynamic data, {Function ack, bool binary})
      emitWithAck;
  static init({Map<String, dynamic> query}) {
    // 构建请求头，可以放一些cookie等信息，这里加上了origin，因为服务端有origin校验
    Map<String, dynamic> headers = new Map();
    headers['origin'] = Dios.API_PREFIX;
    // 建立websocket链接
    // 链接的书写规范，schame://host:port/namespace, 这里socket_io_client在处理链接时候会把path和后面的query参数都作为namespace来处理，所以如果我们的namespace是/的话，就直接使用http://host/
    io = IO.io(Dios.API_PREFIX + '/news', <String, dynamic>{
      // 请求的path
      // 'path': 'io',
      // 构造的header放这里
      'extraHeaders': headers,
      // 查询参数，扔这里
      'query': query,
      // 说明需要升级成websocket链接
      'transports': ['websocket'],
    });
    on = io.on;
    emitWithAck = io.emitWithAck;
  }

  /// 发送消息给好友
  static sendFriend(dynamic data, {Function(int isSuccess) callback}) {
    io.emitWithAck('sendFriend', data, ack: callback);
  }

  /// 链接建立成功之后，可以发送数据到socket.io的后端了
  static onConnect() {
    io.on('connect', (_) {
      print('connect');
      // 发送消息和回调函数给socket.io服务端，在服务端可以直接获取到该方法，然后调用
      io.emitWithAck('exchange', '11111', ack: (data) {
        print('ack $data');
        if (data != null) {
          print('from server $data');
        } else {
          print("Null");
        }
      });
    });
  }

  /// 链接建立失败时调用
  static onError() {
    io.on('error', (data) {
      print('error');
      print(data);
    });
  }

  /// 链接出错时调用
  static onConnectError() {
    io.on("connect_error", (data) => print('connect_error: '));
  }

  /// 链接断开时调用
  static onDisconnect() {
    io.on('disconnect', (_) => print('disconnect======'));
  }

  /// 链接关闭时调用
  static onClose() {
    io.on('close', (_) => print('close===='));
  }
}
