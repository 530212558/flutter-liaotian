import 'package:huasheng_front_flutter/http/dio.dart';
import 'package:huasheng_front_flutter/models/Industry.dart';
import 'package:huasheng_front_flutter/models/chinaArea.dart';

const prefix = "/public";

requestIndustry({Function(List<Industry> t) onSuccess}) {
  Dios.request(prefix + '/industry',
      method: Dios.GET, onSuccess: onSuccess, fromJson: industryFromJson);
}

requestChinaArea({Function(List<ChinaArea> t) onSuccess}) {
  Dios.request(prefix + '/chinaArea',
      method: Dios.GET, onSuccess: onSuccess, fromJson: chinaAreaFromJson);
}
