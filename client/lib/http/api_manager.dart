import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:huasheng_front_flutter/http/account_manager.dart';
import 'package:huasheng_front_flutter/http/api_defines.dart';
import 'package:huasheng_front_flutter/models/response_model.dart';
import 'package:huasheng_front_flutter/pages/widgets/public_ui.dart';

import 'package:path/path.dart';

class APIManager {
  static final _singleton = APIManager._internal();

  factory APIManager() {
    return _singleton;
  }

  APIManager._internal();

  final _dio = Dio(
    BaseOptions(
      connectTimeout: 15000,
      receiveTimeout: 15000,
    ),
  );

  /// GET请求
  Future<ResponseModel> get(String url, Map<String, dynamic> params) async {
    return request(APIMethod.get, url, params);
  }

  /// POST请求
  Future<ResponseModel> post(String url, Map<String, dynamic> params) async {
    return request(APIMethod.post, url, params);
  }

  /// POST请求  参数是 FormData 类型
  Future<ResponseModel> postFormData(
      String url, Map<String, dynamic> params) async {
    return request(APIMethod.post, url, params, isFormData: true);
  }

  /// PUT请求
  Future<ResponseModel> put(String url, Map<String, dynamic> params) async {
    return request(APIMethod.put, url, params);
  }

  /// 通用请求
  Future<ResponseModel> request(
      APIMethod method, String url, Map<String, dynamic> params,
      {bool isFormData = false}) async {
    final params2 = await _globalParams(params);
    final json = jsonEncode(params2);
    if (method == APIMethod.post) {
      print('POST $url $json');
    } else if (method == APIMethod.put) {
      print('PUT $url $json');
    } else {
      print('GET $url $json');
    }
    final json2 =
        await rawRequest(method, url, params2, isFormData: isFormData);
    final response = ResponseModel.fromJson(json2);
    return response;
  }

  /// 原始请求
  Future<Map<String, dynamic>> rawRequest(
      APIMethod method, String url, Map<String, dynamic> params,
      {bool isFormData = false}) async {
    Response response;
    try {
      if (method == APIMethod.post) {
        if (isFormData) {
          FormData formData = FormData.fromMap(params);
          response = await _dio.post(url, data: formData);
        } else {
          response = await _dio.post(url, data: params);
        }
      } else if (method == APIMethod.put) {
        response = await _dio.put(url, data: params);
      } else {
        response = await _dio.get(url, queryParameters: params);
      }

      final json = jsonEncode(response.data);
      if (json == null) {
        print(response.data);
      } else {
        print(json);
      }

      if (response.statusCode == HttpStatus.ok) {
        if (response.data is Map<String, dynamic>) {
          return response.data;
        } else {
          return null;
        }
      } else {
        print('http status ${response.statusCode}');
        return null;
      }
    } catch (error, stacktrace) {
      print('Exception occured: $error stackTrace: $stacktrace');
      if (error is DioError) {
        response = error.response;
      }
    }

    if (response == null) {
      return null;
    } else {
      if (response.data is Map<String, dynamic>) {
        return response.data;
      } else {
        try {
          final json = jsonDecode(response.data);
          if (json != null && json is Map<String, dynamic>) {
            return json;
          }
        } catch (error, stacktrace) {
          print('Exception occured: $error stackTrace: $stacktrace');
        }

        return null;
      }
    }
  }

  /// 上传文件请求
  Future<ResponseModel> uploadFile(String url, List<String> filePaths,
      {Map params}) async {
    print('POST $url');
    final json2 = await rawUploadFile(url, filePaths, params);
    final response = ResponseModel.fromJson(json2);
    return response;
  }

  /// 上传文件原始请求
  Future<Map<String, dynamic>> rawUploadFile(
      String url, List<String> filePaths, Map<String, dynamic> params) async {
    Response response;
    try {
      final params2 = await _globalParams(params);

      List<MultipartFile> files = [];
      for (var i = 0; i < filePaths.length; i++) {
        final fileExtension = extension(filePaths[i]);
        final file = await MultipartFile.fromFile(filePaths[i],
            filename: '$i$fileExtension');
        files.add(file);
      }
      params2['files'] = files;
      final formData = FormData.fromMap(params2);
      response = await _dio.post(url, data: formData);

      final json = jsonEncode(response.data);
      if (json == null) {
        print(response.data);
      } else {
        print(json);
      }

      if (response.statusCode == HttpStatus.ok) {
        if (response.data is Map<String, dynamic>) {
          return response.data;
        } else {
          return null;
        }
      } else {
        print('http status ${response.statusCode}');
        return null;
      }
    } catch (error, stacktrace) {
      print('Exception occured: $error stackTrace: $stacktrace');
      if (error is DioError) {
        response = error.response;
      }
    }

    if (response == null) {
      return null;
    } else {
      if (response.data is Map<String, dynamic>) {
        return response.data;
      } else {
        try {
          final json = jsonDecode(response.data);
          if (json != null && json is Map<String, dynamic>) {
            return json;
          }
        } catch (error, stacktrace) {
          print('Exception occured: $error stackTrace: $stacktrace');
        }

        return null;
      }
    }
  }

  /// 公共请求字段
  Future<Map<String, dynamic>> _globalParams(
      Map<String, dynamic> params) async {
    Map<String, dynamic> params2 = {};
    if (AccountManager().hasLoggedIn()) {
      final loginInfo = AccountManager().loginInfo;
      params2['openid'] = loginInfo == null ? '' : (loginInfo.openID ?? '');
      params2['resourcetoken'] =
          loginInfo == null ? '' : (loginInfo.resourceToken ?? '');
    }

    if (params != null) {
      params.forEach((k, v) {
        params2[k] = v;
      });
    }

    return params2;
  }

  /// 通用错误提示
  toastAPIError(ResponseModel response) {
    if (response.message != null && response.message.length > 0) {
      toast(response.message);
    } else {
      toast(kAPIResultStatusMessageFailed);
    }
  }
}
