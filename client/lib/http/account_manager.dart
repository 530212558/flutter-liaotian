import 'dart:convert';

import 'package:huasheng_front_flutter/models/login_info_model.dart';
// import 'package:huasheng_front_flutter/models/user_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AccountManager {
  static final _singleton = AccountManager._internal();
  LoginInfo loginInfo;
  // User user;

  factory AccountManager() {
    return _singleton;
  }

  AccountManager._internal() {
    // loadLoginInfo();
    // loadUser();
  }

  /// 判断是否已登录
  bool hasLoggedIn() {
    return loginInfo != null &&
        loginInfo.openID != null &&
        loginInfo.openID.length > 0;
  }

  /// 加载LoginInfo
  Future loadLoginInfo() async {
    try {
      final prefs = await SharedPreferences.getInstance();
      final s = prefs.getString('loginInfo');
      if (s != null) {
        final json = jsonDecode(s);
        loginInfo = LoginInfo.fromJson2(json);
      }
    } catch (error, stacktrace) {
      print('Exception occured: $error stackTrace: $stacktrace');
    }
  }

  /// 存储LoginInfo
  Future storeLoginInfo() async {
    try {
      final prefs = await SharedPreferences.getInstance();
      if (loginInfo != null) {
        final json = loginInfo.toJson();
        final s = jsonEncode(json);
        prefs.setString('loginInfo', s);
      } else {
        prefs.setString('loginInfo', null);
      }
    } catch (error, stacktrace) {
      print('Exception occured: $error stackTrace: $stacktrace');
    }
  }

  /// 清空LoginInfo
  clearLoginInfo() {
    loginInfo = null;
    storeLoginInfo();
  }

  // Future loadUser() async {
  //   try {
  //     final prefs = await SharedPreferences.getInstance();
  //     final s = prefs.getString('user');
  //     if (s != null) {
  //       final json = jsonDecode(s);
  //       user = User.fromJson2(json);
  //     } else {
  //       user = User();
  //     }
  //   } catch (error, stacktrace) {
  //     print('Exception occured: $error stackTrace: $stacktrace');
  //     user = User();
  //   }
  // }

  // Future saveUser() async {
  //   try {
  //     final prefs = await SharedPreferences.getInstance();
  //     if (user != null) {
  //       final json = user.toJson();
  //       final s = jsonEncode(json);
  //       prefs.setString('user', s);
  //     } else {
  //       prefs.setString('user', null);
  //     }
  //   } catch (error, stacktrace) {
  //     print('Exception occured: $error stackTrace: $stacktrace');
  //   }
  // }

  // clearUser() {
  //   user = User();
  //   saveUser();
  // }
}
