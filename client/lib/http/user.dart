import 'package:huasheng_front_flutter/http/dio.dart';
import 'package:huasheng_front_flutter/models/Album.dart';
import 'package:huasheng_front_flutter/models/userInfo.dart';
import 'package:huasheng_front_flutter/models/user_list_model.dart';

const prefix = "/user";

requestLogin(Map<String, String> data, {Function(UserInfo t) onSuccess}) {
  Dios.request(prefix + '/login',
      parameters: data,
      method: Dios.POST,
      onSuccess: onSuccess,
      isShowLoading: true,
      fromJson: userInfoFromJson);
}

requesRegister(Map<String, String> data, {Function(dynamic t) onSuccess}) {
  Dios.request(prefix + '/register',
      method: Dios.POST,
      parameters: data,
      isShowLoading: true,
      onSuccess: onSuccess);
}

requesGetInfo(int userId, {Function(Info t) onSuccess}) {
  Dios.request(prefix + '/getInfo?user_id=$userId',
      method: Dios.GET, onSuccess: onSuccess, fromJson: infoFromJson);
}

requesInfo(Map<String, dynamic> data, {Function(int t) onSuccess}) {
  Dios.request(prefix + '/info',
      method: Dios.POST,
      parameters: data,
      onSuccess: onSuccess,
      isShowLoading: true);
}

requesGetUsers(
    {Function(List<UserList> t) onSuccess,
    double longitude,
    double latitude,
    int gender}) {
  Dios.request(
      prefix +
          '/getUsers?longitude=$longitude&latitude=$latitude&gender=$gender',
      method: Dios.GET,
      onSuccess: onSuccess,
      fromJson: userListFromJson);
}

requesGetAlbum(
  int userId,
  String purpose, {
  Function(List<Album> t) onSuccess,
}) {
  Dios.request(
    prefix + '/getAlbum?user_id=$userId&purpose=$purpose',
    method: Dios.GET,
    onSuccess: onSuccess,
    fromJson: albumFromJson,
  );
}

requesSaveAlbum(
  List<dynamic> data, {
  Function onSuccess,
}) {
  Dios.request(prefix + '/saveAlbum',
      isShowLoading: true,
      parameters: {'album': data},
      method: Dios.POST,
      onSuccess: onSuccess);
}
