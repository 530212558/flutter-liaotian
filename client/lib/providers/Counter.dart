import 'package:flutter/material.dart';

class Counter with ChangeNotifier {
  int _count = 100;
  ThemeData _themeData = ThemeData.light();

  get count => _count;
  get themeData => _themeData;

  increment() {
    _count++;
    notifyListeners(); //  通知听众
  }

  addCount() {
    _count++;
    notifyListeners();
  }
}
