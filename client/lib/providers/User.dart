import 'package:flutter/material.dart';

class User with ChangeNotifier {
  bool _isLogin = false;
  String nickname = 'test';
  get isLogin => _isLogin;

  update() {
    this.nickname = 'will';
  }

  login() {
    _isLogin = true;
    notifyListeners();
  }

  logout() {
    _isLogin = false;
    notifyListeners();
  }
}
