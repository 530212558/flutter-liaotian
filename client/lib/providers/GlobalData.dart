import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/models/Industry.dart';
import 'package:huasheng_front_flutter/models/chinaArea.dart';

class GlobalData with ChangeNotifier {
  static List<Industry> _data;
  static List<Industry> get data => _data;

  static setIndustry(List<Industry> industry) {
    _data = industry;

    // notifyListeners();
  }

  static List<ChinaArea> chinaAreaList;

  static setChinaArea(List<ChinaArea> chinaArea) {
    chinaAreaList = chinaArea;

    // notifyListeners();
  }
}
