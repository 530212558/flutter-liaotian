import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/models/userInfo.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserInfoProvider with ChangeNotifier {
  UserInfo _userInfo = UserInfo();
  UserInfo get userInfo => _userInfo;

  setUserInfo(UserInfo userInfo) {
    this._userInfo = userInfo;
    // SharedPreferences.getInstance()
    //     .then((value) => {value.setString("userInfo", json.encode(userInfo))});

    notifyListeners();
  }

  logout() {
    this._userInfo = UserInfo();
    SharedPreferences.getInstance().then((value) {
      value.remove("userInfo");
      notifyListeners();
    });
    // _isLogin = false;
  }

  // login() {
  //   // _isLogin = true;
  //   notifyListeners();
  // }

}
