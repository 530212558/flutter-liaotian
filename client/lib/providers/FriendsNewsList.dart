import 'package:flutter/material.dart';
import 'package:huasheng_front_flutter/models/FriendsNewsList.dart';

class FriendsNewsListProvider with ChangeNotifier {
  List<FriendsNewsList> _friendsNewsList = [];
  List<FriendsNewsList> get getFriendsNewsList => _friendsNewsList;

  setFriendsNewsList(List<FriendsNewsList> friendsNewsList) {
    this._friendsNewsList = friendsNewsList;
    notifyListeners();
  }
}
