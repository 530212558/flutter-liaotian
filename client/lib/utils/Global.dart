import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/widgets.dart';
import 'package:huasheng_front_flutter/pages/widgets/ShowDialog.dart';
import 'package:huasheng_front_flutter/utils/authorityCheck.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:path_provider/path_provider.dart';

class Global {
  // App启动时，异步初始化全局信息
  static Future init() async {
    WidgetsFlutterBinding.ensureInitialized();
  }
}

class UnitModel {
  double number;
  String unit;

  /// JSON转换成对象，接口响应时用
  UnitModel.fromJson(Map<String, dynamic> json)
      : number = json != null ? json['number'] : '',
        unit = json != null ? json['unit'] : '';
}

/**
 * @{number} 价格
 * @{toStringAsFixed} 保留的小数位
 */
UnitModel convertingMoney(double number, {int toStringAsFixed = 2}) {
  String unit = '元';
  if (number >= 100000000) {
    number = number / 100000000;
    unit = '亿';
  } else if (number >= 10000000) {
    number = number / 10000000;
    unit = '千万';
  } else if (number >= 1000000) {
    number = number / 1000000;
    unit = '百万';
  } else if (number >= 10000) {
    number = number / 10000;
    unit = '万';
  }
  number = double.parse(number.toStringAsFixed(toStringAsFixed));
  UnitModel result = UnitModel.fromJson({"number": number, "unit": unit});
  return result;
}

/**
 * @{number} 单位（m）
 * @{toStringAsFixed} 保留的小数位
 */
UnitModel conversionDistance(double number, {int toStringAsFixed = 2}) {
  String unit = 'M';
  if (number >= 1000) {
    number = number / 1000;
    unit = 'KM';
  }
  number = double.parse(number.toStringAsFixed(toStringAsFixed));
  UnitModel result = UnitModel.fromJson({"number": number, "unit": unit});
  return result;
}

/**
 * @{resultList} 获取的图片数据
 * return 返回本地图片路劲
 */
_imagesList(List<Asset> resultList) async {
  List<File> files = [];
  for (int i = 0; i < resultList.length; i++) {
    String name = resultList[i].name;
    //请求原始图片数据
    ByteData item = await resultList[i].getByteData();
    //获取图片数据，并转换成uint8List类型
    Uint8List imageData = item.buffer.asUint8List();

    //获得应用临时目录路径
    final Directory _directory = await getTemporaryDirectory();
    final Directory _imageDirectory =
        await new Directory('${_directory.path}/image/')
            .create(recursive: true);
    var path = _imageDirectory.path;
    File imageFile = new File('$path$name')..writeAsBytesSync(imageData);

    //  添加路径
    File file = new File(imageFile.path);
    files.add(file);
  }
  return files;
  // saveDynamicsParams['files'] = files;
  // saveDynamicsParams['files'] = await toMultipartFile(files);
}

/**
  * @{maxImages} 选择图片的最大数量（默认99）
  * return 返回本地图片路劲 
*/
getImages(BuildContext context, {int maxImages = 99}) async {
  // final pickedFile = await picker.getImage(source: ImageSource.camera);
  bool result = await Permissions.getCamera();
  List<File> files = [];
  if (result) {
    try {
      List<Asset> resultList = await MultiImagePicker.pickImages(
        // 选择图片的最大数量
        maxImages: maxImages,
        // 是否支持拍照
        enableCamera: true,
        materialOptions: MaterialOptions(
            // 显示所有照片，值为 false 时显示相册
            startInAllView: true,
            allViewTitle: '所有照片',
            actionBarColor: '#2196F3',
            textOnNothingSelected: '没有选择照片'),
      );
      files = await _imagesList(resultList);
    } on Exception catch (e) {
      e.toString();
    }
  } else {
    alertDialog(
      context: context,
      onCancel: () {
        print('onCancel');
        Navigator.of(context).pop();
        // widget.callBack();
      },
      onConfirm: () {
        print('onConfirm');
        Permissions.openAppSetting();
        Navigator.of(context).pop();
        // widget.callBack();
      },
      title: Text('相册权限被拒绝'),
      content: Text(('是否需要跳转到打开相册权限？')),
    );
  }
  return files;
}
