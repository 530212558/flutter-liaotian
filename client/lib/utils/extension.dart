import 'package:huasheng_front_flutter/utils/Global.dart';

import './size_fit.dart';

//  1.对 Int 类型扩展
extension IntFit on int {
  double get px {
    return HYSizeFit.setPx(this.toDouble());
  }

  double get rpx {
    return HYSizeFit.setRpx(this.toDouble());
  }
}

//  2.对 Double 类型扩展
extension DoubleFit on double {
  double get px {
    return HYSizeFit.setPx(this);
  }

  double get rpx {
    return HYSizeFit.setRpx(this);
  }

  double get getMoney {
    return convertingMoney(this).number;
  }

  String get getMoneyUnit {
    return convertingMoney(this).unit;
  }

  double get getDistance {
    return conversionDistance(this).number;
  }

  String get getDistanceUnit {
    return conversionDistance(this).unit;
  }
}

//  3.对 String 类型扩展
// extension StringFit on String {
//   double get gMoney {
//     return convertingMoney(this).number;
//   }
// }

// import './extension.dart';

// print(200.px); // 在不同屏幕下200px是不同的值
// print(400.rpx); // 在不同屏幕下400rpx是不同的值
