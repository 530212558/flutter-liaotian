import 'package:permission_handler/permission_handler.dart';

class Permissions {
  // Permissions() {
  //   [Permission.camera, Permission.location].request();
  // }
  static getCamera() async {
    // print('getCamera');
    bool result = true;
    await Permission.camera.request();
    if (await Permission.camera.isGranted) {
      print("用户相机同意了");
    } else if (await Permission.camera.isDenied) {
      print("用户相机拒绝了");
      result = false;
      // openAppSettings(); //  打开应用设置权限
    } else if (await Permission.camera.isPermanentlyDenied) {
      print("用户相机永久拒绝");
      result = false;
      // openAppSettings(); //  打开应用设置权限
    }
    return result;
  }

  static getLocation() async {
    await Permission.location.request();
    bool result = true;
    if (await Permission.location.isGranted) {
      print("用户定位同意了");
    } else if (await Permission.location.isDenied) {
      print("用户定位拒绝了");
      result = false;
      // openAppSettings(); //  打开应用设置权限
    } else if (await Permission.location.isPermanentlyDenied) {
      print("用户定位永久拒绝");
      result = false;
      // openAppSettings(); //  打开应用设置权限
    }
    return result;
  }

  static getStorage() async {
    // 检查是否已有读写内存的权限
    await Permission.storage.request();
    bool status = await Permission.storage.isGranted;

    //判断如果还没拥有读写权限就申请获取权限
    if (!status) {
      // 直接Permission.storage.request().isGranted的结果就是是否授权的结果。
      return await Permission.storage.request().isGranted;
    }
  }

  static openAppSetting() {
    openAppSettings(); //  打开应用设置权限
  }
}
