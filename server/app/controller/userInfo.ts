import { Controller } from 'egg';

export default class UserInfoController extends Controller {
  public async getAlbum() {
    const { ctx } = this;
    const { user_id, purpose } = ctx.request.query;
    const result = await ctx.service.userInfo.getAlbum(user_id, purpose);
    return (ctx.body = ctx.helper.success(result));
  }

  public async saveAlbum() {
    const { ctx } = this;
    const { album } = ctx.request.body;
    const result = await ctx.service.userInfo.saveAlbum(album);
    return (ctx.body = ctx.helper.success(result));
  }
}
