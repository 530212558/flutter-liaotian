import { Controller } from 'egg';

export default class NewsController extends Controller {
  async getFriendsNewsList() {
    const { ctx } = this;
    const { user_id } = ctx.request.query;
    const result = await ctx.service.news.friendsNewsList(Number(user_id));
    return (ctx.body = ctx.helper.success(result));
  }
  async getNewsList() {
    const { ctx } = this;
    const { from, to, message_type, page, size }: any = ctx.request.query;
    const result = await ctx.service.news.newsList(
      from,
      to,
      message_type,
      page,
      size,
    );
    return (ctx.body = ctx.helper.success(result));
  }
}
