import { Controller } from 'egg';
// import { UserInfo } from '../service/public';

export default class UserController extends Controller {
  public async getIndustry() {
    const { ctx } = this;
    let result = await ctx.model.Industry.find({});
    result = filterIndustry(result);
    ctx.body = ctx.helper.success(result);
    return ctx.body;
  }
  public async getChinaArea() {
    const { ctx } = this;
    // 其中 1 为升序排列，而 -1 是用于降序排列。
    const result = await ctx.model.ChinaArea.find({}).sort({ province: 1 });
    ctx.body = ctx.helper.success(result);
    return ctx.body;
  }
}

function filterIndustry(data: any) {
  const result = data.map((item: any) => {
    let subLevelModelList: any = [];
    if (item.subLevelModelList) {
      item.subLevelModelList.forEach((item2: any) => {
        subLevelModelList.push({
          code: item2.code,
          name: item2.name,
          subLevelModelList: item2.subLevelModelList
            ? filterIndustry(item2.subLevelModelList)
            : null,
        });
      });
    } else {
      subLevelModelList = null;
    }
    return {
      code: item.code,
      name: item.name,
      subLevelModelList,
    };
  });

  return result;
}
