import { Controller } from 'egg';
import { UserInfo } from '../service/user';

export default class UserController extends Controller {
  public async index() {
    const { ctx } = this;
    ctx.body = await ctx.service.test.sayHi('egg');
    // ctx.body = app.mysql.get('flutter');
  }
  public async register() {
    const { ctx } = this;
    // ctx.body = await ctx.service.test.sayHi('egg');
    // const results = await app.mysql.select('user');
    const { phone, password } = ctx.request.body;
    const result = await ctx.service.user.register({ phone, password });
    if (!result) return (ctx.body = ctx.helper.paramError('1001'));
    return (ctx.body = ctx.helper.success(result));
  }
  public async login() {
    const { ctx } = this;
    const { phone, password } = ctx.request.body;
    // console.log({ phone, password })
    const result = await ctx.service.user.login({ phone, password });
    if (result) {
      const info = (await ctx.service.user.getInfo(result.id)) || null;
      const token = ctx.app.jwt.sign(
        {
          ...ctx.request.body,
        },
        this.app.config.jwt.secret,
        {
          expiresIn: '7 days', // 七天 时间根据自己定，具体可参考 jsonwebtoken 插件官方说明
        },
      );
      ctx.body = ctx.helper.success({ ...result, info, token });
    } else {
      ctx.body = ctx.helper.paramError('账号或者密码错误');
    }
    return ctx.body;
  }

  public async getInfo() {
    const { ctx } = this;
    const user_id = ctx.query.user_id;
    ctx.body = ctx.helper.success(
      (await ctx.service.user.getInfo(user_id)) || null,
    );
    return ctx.body;
  }
  public async saveInfo() {
    const { ctx } = this;
    // ctx.body = await ctx.service.test.sayHi('egg');
    // const results = await app.mysql.select('user');
    const {
      user_id,
      gender,
      nickname,
      avatar,
      birth_year,
      birth_month,
      birth_day,
      hight,
      weight,
      longitude,
      latitude,
      province,
      city,
      district,
      address,
      trade,
      career,
      preference,
      introduction,
    } = ctx.request.body;
    const params: UserInfo = {
      user_id,
      gender,
      nickname,
      avatar,
      birth_year,
      birth_month,
      birth_day,
      hight,
      weight,
      longitude,
      latitude,
      province,
      city,
      district,
      address,
      trade,
      career,
      preference,
      introduction,
    };
    for (const key in params) {
      if (params[key] == null) {
        ctx.body = ctx.helper.paramError(`参数❌：${key} 是必填项`);
        return ctx.body;
      }
    }
    const result = await ctx.service.user.saveInfo(params);
    return (ctx.body = ctx.helper.success(result));
  }

  public async getUsers() {
    const { ctx } = this;
    const {
      // gender, city,
      longitude,
      latitude,
      gender,
    } = ctx.query;
    ctx.body = ctx.helper.success(
      await ctx.service.user.getUsers(longitude, latitude, gender),
    );
    return ctx.body;
  }
}
