// app/router/user.ts
module.exports = app => {
  const { controller } = app;
  const router = app.router.namespace(
    '/news',
    // , app.middleware.jsonp()
  );
  router.get('/getNewsList', controller.news.getNewsList);
  router.get('/getFriendsNewsList', controller.news.getFriendsNewsList);
};
