// app/router/user.ts
module.exports = app => {
  const { controller } = app;
  const router = app.router.namespace(
    '/public',
    // , app.middleware.jsonp()
  );
  //  获取行业分类
  router.get('/industry', controller.public.getIndustry);
  //  获取中国地区
  router.get('/chinaArea', controller.public.getChinaArea);
};
