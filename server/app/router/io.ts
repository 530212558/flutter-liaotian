// {app_root}/app/router.js

module.exports = app => {
  const { io } = app;
  io.of('/news').route('sendFriend', io.controller.news.sendFriend);
};
