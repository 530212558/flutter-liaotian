// app/router/user.ts
module.exports = app => {
  const { controller } = app;
  const router = app.router.namespace(
    '/user',
    // , app.middleware.jsonp()
  );
  router.post('/login', controller.user.login);
  router.post('/register', controller.user.register);

  router.get('/getInfo', controller.user.getInfo);
  router.post('/info', controller.user.saveInfo);

  router.get('/getUsers', controller.user.getUsers);

  router.get('/getAlbum', controller.userInfo.getAlbum);
  router.post('/saveAlbum', controller.userInfo.saveAlbum);
};
