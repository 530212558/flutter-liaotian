import { Controller } from 'egg';
import { ISaveNews } from '../../service/news';

export default class ChatController extends Controller {
  async exchange() {
    const { ctx } = this;
    const message = ctx.args[0];
    if (message) {
      console.log('packet:exchange', message);
    }
    await ctx.socket.emit('res', `Hi! I've got your message: ${message}`);
  }
  async sendFriend() {
    const { ctx } = this;
    const data: ISaveNews = ctx.args[0];
    if (data) {
      // console.log('ctx.args', ctx.args);
      console.log('sendFriend', data);
      // console.log('packet:', ctx.packet);

      const result = await ctx.service.news.insertNews(data);
      if (ctx.args[1]) {
        const insertId = result.insertId;
        ctx.args[1](insertId);
      }
    }
    // await ctx.socket.emit('res', `Hi! I've got your message: ${message}`);
  }
}
