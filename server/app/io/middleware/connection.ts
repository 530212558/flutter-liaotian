// {app_root}/app/io/middleware/connection.js
module.exports = () => {
  return async (ctx, next) => {
    // ctx.socket.emit('res', 'connected!');
    const { socket } = ctx;
    // 获取客服端传递的参数
    const query = socket.handshake.query;
    await next();
    // execute when disconnect.
    console.log('disconnection!', query);
  };
};
