import { Service } from 'egg';
export default class News extends Service {
  public async newsList(
    from: number,
    to: number,
    message_type: number,
    page = 1,
    size = 10,
  ) {
    const { app } = this;
    const result = await app.mysql.select('news', {
      where: {
        from: [from, to],
        to: [from, to],
        message_type,
      },
      orders: [
        // 排序方式  降序 desc，升序 asc
        ['creation_time', 'asc'], // 根据 creation_time 降序
      ],
      limit: size, // 返回数据量
      offset: size * page - size, // 数据偏移量
    });
    return result;
  }

  public async insertNews(data: ISaveNews) {
    const { app } = this;
    const result = await app.mysql.insert('news', data);
    return result;
  }

  public async friendsNewsList(user_id: number) {
    const { app } = this;
    const results = await app.mysql.query(
      `select a.user_id,user_info.nickname,user_info.avatar,
        JSON_OBJECT('id', news.id, 'from', news.from, 'to', news.to, 'message_type', 
        news.message_type, 'content', news.content, 'creation_time', news.creation_time) news
      from (SELECT MAX(id) as id,news.to+news.from-? as 'user_id'
        from news where news.from = ? or news.to = ?
        GROUP BY news.to+news.from
      ) a
      left join news on news.id = a.id
      left join user_info on user_info.user_id = a.user_id`,
      [user_id, user_id, user_id],
    );
    return results;
  }
}

export interface ISaveNews {
  from: number;
  to: number;
  message_type: number;
  content: string;
  creation_time: string;
}
