import { Service } from 'egg';
/**
 * User Service
 */
export default class UserInfo extends Service {
  public async getAlbum(user_id: string, purpose: string) {
    const { app } = this;
    const result = await app.mysql.select('album', {
      where: {
        user_id,
        purpose,
      },
    });
    return result;
  }

  public async saveAlbum(data: ISaveAlbum[]) {
    const { app } = this;
    const result = await app.mysql.insert('album', data);
    return result;
  }
}

export interface ISaveAlbum {
  url: string;
  user_id: string;
  purpose: string;
}
