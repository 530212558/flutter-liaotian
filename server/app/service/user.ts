import { Service } from 'egg';
const moment = require('moment');
/**
 * User Service
 */
const prefix = 'user';
export default class User extends Service {
  /**
   * sayHi to you
   * @param name - your name
   */
  public async register(data: IRegister) {
    const { app } = this;
    const result = await app.mysql.get(
      prefix,
      { phone: data.phone },
      {
        columns: ['id', 'name', 'phone'],
      },
    );
    if (!result) {
      const result = await app.mysql.insert(prefix, { name: '王', ...data });
      return result.insertId;
    }
    return false;
  }

  public async login(data: IRegister) {
    const { app } = this;
    const result = await app.mysql.get(prefix, data, {
      columns: ['id', 'name', 'phone'],
    });
    return result;
  }

  public async getInfo(user_id: number) {
    const { app } = this;
    const result = await app.mysql.get('user_info', { user_id });
    return result;
  }

  public async saveInfo(data: UserInfo) {
    const { app } = this;
    const result = await app.mysql.insert('user_info', {
      ...data,
      update_time: moment(Date.now()).format('YYYY-MM-DD HH:mm:ss'),
    });
    return result.insertId;
  }

  public async getUsers(longitude, latitude, gender, params?) {
    const { app } = this;
    // const longitude = 121.458495;
    // const latitude = 31.166349;
    // const gender = '1';
    //  1：附近 2：新注册，3：女神：4：男神，5：真人
    // params = 1;
    //  默认新注册
    let order = '';
    if (params === 1) {
      order = 'ORDER BY juli DESC';
    } else if (params === 2) {
      order = 'ORDER BY update_time DESC';
    }
    // else if (_params === 3 || _params === 4) {
    //   //
    // }

    const results = await app.mysql.query(
      `SELECT (st_distance (point (b.longitude,b.latitude),point (?,?))*111195 )as distance, 
      a.id,a.name, b.user_id, b.gender, b.nickname, b.avatar, 
      ROUND(DATEDIFF(CURDATE(), concat(b.birth_year,'-',b.birth_month,'-',b.birth_day))/365.2422) as age, 
      b.hight, b.weight, b.longitude, b.latitude, b.province, b.city, b.district, b.address, b.trade, b.career, 
      b.preference, b.introduction, b.update_time, c.album 
      FROM user as a LEFT JOIN user_info as b on a.id = b.user_id 
       LEFT JOIN (
            SELECT 
                user_id, 
                JSON_ARRAYAGG(JSON_OBJECT('id', id, 'url', url,'user_id', user_id,purpose,'purpose')) album
            FROM album 
            GROUP BY user_id
       ) c ON c.user_id = a.id
      WHERE a.id = b.user_id AND b.gender=? ${order}`,
      [longitude, latitude, gender],
    );

    results.forEach(item => {
      item.album = JSON.parse(item.album) || [];
    });
    return results;
  }
}

export interface IRegister {
  // name:string;
  phone: string;
  password: string;
}

export interface UserInfo {
  user_id: number;
  gender: number;
  nickname: string;
  avatar: string;
  birth_year: number;
  birth_month: number;
  birth_day: number;
  hight: number;
  weight: number;
  longitude: number;
  latitude: number;
  province: string;
  city: string;
  district: string;
  address: string;
  trade: string;
  career: string;
  preference: string;
  introduction: string;
}
