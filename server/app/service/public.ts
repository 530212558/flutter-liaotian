import { Service } from 'egg';

/**
 * User Service
 */
const prefix = 'user';
export default class User extends Service {
  /**
   * sayHi to you
   * @param data - your name
   */
  public async register(data) {
    const { app } = this;
    const result = await app.mysql.insert(prefix, {
      name: 'username',
      ...data,
    });
    return result.insertId;
  }
}
