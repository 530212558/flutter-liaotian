// app/extend/helper.js
const errorCode = {
  '1001': { statusCode: 1001, message: '该用户已经注册' },
};
module.exports = {
  success(data) {
    return {
      statusCode: 200,
      data,
    };
  },
  paramError(codeNum) {
    return {
      statusCode: 400,
      message: '未知错误',
      ...(errorCode[codeNum] || {}),
    };
  },
  error(err, message) {
    return {
      statusCode: 400,
      message,
      err,
    };
  },
};
