module.exports = app => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;

  const Industry = new Schema({
    subLevelModelList: { type: Array },
    name: { type: String },
    code: { type: Number },
  });

  return mongoose.model('Industry', Industry, 'Industry');
};
