module.exports = app => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;

  const ChinaArea = new Schema({
    children: { type: Array },
    name: { type: String },
    code: { type: Number },
    province: { type: Number },
  });

  return mongoose.model('ChinaArea', ChinaArea, 'ChinaArea');
};
