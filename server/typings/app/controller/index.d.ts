// This file is created by egg-ts-helper@1.25.8
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportNews from '../../../app/controller/news';
import ExportPublic from '../../../app/controller/public';
import ExportUser from '../../../app/controller/user';
import ExportUserInfo from '../../../app/controller/userInfo';

declare module 'egg' {
  interface IController {
    news: ExportNews;
    public: ExportPublic;
    user: ExportUser;
    userInfo: ExportUserInfo;
  }
}
