// This file is created by egg-ts-helper@1.25.8
// Do not modify this file!!!!!!!!!

import 'egg';
type AnyClass = new (...args: any[]) => any;
type AnyFunc<T = any> = (...args: any[]) => T;
type CanExportFunc = AnyFunc<Promise<any>> | AnyFunc<IterableIterator<any>>;
type AutoInstanceType<T, U = T extends CanExportFunc ? T : T extends AnyFunc ? ReturnType<T> : T> = U extends AnyClass ? InstanceType<U> : U;
import ExportNews from '../../../app/service/news';
import ExportPublic from '../../../app/service/public';
import ExportUser from '../../../app/service/user';
import ExportUserInfo from '../../../app/service/userInfo';

declare module 'egg' {
  interface IService {
    news: AutoInstanceType<typeof ExportNews>;
    public: AutoInstanceType<typeof ExportPublic>;
    user: AutoInstanceType<typeof ExportUser>;
    userInfo: AutoInstanceType<typeof ExportUserInfo>;
  }
}
