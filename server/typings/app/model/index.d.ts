// This file is created by egg-ts-helper@1.25.8
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportChinaArea from '../../../app/model/ChinaArea';
import ExportIndustry from '../../../app/model/industry';

declare module 'egg' {
  interface IModel {
    ChinaArea: ReturnType<typeof ExportChinaArea>;
    Industry: ReturnType<typeof ExportIndustry>;
  }
}
