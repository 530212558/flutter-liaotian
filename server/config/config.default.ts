import { EggAppConfig, EggAppInfo, PowerPartial } from 'egg';

export default (appInfo: EggAppInfo) => {
  const config = {} as PowerPartial<EggAppConfig>;

  // override config from framework / plugin
  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1602556351626_9321';

  // add your egg config in here
  config.middleware = [];

  config.jwt = {
    secret: 'ylw', // 自定义 token 的加密条件字符串
  };

  config.mysql = {
    // 单数据库信息配置
    client: {
      // host
      host: '47.101.137.34',
      // 端口号
      port: '3306',
      // 用户名
      user: 'root',
      // 密码
      password: '123456',
      // 数据库名
      database: 'flutter',
      //  设置连接时的字符格式
      charset: 'utf8mb4',
    },

    // 是否加载到 app 上，默认开启
    app: true,
    // 是否加载到 agent 上，默认关闭
    agent: false,
  };

  config.mongoose = {
    url: 'mongodb://47.101.137.34:27017/flutter',
    options: {},
    // mongoose global plugins, expected a function or an array of function and options
    // plugins: [createdPlugin, [updatedPlugin, pluginOptions]],
  };

  config.security = {
    // domainWhiteList: ['127.0.0.1'], // 安全白名单，以 . 开头
    csrf: {
      // ignore: ctx => isInnerIp(ctx.ip), // 判断是否需要 ignore 的方法，请求上下文 context 作为第一个参数
      enable: false, // 禁止默认安全
    },
    xframe: {
      enable: false,
    },
  };

  config.io = {
    init: {}, // passed to engine.io
    namespace: {
      '/news': {
        // 预处理器中间件, 这个是连接中间件， （客户端连接成功与断开连接【触发】）
        // 它对应的文件是/app/io/middleware/connection.js, 这里可以配置多个文件, 用逗号隔开
        connectionMiddleware: ['connection'],
        // 通常用于对消息做预处理，又或者是对加密消息的解密等操 （这个会在每次消息的时候触发）
        packetMiddleware: ['packet'],
      },
      '/example': {
        connectionMiddleware: [],
        packetMiddleware: [],
      },
    },
  };

  // add your special config in here
  const bizConfig = {
    sourceUrl: `https://github.com/eggjs/examples/tree/master/${appInfo.name}`,
  };

  // the return config will combines to EggAppConfig
  return {
    ...config,
    ...bizConfig,
  };
};
